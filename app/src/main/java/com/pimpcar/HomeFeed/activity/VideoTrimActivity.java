package com.pimpcar.HomeFeed.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.EditText;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import com.gowtham.library.utils.LogMessage;
import com.gowtham.library.utils.TrimVideo;
import com.pimpcar.BuildConfig;
import com.pimpcar.PostEditting.activity.VideoPimpingActivity;
import com.pimpcar.R;

import java.io.File;
import java.util.UUID;

public class VideoTrimActivity extends AppCompatActivity {

    private static final String TAG = "VideoTrimActivity";
    private VideoView videoView;
    TextView uploadVidepo;
    String selected_video = "";
    private MediaController mediaController;
    private EditText edtFixedGap, edtMinGap, edtMinFrom, edtMAxTo;
    private int trimType;
    ActivityResultLauncher<Intent> videoTrimResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == Activity.RESULT_OK &&
                        result.getData() != null) {
                    Uri uriResult = Uri.parse(TrimVideo.getTrimmedVideoPath(result.getData()));
                    Log.d(TAG, "Trimmed path:: " + uriResult);
                    Toast.makeText(this, ""+ uriResult, Toast.LENGTH_SHORT).show();
                    File videoFile = new File(String.valueOf(uriResult));

                    Log.i(TAG, Uri.fromFile(videoFile).toString());
//                    Toast.makeText(VideoTrimActivity.this, "" + Uri.fromFile(videoFile).toString(), Toast.LENGTH_SHORT).show();
                    MediaScannerConnection.scanFile(this, new String[]{videoFile.getAbsolutePath()}, null, (path, uri) -> Toast.makeText(this, "final Uri" + path, Toast.LENGTH_SHORT).show());
//                    String outputFile = getExternalFilesDir("DirName") + "/fileName.extension";

                    File file = new File(uriResult.toString());
//                    Log.e("OutPutFile",outputFile);
                    Uri uri = FileProvider.getUriForFile(VideoTrimActivity.this,
                            BuildConfig.APPLICATION_ID + ".provider", file);
                    Toast.makeText(this, "" + uri, Toast.LENGTH_SHORT).show();

//                    videoView.setMediaController(mediaController);
//                    videoView.setVideoURI(uri);
//                    videoView.requestFocus();
//                    videoView.start();
//
//                    videoView.setOnPreparedListener(mediaPlayer -> {
//                        mediaController.setAnchorView(videoView);
//                    });
//                    String selectedFilePath = HelperMethods.getPath(getApplicationContext(), uriResult);
//                    File file = new File(selectedFilePath);

                    Intent in1 = new Intent(this, VideoPimpingActivity.class);
                    in1.putExtra("selected_video", uriResult.toString());
                    in1.putExtra("selected_uri", uri.toString());
                    in1.putExtra("data", "camera");
                    startActivity(in1);
                    finish();
                    String filepath = String.valueOf(uriResult);
                    File filee = new File(filepath);
                    long length = filee.length();
                    Log.d(TAG, "Video size:: " + (length / 1024));
                } else
                    LogMessage.v("videoTrimResultLauncher data is null");
            });


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_trim);

        Bundle bundle = getIntent().getExtras();
        String dataVideo = bundle.getString("selected_uri");
        selected_video = bundle.getString("selected_video");
        Toast.makeText(this, "" + dataVideo, Toast.LENGTH_SHORT).show();
        openTrimActivity(dataVideo);
    }

    private void openTrimActivity(String data) {

        TrimVideo.activity(data)
                .setFixedDuration(30)
                .setLocal("en")
                .start(this, videoTrimResultLauncher);
    }
}