package com.pimpcar.HomeFeed.fragments.home.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.pimpcar.HomeFeed.fragments.home.eventbus.OpenUserProfileEvent;
import com.pimpcar.HomeFeed.fragments.home.eventbus.StartCommentActivity;
import com.pimpcar.HomeFeed.fragments.home.pojos.HomeFeedResponse;
import com.pimpcar.HomeFeed.fragments.home.viewholder.HomeFeedViewHolder;
import com.pimpcar.HomeFeed.fragments.profile.comments.activity.PostComment;
import com.pimpcar.HomeFeed.fragments.profile.comments.activity.PostLikes;
import com.pimpcar.HomeFeed.fragments.profile.profile_pager_fragments.viewholder.FeedInfiniteLoadingView;
import com.pimpcar.R;
import com.pimpcar.Widgets.InfiniteAdapter.InfiniteAdapter;
import com.pimpcar.utils.Constants;
import com.pimpcar.utils.HelperMethods;
import com.pimpcar.utils.ResourcesUtil;
import com.pimpcar.utils.Utils;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.squareup.picasso.Transformation;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;


public class HomeFeedAdapter extends InfiniteAdapter<HomeFeedViewHolder> {
    private Context _mContext;
    private LayoutInflater _mLayoutInflater_item = null, _mLayoutInflater_loading = null;
    private FeedInfiniteLoadingView viewHolderForLoadingView;
    private ArrayList<HomeFeedResponse.DataBean.PostsBean> _mPostsBeans;
    private HomeFeedViewHolder homeFeedViewHolder;

    public HomeFeedAdapter(Context context, ArrayList<HomeFeedResponse.DataBean.PostsBean> postsBeans) {
        this._mContext = context;
        this._mPostsBeans = postsBeans;
        if (_mContext != null) {
            _mLayoutInflater_item = LayoutInflater.from(_mContext);
            _mLayoutInflater_loading = LayoutInflater.from(_mContext);
        }
    }


    @Override
    public RecyclerView.ViewHolder getLoadingViewHolder(ViewGroup parent) {
        View itemView = _mLayoutInflater_loading.inflate(R.layout.infinite_loading_view, parent, false);
        ButterKnife.bind(this, itemView);
        viewHolderForLoadingView = new FeedInfiniteLoadingView(itemView);
        return viewHolderForLoadingView;
    }

    @Override
    public int getCount() {
        if (_mPostsBeans != null && _mPostsBeans.size() > 0)
            return _mPostsBeans.size();
        return 0;
    }

    @Override
    public int getViewType(int position) {
        return 1;
    }

    @Override
    public HomeFeedViewHolder onCreateView(ViewGroup parent, int viewType) {
        View itemView = _mLayoutInflater_item.inflate(R.layout.profile_rv_single_item, parent, false);
        ButterKnife.bind(this, itemView);
        homeFeedViewHolder = new HomeFeedViewHolder(itemView, _mPostsBeans, _mContext);
        return homeFeedViewHolder;

    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof FeedInfiniteLoadingView) {


        } else {

            homeFeedViewHolder = (HomeFeedViewHolder) holder;
            homeFeedViewHolder.r1.setVisibility(View.VISIBLE);
            HomeFeedResponse.DataBean.PostsBean single_post_details = _mPostsBeans.get(position);
            if (!single_post_details.getUser_name().equalsIgnoreCase("")) {
                homeFeedViewHolder.user_name_tv.setText(single_post_details.getUser_name());
            } else {
                homeFeedViewHolder.user_name_tv.setText(single_post_details.getPost_user_name());
            }


            if (single_post_details != null && single_post_details.getUser_profile_pic_url() != null && !single_post_details.getUser_profile_pic_url().isEmpty()) {
                Picasso.with(_mContext)
                        .load(single_post_details.getUser_profile_pic_url())
                        .fit()
                        .into(homeFeedViewHolder.user_profile_pic_rounded);
            } else {

            }

            if (single_post_details != null && single_post_details.getPost_image_url() != null && !single_post_details.getPost_image_url().isEmpty()) {


                DisplayMetrics displayMetrics = new DisplayMetrics();
                ((Activity) _mContext).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                int height = displayMetrics.heightPixels;
                int width = displayMetrics.widthPixels;


                Transformation transformation = new Transformation() {

                    @Override
                    public Bitmap transform(Bitmap source) {


                        int targetWidth = width;

                        double aspectRatio = (double) source.getHeight() / (double) source.getWidth();
                        int targetHeight = (int) (targetWidth * aspectRatio);
                        Bitmap result = Bitmap.createScaledBitmap(source, targetWidth, targetHeight, false);
                        if (result != source) {
                            // Same bitmap is returned if sizes are the same
                            source.recycle();
                        }
                        return result;
                    }

                    @Override
                    public String key() {
                        return "transformation" + " desiredWidth";
                    }
                };


                Picasso.with(_mContext)
                        .load(single_post_details.getPost_image_url())
                        .transform(transformation)
                        .into(homeFeedViewHolder.user_post_image);
            } else {

            }


            if (single_post_details != null && single_post_details.getPost_user_name() != null && single_post_details.getPost_description() != null) {
                String description = single_post_details.getPost_description();
                homeFeedViewHolder.post_description.setText(Html.fromHtml(description));
            }


            if (single_post_details != null && single_post_details.getPost_likes() != null && single_post_details.getPost_likes().size() > 0) {
                homeFeedViewHolder.post_number_of_likes.setText(single_post_details.getPost_likes().size() + " likes");
                homeFeedViewHolder.post_number_of_likes.setVisibility(View.VISIBLE);
            } else {
                homeFeedViewHolder.post_number_of_likes.setText(0 + " likes");

                homeFeedViewHolder.post_number_of_likes.setVisibility(View.VISIBLE);
            }

            if (single_post_details != null && single_post_details.getPost_location() != null) {
                homeFeedViewHolder.user_location.setText(single_post_details.getPost_location());

            }

            if (single_post_details != null && single_post_details.getPost_likes() != null && single_post_details.getPost_likes().size() > 4) {


                homeFeedViewHolder.first_person_image.setVisibility(View.VISIBLE);
                homeFeedViewHolder.second_person_dp.setVisibility(View.VISIBLE);
                homeFeedViewHolder.third_person_dp.setVisibility(View.VISIBLE);
                homeFeedViewHolder.fourth_person_dp.setVisibility(View.VISIBLE);


                if (single_post_details.getPost_likes().get(0).getUser_profile_pic_url() != null && !single_post_details.getPost_likes().get(0).getUser_profile_pic_url().isEmpty()) {
                    Picasso.with(_mContext)
                            .load(single_post_details.getPost_likes().get(0).getUser_profile_pic_url())
                            .fit()
                            .into(homeFeedViewHolder.first_person_image);

                }

                if (single_post_details.getPost_likes().get(1).getUser_profile_pic_url() != null && !single_post_details.getPost_likes().get(1).getUser_profile_pic_url().isEmpty()) {
                    Picasso.with(_mContext)
                            .load(single_post_details.getPost_likes().get(1).getUser_profile_pic_url())
                            .fit()
                            .into(homeFeedViewHolder.second_person_dp);
                }


                if (single_post_details.getPost_likes().get(2).getUser_profile_pic_url() != null && !single_post_details.getPost_likes().get(2).getUser_profile_pic_url().isEmpty()) {
                    Picasso.with(_mContext)
                            .load(single_post_details.getPost_likes().get(2).getUser_profile_pic_url())
                            .fit()
                            .into(homeFeedViewHolder.third_person_dp);

                }

                if (single_post_details.getPost_likes().get(3).getUser_profile_pic_url() != null && !single_post_details.getPost_likes().get(2).getUser_profile_pic_url().isEmpty()) {
                    Picasso.with(_mContext)
                            .load(single_post_details.getPost_likes().get(3).getUser_profile_pic_url())
                            .fit()
                            .into(homeFeedViewHolder.fourth_person_dp);
                }


            } else {
                if (single_post_details != null && single_post_details.getPost_likes() != null && single_post_details.getPost_likes().size() == 0) {
                    homeFeedViewHolder.first_person_image.setVisibility(View.GONE);
                    homeFeedViewHolder.second_person_dp.setVisibility(View.GONE);
                    homeFeedViewHolder.third_person_dp.setVisibility(View.GONE);
                    homeFeedViewHolder.fourth_person_dp.setVisibility(View.GONE);
                }
                if (single_post_details != null && single_post_details.getPost_likes() != null && single_post_details.getPost_likes().size() == 1) {
                    homeFeedViewHolder.first_person_image.setVisibility(View.VISIBLE);
                    homeFeedViewHolder.second_person_dp.setVisibility(View.GONE);
                    homeFeedViewHolder.third_person_dp.setVisibility(View.GONE);
                    homeFeedViewHolder.fourth_person_dp.setVisibility(View.GONE);


                    if (single_post_details.getPost_likes().get(0).getUser_profile_pic_url() != null && !single_post_details.getPost_likes().get(0).getUser_profile_pic_url().isEmpty()) {
                        Picasso.with(_mContext)
                                .load(single_post_details.getPost_likes().get(0).getUser_profile_pic_url())
                                .fit()
                                .into(homeFeedViewHolder.first_person_image);

                    }


                }
                if (single_post_details != null && single_post_details.getPost_likes() != null && single_post_details.getPost_likes().size() == 2) {
                    homeFeedViewHolder.first_person_image.setVisibility(View.VISIBLE);
                    homeFeedViewHolder.second_person_dp.setVisibility(View.VISIBLE);
                    homeFeedViewHolder.third_person_dp.setVisibility(View.GONE);
                    homeFeedViewHolder.fourth_person_dp.setVisibility(View.GONE);


                    if (single_post_details.getPost_likes().get(0).getUser_profile_pic_url() != null && !single_post_details.getPost_likes().get(0).getUser_profile_pic_url().isEmpty()) {
                        Picasso.with(_mContext)
                                .load(single_post_details.getPost_likes().get(0).getUser_profile_pic_url())
                                .fit()
                                .into(homeFeedViewHolder.first_person_image);

                    }

                    if (single_post_details.getPost_likes().get(1).getUser_profile_pic_url() != null && !single_post_details.getPost_likes().get(1).getUser_profile_pic_url().isEmpty()) {
                        Picasso.with(_mContext)
                                .load(single_post_details.getPost_likes().get(1).getUser_profile_pic_url())
                                .fit()
                                .into(homeFeedViewHolder.second_person_dp);
                    }


                }
                if (single_post_details != null && single_post_details.getPost_likes() != null && single_post_details.getPost_likes().size() == 3) {

                    homeFeedViewHolder.first_person_image.setVisibility(View.VISIBLE);
                    homeFeedViewHolder.second_person_dp.setVisibility(View.VISIBLE);
                    homeFeedViewHolder.third_person_dp.setVisibility(View.VISIBLE);
                    homeFeedViewHolder.fourth_person_dp.setVisibility(View.GONE);

                    if (single_post_details.getPost_likes().get(0).getUser_profile_pic_url() != null && !single_post_details.getPost_likes().get(0).getUser_profile_pic_url().isEmpty()) {
                        Picasso.with(_mContext)
                                .load(single_post_details.getPost_likes().get(0).getUser_profile_pic_url())
                                .fit()
                                .into(homeFeedViewHolder.first_person_image);

                    }

                    if (single_post_details.getPost_likes().get(1).getUser_profile_pic_url() != null && !single_post_details.getPost_likes().get(0).getUser_profile_pic_url().isEmpty()) {
                        Picasso.with(_mContext)
                                .load(single_post_details.getPost_likes().get(1).getUser_profile_pic_url())
                                .fit()
                                .into(homeFeedViewHolder.second_person_dp);
                    }


                    if (single_post_details.getPost_likes().get(2).getUser_profile_pic_url() != null && !single_post_details.getPost_likes().get(0).getUser_profile_pic_url().isEmpty()) {
                        Picasso.with(_mContext)
                                .load(single_post_details.getPost_likes().get(2).getUser_profile_pic_url())
                                .fit()
                                .into(homeFeedViewHolder.third_person_dp);

                    }


                }
                if (single_post_details != null && single_post_details.getPost_likes() != null && single_post_details.getPost_likes().size() == 4) {

                    homeFeedViewHolder.first_person_image.setVisibility(View.VISIBLE);
                    homeFeedViewHolder.second_person_dp.setVisibility(View.VISIBLE);
                    homeFeedViewHolder.third_person_dp.setVisibility(View.VISIBLE);
                    homeFeedViewHolder.fourth_person_dp.setVisibility(View.VISIBLE);


                    if (single_post_details.getPost_likes().get(0).getUser_profile_pic_url() != null && !single_post_details.getPost_likes().get(0).getUser_profile_pic_url().isEmpty()) {
                        Picasso.with(_mContext)
                                .load(single_post_details.getPost_likes().get(0).getUser_profile_pic_url())
                                .fit()
                                .into(homeFeedViewHolder.first_person_image);

                    }

                    if (single_post_details.getPost_likes().get(1).getUser_profile_pic_url() != null && !single_post_details.getPost_likes().get(1).getUser_profile_pic_url().isEmpty()) {
                        Picasso.with(_mContext)
                                .load(single_post_details.getPost_likes().get(1).getUser_profile_pic_url())
                                .fit()
                                .into(homeFeedViewHolder.second_person_dp);
                    }


                    if (single_post_details.getPost_likes().get(2).getUser_profile_pic_url() != null && !single_post_details.getPost_likes().get(1).getUser_profile_pic_url().isEmpty()) {
                        Picasso.with(_mContext)
                                .load(single_post_details.getPost_likes().get(2).getUser_profile_pic_url())
                                .fit()
                                .into(homeFeedViewHolder.third_person_dp);

                    }
                    if (single_post_details.getPost_likes().get(3).getUser_profile_pic_url() != null && !single_post_details.getPost_likes().get(1).getUser_profile_pic_url().isEmpty()) {
                        Picasso.with(_mContext)
                                .load(single_post_details.getPost_likes().get(3).getUser_profile_pic_url())
                                .fit()
                                .into(homeFeedViewHolder.fourth_person_dp);
                    }

                }


            }

            if (single_post_details != null && single_post_details.getPost_comments() != null && single_post_details.getPost_comments().size() == 0) {
                homeFeedViewHolder.user_comments_see.setText(ResourcesUtil.getString(R.string.add_comment));
            } else {
                if (single_post_details != null && single_post_details.getPost_comments() != null && single_post_details.getPost_comments().size() == 1) {
                    homeFeedViewHolder.user_comments_see.setText(1 + " comment");
                } else {
                    homeFeedViewHolder.user_comments_see.setText(single_post_details.getPost_comments().size() + " comments");
                }
            }


            if (HelperMethods.does_post_like_is_done_by_user(single_post_details.getPost_likes())) {
                homeFeedViewHolder.post_like_button.setBackgroundResource(R.drawable.liked_pressed);
                homeFeedViewHolder.post_like_button.getLayoutParams().height = Utils.dpToPx(25);
                homeFeedViewHolder.post_like_button.getLayoutParams().width = Utils.dpToPx(25);
                ViewGroup.MarginLayoutParams marginParams = (ViewGroup.MarginLayoutParams) homeFeedViewHolder.post_like_button.getLayoutParams();
                marginParams.setMargins(20, 20, 20, 20);
                homeFeedViewHolder.post_like_button.setTag("user_liked");
            } else {
                homeFeedViewHolder.post_like_button.setBackgroundResource(R.drawable.like_unpressed);
                homeFeedViewHolder.post_like_button.getLayoutParams().height = Utils.dpToPx(25);
                homeFeedViewHolder.post_like_button.getLayoutParams().width = Utils.dpToPx(25);
                ViewGroup.MarginLayoutParams marginParams = (ViewGroup.MarginLayoutParams) homeFeedViewHolder.post_like_button.getLayoutParams();
                marginParams.setMargins(20, 20, 20, 20);
                homeFeedViewHolder.post_like_button.setTag("user_not_liked");

            }


            homeFeedViewHolder.user_comments_see.setOnClickListener(v -> {
                EventBus.getDefault().post(new StartCommentActivity(_mPostsBeans.get(position)));
            });


            homeFeedViewHolder.comment_made_rl.setOnClickListener(v -> {
                Intent intent = new Intent(_mContext, PostComment.class);
                intent.putExtra(Constants.COMMENTS_COMING_FROM, Constants.FEED_COMMENTS);
                intent.putExtra(Constants.SINGLE_POST_DATA, _mPostsBeans.get(position));
                _mContext.startActivity(intent);
            });


            homeFeedViewHolder.post_number_of_likes.setOnClickListener(v -> {
                if (single_post_details.getPost_likes() != null && single_post_details.getPost_likes().size() > 0) {
                    Intent intent = new Intent(_mContext, PostLikes.class);
                    intent.putExtra(Constants.COMMENTS_COMING_FROM, Constants.FEED_COMMENTS);
                    intent.putExtra(Constants.SINGLE_POST_DATA, single_post_details);
                    intent.putExtra(Constants.POST_NUMBER_OF_LIKES, single_post_details.getPost_likes().size() + "");
                    _mContext.startActivity(intent);
                } else {
                    Toasty.error(_mContext, " No comments to check!", Toast.LENGTH_SHORT, true).show();
                }
            });


            homeFeedViewHolder.user_profile_pic_rounded.setOnClickListener(v -> {
                throw_event_to_open_profile(_mPostsBeans.get(position).getUser_id());
            });

            homeFeedViewHolder.user_name_tv.setOnClickListener(v -> {
                throw_event_to_open_profile(_mPostsBeans.get(position).getUser_id());
            });


            homeFeedViewHolder.three_vertical_dots.setOnClickListener(v -> {
                if (HelperMethods.is_user_logged_in()) {
                    show_dialog_for_share(_mPostsBeans.get(position).getPost_image_url(), _mContext);

                } else {
                    Toasty.error(_mContext, ResourcesUtil.getString(R.string.you_have_to_login), Toast.LENGTH_SHORT, true).show();

                }
            });


            if (single_post_details != null && single_post_details.getPost_timestamp() != 0) {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    Long timelong = new Long(single_post_details.getPost_timestamp());
                    String time = HelperMethods.timeAgo(timelong);
                    homeFeedViewHolder.post_time.setText(time);
                }
            }


        }

        super.onBindViewHolder(holder, position);
    }

    private void show_dialog_for_share(String postImage, Context context) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.share_post_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        AppCompatTextView share_layout = dialog.findViewById(R.id.share_layout);
        AppCompatTextView cancel = dialog.findViewById(R.id.cancel_layout);

        share_layout.setOnClickListener(v -> {

            Picasso.with(_mContext).load(postImage).into(new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("image/*");
                    i.putExtra(Intent.EXTRA_STREAM, getLocalBitmapUri(bitmap));
                    _mContext.startActivity(Intent.createChooser(i, "Share Image"));
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                }
            });

            dialog.dismiss();

        });


        cancel.setOnClickListener(v -> {
            dialog.dismiss();
        });


        dialog.show();

    }

    private Uri getLocalBitmapUri(Bitmap bitmap) {
        Uri bmpUri = null;
        try {
            File file = new File(_mContext.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_" + System.currentTimeMillis() + ".png");
            FileOutputStream out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }

    private void throw_event_to_open_profile(String user_id) {
        EventBus.getDefault().post(new OpenUserProfileEvent(user_id));
    }


//    public Uri getImageUri(Context inContext, Bitmap inImage) {
//        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
//
//        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
//        return Uri.parse(path);
//    }
//
//
//    private Bitmap getBitmapFromView(View view) {
//        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
//        Canvas canvas = new Canvas(returnedBitmap);
//        Drawable bgDrawable = view.getBackground();
//        if (bgDrawable != null) {
//            //has background drawable, then draw it on the canvas
//            bgDrawable.draw(canvas);
//        } else {
//            //does not have background drawable, then draw white background on the canvas
//            canvas.drawColor(Color.WHITE);
//        }
//        view.draw(canvas);
//        return returnedBitmap;
//    }


}
