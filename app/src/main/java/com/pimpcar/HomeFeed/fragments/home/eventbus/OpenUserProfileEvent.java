package com.pimpcar.HomeFeed.fragments.home.eventbus;

public class OpenUserProfileEvent {

    private String user_id;

    public OpenUserProfileEvent(String uid) {

        this.user_id = uid;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

}
