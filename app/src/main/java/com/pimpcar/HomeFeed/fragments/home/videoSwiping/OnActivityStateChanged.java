package com.pimpcar.HomeFeed.fragments.home.videoSwiping;

public interface OnActivityStateChanged {
    void onResumed();
    void onPaused();
}
