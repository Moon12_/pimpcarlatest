//package com.pimpcar.HomeFeed.fragments.home.videoSwiping;
//
//import android.content.Context;
//import android.media.AudioManager;
//import android.media.MediaPlayer;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.Button;
//import android.widget.ImageView;
//import android.widget.RelativeLayout;
//import android.widget.Toast;
//
//import androidx.annotation.NonNull;
//import androidx.recyclerview.widget.RecyclerView;
//import androidx.viewpager2.widget.ViewPager2;
//
//import com.pimpcar.R;
//import com.pimpcar.models.VideoDataModel;
//
//import java.util.List;
//
//public class VidViewPagerAdapter extends RecyclerView.Adapter<VidViewPagerAdapter.ViewHolder> {
//    int currentposition;
//    DataSpec dataSpec;
//    private LayoutInflater mInflater;
//    private ViewPager2 viewPager2;
//    PlayerView videoView;
//    SimpleExoPlayer simpleExoPlayer;
//    private List<VideoDataModel> galleryentries;
//    //   String url;
//    Context context;
//
//    //  private int[] colorArray = new int[]{android.R.color.black, android.R.color.holo_blue_dark, android.R.color.holo_green_dark, android.R.color.holo_red_dark};
//
//    public VidViewPagerAdapter(Context context, List<VideoDataModel> murl, ViewPager2 viewPager2) {
//
//        this.context = context;
//        this.mInflater = LayoutInflater.from(context);
//        this.galleryentries = murl;
//        this.viewPager2 = viewPager2;
//        this.setHasStableIds(true);
//    }
//
//
//    @Override
//    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View view = mInflater.inflate(R.layout.item_videos_container, parent, false);
//        return new ViewHolder(view);
//    }
//
//    @Override
//    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView) {
//        super.onDetachedFromRecyclerView(recyclerView);
//        Toast.makeText(context, "onDetachedFromRecyclerView", Toast.LENGTH_SHORT).show();
//
//
//    }
//
//    @Override
//    public void onBindViewHolder(ViewHolder holder, int position) {
//
//        //   super.onViewAttachedToWindow(holder);
//
//        holder.setIsRecyclable(false);
//
//        Log.d("playposition", String.valueOf(holder.getAdapterPosition()) + "   Viewholder get adapter position" + " Item id position "
//                + String.valueOf(holder.getItemId() + " position of it holder .getposition depricated" + String.valueOf(holder.getPosition())));
//
//
//        VideoDataModel vidpos = galleryentries.get(holder.getItemViewType());
//        String url = vidpos.getPost_image_url();
//
//
//        videoView.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
//
//            @Override
//            public void onViewAttachedToWindow(View v) {
//
//                try {
//                    initPlayer(url);
//                    Toast.makeText(context, "onViewAtached", Toast.LENGTH_SHORT).show();
//
//                } catch (NullPointerException ex) {
//
//
//                }
//            }
//            @Override
//            public void onViewDetachedFromWindow(View v) {
//                Toast.makeText(context, "onViewDetached", Toast.LENGTH_SHORT).show();
//
//            }
//        });
//
//
//    }
//
//    @Override
//    public void onViewRecycled(@NonNull ViewHolder holder) {
//        super.onViewRecycled(holder);
//        try {
//            videoView.getPlayer().stop();
//
//            releasePlayer();
//
//        } catch (NullPointerException ex) {
//
//            Log.d("recerror", ex.toString());
//        }
//
//    }
//
//    @Override
//    public int getItemViewType(int position) {
//        return position;
//    }
//
//    @Override
//    public int getItemCount() {
//        return galleryentries.size();
//    }
//
//
//    // stores and recycles views as they are scrolled off screen
//    public class ViewHolder extends RecyclerView.ViewHolder {
//
//        RelativeLayout relativeLayout;
//        Button button;
//
//        MediaPlayer audioplayer;
//        ImageView hiddenimage;
//
//
//        ViewHolder(View itemView) {
//            super(itemView);
//
//
//            Log.d("viewpager_current", String.valueOf(currentposition));
//
//
//            relativeLayout = itemView.findViewById(R.id.container);
//
//            videoView = itemView.findViewById(R.id.playerView);
//
//            audioplayer = new MediaPlayer();
//
//            audioplayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
//
//            simpleExoPlayer = new SimpleExoPlayer.Builder(context).build();
//
//            videoView.setControllerAutoShow(false);
//
//        }
//
//    }
//
//    public void initPlayer(String url) {
////
////        File file = new File(url);
////        // holder.videoView.setSource(url);
////
////        Uri content = Uri.fromFile(file);
////        dataSpec = new DataSpec(content);
////
////
////        final FileDataSource fileDataSource = new FileDataSource();
////        try {
////            fileDataSource.open(dataSpec);
////        } catch (FileDataSource.FileDataSourceException e) {
////            e.printStackTrace();
////        }
////
////        DataSource.Factory factory = new DataSource.Factory() {
////            @Override
////            public DataSource createDataSource() {
////                return fileDataSource;
////            }
////        };
//
//
////        simpleExoPlayer.prepare();
////        videoView.setPlayer(simpleExoPlayer);
////        simpleExoPlayer.setPlayWhenReady(true);
////        simpleExoPlayer.play();
//
//        if (simpleExoPlayer != null)
//            simpleExoPlayer.release();
//        simpleExoPlayer = new SimpleExoPlayer.Builder(context).build();
//        videoView.setPlayer(simpleExoPlayer);
////        holder.playerView.setPlayWhenReady(true);
//        MediaItem mediaItem = MediaItem.fromUri(url);
//        simpleExoPlayer.setMediaItem(mediaItem);
//        simpleExoPlayer.prepare();
//        simpleExoPlayer.play();
//
//
//    }
//
//
//    public void releasePlayer() {
//
//        if (simpleExoPlayer != null) {
//            simpleExoPlayer.setPlayWhenReady(true);
//            simpleExoPlayer.stop();
//            simpleExoPlayer.release();
//            simpleExoPlayer = null;
//
//            videoView.setPlayer(null);
//            videoView = null;
//            System.out.println("releasePlayer");
//        }
//    }
//
//    public OnActivityStateChanged registerActivityState() {
//
//        return new OnActivityStateChanged() {
//
//            public void onResumed() {
//                Log.d("SimpleTextListAdapter", "onResumed: ");
//
//                Toast.makeText(context, "Resumed now", Toast.LENGTH_SHORT).show();
//
//            }
//
//
//            public void onPaused() {
//
//
////                View view = context.getCurrentFocus();
//
//
//                Log.d("SimpleTextListAdapter", "onPaused: ");
//
//
//                Toast.makeText(context, "Paused now", Toast.LENGTH_SHORT).show();
//
//            }
//
//        };
//    }
//
//
//}
//
///////////////////  this is the listener designed to get onpause from main activity.
