package com.pimpcar.HomeFeed.fragments.home.videoSwiping;//package com.pimpcar.HomeFeed.fragments.home.videoSwiping;
//
//import android.app.Activity;
//import android.content.Intent;
//import android.os.Bundle;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.Toast;
//
//import androidx.annotation.Nullable;
//import androidx.fragment.app.Fragment;
//import androidx.viewpager2.widget.ViewPager2;
//
//import com.clockbyte.admobadapter.AdmobRecyclerAdapterWrapper;
//import com.google.gson.JsonObject;
//import com.pimpcar.HomeFeed.fragments.home.adapters.HomeVideoNewAdapter;
//import com.pimpcar.Network.ApiClient;
//import com.pimpcar.Network.ApiService;
//import com.pimpcar.R;
//import com.pimpcar.Widgets.InfiniteAdapter.InfiniteAdapter;
//import com.pimpcar.Widgets.progressbar.KProgressHUD;
//import com.pimpcar.models.VideoDataModel;
//import com.pimpcar.models.VideoListResponseModel;
//import com.pimpcar.models.comments.models.CommentsDataModel;
//import com.pimpcar.models.comments.models.CommentsResponseModel;
//import com.pimpcar.utils.HelperMethods;
//import com.pimpcar.utils.Utilities;
//import com.pimpcar.videoPart.VideoInterface;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import butterknife.Unbinder;
//import io.reactivex.disposables.CompositeDisposable;
//import retrofit2.Call;
//import retrofit2.Callback;
//import retrofit2.Response;
//import retrofit2.Retrofit;
//import retrofit2.converter.gson.GsonConverterFactory;
//import timber.log.Timber;
//
//public class VideoSwipingFragment11 extends Fragment {
//    View view;
//    private KProgressHUD hud;
//    ViewPager2 videosViewPager;
//    private Unbinder unbinder = null;
//    List<VideoDataModel> videoItems;
//
//    private AdmobRecyclerAdapterWrapper adapterWrapper;
//    private int first_next_page = 0;
//    private ApiService apiService;
//    private CompositeDisposable disposable = new CompositeDisposable();
//    private HomeVideoNewAdapter homeFeedAdapter = null;
//    private int REQUEST_ACTIVITY_RESULT_CALL_FEED = 1234;
//    ArrayList<VideoItem> videoSeparate;
//    List<CommentsDataModel> commentsDataModels;
//    private InfiniteAdapter.OnLoadMoreListener onLoadMoreListener =
//            new InfiniteAdapter.OnLoadMoreListener() {
//                @Override
//                public void onLoadMore() {
//
//                    JsonObject jsonObject = new JsonObject();
//                    JsonObject data = new JsonObject();
//                    JsonObject attributes = new JsonObject();
//                    data.add("attributes", attributes);
//                    attributes.addProperty("page_number", first_next_page);
////        attributes.addProperty("user_id", HelperMethods.get_user_id());
//                    jsonObject.add("data", data);
//
//
//                    hud = KProgressHUD.create(getActivity())
//                            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
//                            .setLabel("Please wait")
//                            .setCancellable(true)
//                            .setAnimationSpeed(2)
//                            .setSize(50, 50)
//                            .setDimAmount(0.5f)
//                            .show();
//                    ApiService service = ApiClient.getClient(getActivity()).create(ApiService.class);
//                    Call<VideoListResponseModel> call = service.getVideoPosts(jsonObject, HelperMethods.get_user_jwt_key());
//                    call.enqueue(new Callback<VideoListResponseModel>() {
//                        @Override
//                        public void onResponse(Call<VideoListResponseModel> call, Response<VideoListResponseModel> response) {
//
//                            assert response.body() != null;
//                            if (response.isSuccessful()) {
//                                hud.dismiss();
////                    Toast.makeText(getActivity(), response.body().getData().getMessage(), Toast.LENGTH_SHORT).show();
//                                videoItems = response.body().getData().getPosts();
//                                if (response.body().getData().getPosts() != null && response.body().getData().getPosts().size() > 0) {
//                                    for (int i = 0; i < response.body().getData().getPosts().size(); i++) {
////                                            videoItems.add(response.body().getData().getPosts().get(i));
//                                        homeFeedAdapter.notifyItemInserted(videoItems.size() - 1);
//                                        String fileType = getFileTypeFromURL(videoItems.get(i).getPost_image_url());
//                                        if (fileType.equals("video")) {
//                                            VideoItem item = new VideoItem();
//                                            item.videoURL = videoItems.get(i).getPost_image_url();
//                                            item.videoTitle = videoItems.get(i).getUser_name();
//                                            item.videoDesc = videoItems.get(i).getPost_description();
//                                            item.post_id = videoItems.get(i).get_id();
//                                            item.userName = videoItems.get(i).getUser_name();
//                                            item.user_profile_pic_url = videoItems.get(i).getUser_profile_pic_url();
//                                            videoSeparate.add(item);
//                                            Utilities.saveString(getActivity(), "videoUploaded", "no");
//                                            videosViewPager.setAdapter(new HomeVideoNewAdapter(getActivity(), videoSeparate));
//
//
//                                        }
//                                        homeFeedAdapter.moreDataLoaded(videoItems.size(), response.body().getData().getPosts().size());
//
//
//                                        if (response.body().getData().getNext_page() == 1) {
//                                            first_next_page = first_next_page + 1;
//                                            homeFeedAdapter.setShouldLoadMore(true);
//                                        } else {
//                                            homeFeedAdapter.setShouldLoadMore(false);
//                                        }
//
//                                    }
//                                } else {
//                                    homeFeedAdapter.setShouldLoadMore(false);
//                                }
//
//
//                            } else {
//                                hud.dismiss();
//                                Toast.makeText(getActivity(), "failed", Toast.LENGTH_SHORT).show();
//                            }
//                        }
//
//                        @Override
//                        public void onFailure(Call<VideoListResponseModel> call, Throwable t) {
//                            hud.dismiss();
////                t.printStackTrace();
//                            Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
//                        }
//                    });
//                }
//            };
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        // Inflate the layout for this fragment
//        view = inflater.inflate(R.layout.fragment_video_swiping, container, false);
//
//        videosViewPager = view.findViewById(R.id.viewPagerVideos);
//        videoItems = new ArrayList<>();
//        videoSeparate = new ArrayList<>();
//        commentsDataModels = new ArrayList<>();
//        setrefrest_layout();
//        setRetainInstance(true);
//        getGridUserPostSApi();
////        videosViewPager.setAdapter(new VideoSwipingAdapter(getActivity(), videoItems));
//        return view;
//    }
//
//    private void getGridUserPostSApi() {
//
//        JsonObject jsonObject = new JsonObject();
//        JsonObject data = new JsonObject();
//        JsonObject attributes = new JsonObject();
//        data.add("attributes", attributes);
//        attributes.addProperty("page_number", first_next_page);
////        attributes.addProperty("user_id", HelperMethods.get_user_id());
//        jsonObject.add("data", data);
//
//
//        hud = KProgressHUD.create(getActivity())
//                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
//                .setLabel("Please wait")
//                .setCancellable(true)
//                .setAnimationSpeed(2)
//                .setSize(50, 50)
//                .setDimAmount(0.5f)
//                .show();
//        ApiService service = ApiClient.getClient(getActivity()).create(ApiService.class);
//        Call<VideoListResponseModel> call = service.getVideoPosts(jsonObject, HelperMethods.get_user_jwt_key());
//        call.enqueue(new Callback<VideoListResponseModel>() {
//            @Override
//            public void onResponse(Call<VideoListResponseModel> call, Response<VideoListResponseModel> response) {
//
//                assert response.body() != null;
//                if (response.isSuccessful()) {
//                    hud.dismiss();
////                    Toast.makeText(getActivity(), response.body().getData().getMessage(), Toast.LENGTH_SHORT).show();
//                    videoItems = response.body().getData().getPosts();
//
//
//                    for (int i = 0; i <= videoItems.size() - 1; i++) {
//                        String fileType = getFileTypeFromURL(videoItems.get(i).getPost_image_url());
//                        if (fileType.equals("video")) {
//                            VideoItem item = new VideoItem();
//                            item.videoURL = videoItems.get(i).getPost_image_url();
//                            item.videoTitle = videoItems.get(i).getUser_name();
//                            item.videoDesc = videoItems.get(i).getPost_description();
//                            item.post_id = videoItems.get(i).get_id();
//                            item.userName = videoItems.get(i).getUser_name();
//                            item.user_profile_pic_url = videoItems.get(i).getUser_profile_pic_url();
//                            videoSeparate.add(item);
//                            Utilities.saveString(getActivity(), "videoUploaded", "no");
//                            videosViewPager.setAdapter(new HomeVideoNewAdapter(getActivity(), videoSeparate));
//                            homeFeedAdapter.setOnLoadMoreListener(onLoadMoreListener);
//                            if (response.body().getData().getNext_page() == 1) {
//                                increase_page_number();
//                                homeFeedAdapter.setShouldLoadMore(true);
//                            } else {
//                                homeFeedAdapter.setShouldLoadMore(false);
//                            }
//
//                        }
////
//                    }
//
//
//                } else {
//                    hud.dismiss();
//                    Toast.makeText(getActivity(), "failed", Toast.LENGTH_SHORT).show();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<VideoListResponseModel> call, Throwable t) {
//                hud.dismiss();
////                t.printStackTrace();
//                Toast.makeText(getActivity(), "" + t.getMessage(), Toast.LENGTH_SHORT).show();
//            }
//        });
//    }
//
//    private String getFileTypeFromURL(String url) {
//        String[] splitedArray = url.split("\\.");
//        String lastValueOfArray = splitedArray[splitedArray.length - 1];
//        if (lastValueOfArray.equals("mp4") || lastValueOfArray.equals("flv") || lastValueOfArray.equals("m4a") || lastValueOfArray.equals("3gp") || lastValueOfArray.equals("mkv")) {
//            return "video";
//        } else if (lastValueOfArray.equals("mp3") || lastValueOfArray.equals("ogg")) {
//            return "audio";
//        } else if (lastValueOfArray.equals("jpg") || lastValueOfArray.equals("png") || lastValueOfArray.equals("gif")) {
//            return "photo";
//        } else {
//            return "";
//        }
//    }
//
//    private void getCommentsApi() {
//        hud = KProgressHUD.create(getActivity())
//                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE);
//        hud.setSize(50, 50);
//        hud.show();
//
//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl(VideoInterface.VIDEOURL)
//                .addConverterFactory(GsonConverterFactory.create())
//                .build();
//
//        //Create a file object using file path
//
//        VideoInterface getResponse = retrofit.create(VideoInterface.class);
////        Call<String> call = getResponse.uploadImage(fileToUpload,"descriptions","hashtags","userTags","6184be3e6274da6a8865e3ed","1637392740");
//        Call<CommentsResponseModel> call = getResponse.getCommetnsApi("618617f76274da6a8865e3f6");
//        Log.d("assss", "asss");
//        call.enqueue(new Callback<CommentsResponseModel>() {
//            @Override
//            public void onResponse(Call<CommentsResponseModel> call, Response<CommentsResponseModel> response) {
//                if (response.code() == 200) {
//                    hud.dismiss();
//                    for (int i = 0; i <= commentsDataModels.size(); i++) {
//
//                        Toast.makeText(getActivity(), "Success" + response.body().getComeentdata().get(i).getComment_replies(), Toast.LENGTH_SHORT).show();
//
//
//                    }
//                } else {
//                    Toast.makeText(getActivity(), "fail", Toast.LENGTH_SHORT).show();
//
//
//                }
//            }
//
//            @Override
//            public void onFailure(Call call, Throwable t) {
//                hud.dismiss();
//                Log.d("gttt", call.toString());
//                Toast.makeText(getActivity(), "failed", Toast.LENGTH_SHORT).show();
//
//            }
//        });
//
//    }
//
//    @Override
//    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
//        super.onActivityCreated(savedInstanceState);
//        if (getActivity() != null) {
//            apiService = ApiClient.getClient(getActivity().getApplicationContext()).create(ApiService.class);
//            getGridUserPostSApi();
//        }
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
////        if (!EventBus.getDefault().isRegistered(this)) {
////            EventBus.getDefault().register(this);
////        }
//    }
//
//    @Override
//    public void onPause() {
//        setFirst_next_page(0);
//        super.onPause();
////        if (EventBus.getDefault().isRegistered(this))
////            EventBus.getDefault().unregister(this);
//    }
//
//
//    private void increase_page_number() {
//        first_next_page = first_next_page + 1;
//    }
//
//
//    private void setrefrest_layout() {
//
//        setFirst_next_page(0);
//        getGridUserPostSApi();
//
//    }
//
//    @Override
//    public void onDestroyView() {
//        if (unbinder != null) {
//            unbinder.unbind();
//        }
//
//        if (adapterWrapper != null) {
//            adapterWrapper.destroyAds();
//        }
//
//        super.onDestroyView();
//
//    }
//
//
//    public int getFirst_next_page() {
//        return first_next_page;
//    }
//
//    public void setFirst_next_page(int first_next_page) {
//        this.first_next_page = first_next_page;
//    }
//
//
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == REQUEST_ACTIVITY_RESULT_CALL_FEED) {
//            if (resultCode == Activity.RESULT_OK) {
//                Bundle mbundle = data.getExtras();
//                String result_data = mbundle.getString("result");
//                Timber.d("result data : " + result_data);
//                if (result_data.contains("yes updated")) {
//                    getGridUserPostSApi();
//                }
//            }
//        }
//
//    }
//
//
////    @Subscribe
////    public void likeDislikePost(LikeOnPostEvent postLike) {
////        PostLikeDislikeRequestBody post_like_dislike_body = HelperMethods.create_post_like_dislike_body(postLike.getPost_id());
////        disposable.add(apiService.postLikeDislikeOnPost(HelperMethods.get_user_jwt_key(), post_like_dislike_body)
////                .subscribeOn(Schedulers.io())
////                .observeOn(AndroidSchedulers.mainThread())
////                .subscribeWith(new DisposableSingleObserver<PostLikeDislikeResponse>() {
////                    @Override
////                    public void onSuccess(PostLikeDislikeResponse s) {
////                        Timber.d("post like dislike done message " + s.getData().getMessage());
////
////                    }
////
////                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
////                    @Override
////                    public void onError(Throwable e) {
////                        if (e instanceof HttpException) {
////                            ResponseBody body = ((HttpException) e).response().errorBody();
////                            try {
////                                String error_body = body.string();
////                                String error = Utils.get_error_from_response(error_body);
////
////                                if (error.contentEquals(NetworkResponseMessages.USER_EMAIL_NOT_VERIFIED)) {
////
////                                }
////
////                                EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, error.toUpperCase()));
////                            } catch (IOException e1) {
////                                e1.printStackTrace();
////                            }
////                        }
////                    }
////                }));
////
////
////    }
////    @Override
////    public void onResume() {
////        super.onResume();
////        if (Utilities.getString(getActivity(),"videoUploaded").equals("yes")){
////            getGridUserPostSApi();
////
////        }
////    }
//}