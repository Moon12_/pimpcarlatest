package com.pimpcar.HomeFeed.fragments.notification.adapters.viewholders;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;

import com.pimpcar.HomeFeed.fragments.notification.models.NotificationResponseBody;
import com.pimpcar.HomeFeed.fragments.notification.models.eventbus.FollowUser;
import com.pimpcar.HomeFeed.fragments.notification.models.eventbus.UnfollowUser;
import com.pimpcar.R;
import com.pimpcar.Widgets.CircleImageView;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

public class NotificationViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.not_person_pic)
    public CircleImageView not_person_pic;

    @BindView(R.id.user_name)
    public AppCompatTextView user_name;

    @BindView(R.id.user_action_text)
    public AppCompatTextView user_action_text;

    @BindView(R.id.notification_post_image)
    public AppCompatImageView notification_post_image;

    @BindView(R.id.user_follow_button)
    public AppCompatTextView user_follow_button;

    @BindView(R.id.user_un_follow_button)
    public AppCompatTextView user_un_follow_button;

    @BindView(R.id.user_follow_button_rl)
    public RelativeLayout user_follow_button_rl;

    public NotificationViewHolder(@NonNull View itemView, List<NotificationResponseBody.DataBean.NotificationBean> notificationBeans, Context context) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        Timber.d("--------###### " + notificationBeans.size());
        Timber.d("--------###### " + getAdapterPosition());

        user_un_follow_button.setOnClickListener(v -> {
            user_un_follow_button.setVisibility(View.GONE);
            user_follow_button.setVisibility(View.VISIBLE);
            EventBus.getDefault().post(new UnfollowUser(notificationBeans.get(getAdapterPosition()).getUser_caused_notification_uid()));

        });

        user_follow_button.setOnClickListener(v -> {
            user_follow_button.setVisibility(View.GONE);
            user_un_follow_button.setVisibility(View.VISIBLE);
            EventBus.getDefault().post(new FollowUser(notificationBeans.get(getAdapterPosition()).getUser_caused_notification_uid()));
        });

    }

}
