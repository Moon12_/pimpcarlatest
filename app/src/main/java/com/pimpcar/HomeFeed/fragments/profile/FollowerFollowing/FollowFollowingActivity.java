package com.pimpcar.HomeFeed.fragments.profile.FollowerFollowing;

import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.pimpcar.HomeFeed.fragments.profile.FollowerFollowing.adapter.FFPageAdapter;
import com.pimpcar.HomeFeed.fragments.search.Interface.IDataCallback;
import com.pimpcar.HomeFeed.fragments.search.Interface.IFragmentListener;
import com.pimpcar.HomeFeed.fragments.search.Interface.ISearch;
import com.pimpcar.R;

import java.util.ArrayList;

public class FollowFollowingActivity extends AppCompatActivity implements IFragmentListener, View.OnClickListener {
    TabLayout tabLayout;
    ViewPager viewPager;
    ImageView imageView7;
    String tagName = "", user_name = "", user_id = "";
    TextView textView12;
    ArrayList<ISearch> iSearch = new ArrayList<>();
    ArrayList<String> listData = null;
    IDataCallback iDataCallback = null;
    private FFPageAdapter adapter;

    public void setiDataCallback(IDataCallback iDataCallback) {
        this.iDataCallback = iDataCallback;
        iDataCallback.onFragmentCreated(listData);
    }

    @Override
    public void addiSearch(ISearch iSearch) {
        this.iSearch.add(iSearch);
    }

    @Override
    public void removeISearch(ISearch iSearch) {
        this.iSearch.remove(iSearch);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_follow_following);
        listData = new ArrayList<>();
        tagName = getIntent().getStringExtra("checkingState");
        user_name = getIntent().getStringExtra("userName");
        user_id = getIntent().getStringExtra("userID");

        textView12 = (TextView) findViewById(R.id.textView12);
        textView12.setText(user_name);

        imageView7 = findViewById(R.id.imageView7);
        imageView7.setOnClickListener(this);

        tabLayout = (TabLayout) findViewById(R.id.tabs_f);
        tabLayout.addTab(tabLayout.newTab().setText("Followers"));
        tabLayout.addTab(tabLayout.newTab().setText("Following"));

        viewPager = (ViewPager) findViewById(R.id.viewPager_f);

        adapter = new FFPageAdapter(getSupportFragmentManager(), "user_id");
        viewPager.setAdapter(adapter);

        if (!tagName.equals("follwer")) {
            viewPager.setCurrentItem(1);
        }
        tabLayout.setupWithViewPager(viewPager);


        adapter.setTextQueryChanged(user_id);

        for (ISearch iSearchLocal : iSearch)
            iSearchLocal.onTextQuery(user_id);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.imageView7) {
            finish();
        }
    }


}
