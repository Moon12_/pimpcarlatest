package com.pimpcar.HomeFeed.fragments.profile.FollowerFollowing.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.pimpcar.HomeFeed.fragments.profile.FollowerFollowing.Fragments.FollowFragment;
import com.pimpcar.HomeFeed.fragments.profile.FollowerFollowing.Fragments.FollowingFragment;

public class FFPageAdapter extends FragmentStatePagerAdapter {

    String user_id = "";

    public FFPageAdapter(FragmentManager fm, String id) {
        super(fm);
        this.user_id = id;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                FollowFragment tab1 = FollowFragment.newInstance(user_id);
                return tab1;
            case 1:
                FollowingFragment tab2 = FollowingFragment.newInstance(user_id);
                return tab2;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        if (position == 0) {
            title = "Followers";
        } else if (position == 1) {
            title = "Following";
        }
        return title;
    }

    public void setTextQueryChanged(String newText) {
        user_id = newText;
    }
}
