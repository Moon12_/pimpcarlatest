package com.pimpcar.HomeFeed.fragments.profile.FollowerFollowing.adapter;

import android.content.Context;
import android.view.View;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.pimpcar.HomeFeed.fragments.profile.pojos.FollowedAccountResponse;
import com.pimpcar.HomeFeed.fragments.profile.pojos.FollowingAccountResponse;
import com.pimpcar.Network.ApiClient;
import com.pimpcar.Network.ApiService;
import com.pimpcar.R;
import com.pimpcar.Widgets.CircleImageView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.CompositeDisposable;

class UserFeedViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.user_profile_pic)
    public CircleImageView user_profile_pic;

    @BindView(R.id.user_name_full_name)
    public AppCompatTextView user_name_full_name;

    @BindView(R.id.user_name)
    public AppCompatTextView user_name;

//    @BindView(R.id.user_follow_button_rl)
//    public RelativeLayout user_follow_button_rl;

    @BindView(R.id.user_follow_button)
    public AppCompatTextView user_follow_button;

    @BindView(R.id.user_un_follow_button)
    public AppCompatTextView user_un_follow_button;

//    @BindView(R.id.user_name_rl)
//    public RelativeLayout user_name_rl;
//

    private ApiService apiService;
    private CompositeDisposable disposable = new CompositeDisposable();

    public UserFeedViewHolder(View itemView, Context mContext) {
        super(itemView);

        ButterKnife.bind(this, itemView);


        apiService = ApiClient.getClient(mContext.getApplicationContext()).create(ApiService.class);


    }

}
