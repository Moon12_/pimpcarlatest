package com.pimpcar.HomeFeed.fragments.profile.comments.eventbus.event_models;

public class PostLike {

    private String post_id;

    public PostLike(String post_id) {

        this.post_id = post_id;
    }


    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }
}
