package com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class PostAllCommentsBodyModel implements Parcelable {
    /**
     * data : {"attributes":{"post_id":"RZEJX2MwH1564054176241","page_no":2}}
     */

    public PostAllCommentsBodyModel(){

    }

    @SerializedName("data")
    private DataBean data;

    protected PostAllCommentsBodyModel(Parcel in) {
    }

    public static final Creator<PostAllCommentsBodyModel> CREATOR = new Creator<PostAllCommentsBodyModel>() {
        @Override
        public PostAllCommentsBodyModel createFromParcel(Parcel in) {
            return new PostAllCommentsBodyModel(in);
        }

        @Override
        public PostAllCommentsBodyModel[] newArray(int size) {
            return new PostAllCommentsBodyModel[size];
        }
    };

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public static class DataBean implements Parcelable {


        public DataBean(){

        }

        /**
         * attributes : {"post_id":"RZEJX2MwH1564054176241","page_no":2}
         */

        @SerializedName("attributes")
        private AttributesBean attributes;

        protected DataBean(Parcel in) {
        }

        public static final Creator<DataBean> CREATOR = new Creator<DataBean>() {
            @Override
            public DataBean createFromParcel(Parcel in) {
                return new DataBean(in);
            }

            @Override
            public DataBean[] newArray(int size) {
                return new DataBean[size];
            }
        };

        public AttributesBean getAttributes() {
            return attributes;
        }

        public void setAttributes(AttributesBean attributes) {
            this.attributes = attributes;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
        }

        public static class AttributesBean implements Parcelable{


            public AttributesBean(){

            }

            /**
             * post_id : RZEJX2MwH1564054176241
             * page_no : 2
             */

            @SerializedName("post_id")
            private String post_id;
            @SerializedName("page_no")
            private int page_no;

            protected AttributesBean(Parcel in) {
                post_id = in.readString();
                page_no = in.readInt();
            }

            public static final Creator<AttributesBean> CREATOR = new Creator<AttributesBean>() {
                @Override
                public AttributesBean createFromParcel(Parcel in) {
                    return new AttributesBean(in);
                }

                @Override
                public AttributesBean[] newArray(int size) {
                    return new AttributesBean[size];
                }
            };

            public String getPost_id() {
                return post_id;
            }

            public void setPost_id(String post_id) {
                this.post_id = post_id;
            }

            public int getPage_no() {
                return page_no;
            }

            public void setPage_no(int page_no) {
                this.page_no = page_no;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(post_id);
                dest.writeInt(page_no);
            }
        }
    }
}
