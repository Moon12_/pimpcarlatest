package com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans;

import com.google.gson.annotations.SerializedName;

public class UnFollowUserRequestBodyModel {
    /**
     * data : {"attributes":{"to_unfollow":"27f56235-371b-5106-a169-4cb86ff78d3e"}}
     */

    @SerializedName("data")
    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * attributes : {"to_unfollow":"27f56235-371b-5106-a169-4cb86ff78d3e"}
         */

        @SerializedName("attributes")
        private AttributesBean attributes;

        public AttributesBean getAttributes() {
            return attributes;
        }

        public void setAttributes(AttributesBean attributes) {
            this.attributes = attributes;
        }

        public static class AttributesBean {
            /**
             * to_unfollow : 27f56235-371b-5106-a169-4cb86ff78d3e
             */

            @SerializedName("to_unfollow")
            private String to_unfollow;

            public String getTo_unfollow() {
                return to_unfollow;
            }

            public void setTo_unfollow(String to_unfollow) {
                this.to_unfollow = to_unfollow;
            }
        }
    }
}
