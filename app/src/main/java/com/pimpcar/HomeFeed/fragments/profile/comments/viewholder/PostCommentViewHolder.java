package com.pimpcar.HomeFeed.fragments.profile.comments.viewholder;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import com.pimpcar.R;
import com.pimpcar.Widgets.CircleImageView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PostCommentViewHolder extends RecyclerView.ViewHolder {


    @BindView(R.id.user_profile_pic_rounded)
    public CircleImageView user_profile_pic_rounded;

    @BindView(R.id.user_name_tv)
    public AppCompatTextView user_name_tv;

    @BindView(R.id.user_post_description)
    public AppCompatTextView user_post_description;

    @BindView(R.id.comment_time)
    public AppCompatTextView comment_time;

    @BindView(R.id.comment_likes)
    public AppCompatTextView comment_likes;

    @BindView(R.id.comment_reply)
    public AppCompatTextView kcomment_reply;

    @BindView(R.id.comment_like_icon)
    public AppCompatImageView comment_like_icon;


    public PostCommentViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);

        comment_like_icon.setOnClickListener(v -> {


        });

    }
}
