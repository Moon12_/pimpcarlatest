package com.pimpcar.HomeFeed.fragments.profile.pojos;

import com.google.gson.annotations.SerializedName;

public class ChangeUserNameRequestBody {
    /**
     * data : {"attributes":{"username":"Hello brother"}}
     */

    @SerializedName("data")
    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * attributes : {"username":"Hello brother"}
         */

        @SerializedName("attributes")
        private AttributesBean attributes;

        public AttributesBean getAttributes() {
            return attributes;
        }

        public void setAttributes(AttributesBean attributes) {
            this.attributes = attributes;
        }

        public static class AttributesBean {
            /**
             * username : Hello brother
             */

            @SerializedName("username")
            private String username;

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }
        }
    }
}
