package com.pimpcar.HomeFeed.fragments.profile.pojos;

import com.google.gson.annotations.SerializedName;

public class ChangeUserNameResponse {
    /**
     * data : {"message":"username updated successfully","user_name":{"user_name":"Protocol"}}
     */

    @SerializedName("data")
    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * message : username updated successfully
         * user_name : {"user_name":"Protocol"}
         */

        @SerializedName("message")
        private String message;
        @SerializedName("user_name")
        private UserNameBean user_name;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public UserNameBean getUser_name() {
            return user_name;
        }

        public void setUser_name(UserNameBean user_name) {
            this.user_name = user_name;
        }

        public static class UserNameBean {
            /**
             * user_name : Protocol
             */

            @SerializedName("user_name")
            private String user_name;

            public String getUser_name() {
                return user_name;
            }

            public void setUser_name(String user_name) {
                this.user_name = user_name;
            }
        }
    }
}
