package com.pimpcar.HomeFeed.fragments.profile.pojos;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FollowingAccountRequestBody {
    @SerializedName("data")
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data {

        @SerializedName("attributes")
        private AttributesBean attributes;

        public AttributesBean getAttributes() {
            return attributes;
        }

        public void setAttributes(AttributesBean attributes) {
            this.attributes = attributes;
        }

        public static class AttributesBean {
            @SerializedName("message")
            private String message;
            @SerializedName("following_accounts")
            private List<FollowedAccountResponse> followingAccounts = null;


            @SerializedName("page_number")
            private Integer nextPage;

            public String getMessage() {
                return message;
            }

            public void setMessage(String message) {
                this.message = message;
            }

            public Integer getNextPage() {
                return nextPage;
            }

            public void setNextPage(Integer nextPage) {
                this.nextPage = nextPage;
            }

            public List<FollowedAccountResponse> getFollowingAccounts() {
                return followingAccounts;
            }

            public void setFollowingAccounts(List<FollowedAccountResponse> followingAccounts) {
                this.followingAccounts = followingAccounts;
            }
        }


    }
}
