package com.pimpcar.HomeFeed.fragments.profile.profile_pager_fragments;

import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;
import com.pimpcar.HomeFeed.fragments.profile.comments.eventbus.event_models.LikeOnPostEvent;
import com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans.PostAllCommentResponseModel;
import com.pimpcar.HomeFeed.fragments.profile.profile_pager_fragments.adapters.UserPostRVAdapter;
import com.pimpcar.Network.ApiClient;
import com.pimpcar.Network.ApiService;
import com.pimpcar.Network.model.posts.PostLikeDislikeRequestBody;
import com.pimpcar.Network.model.posts.PostLikeDislikeResponse;
import com.pimpcar.Network.model.posts.UserPostBodyResponse;
import com.pimpcar.Network.model.posts.UserPostsBodyRequest;
import com.pimpcar.R;
import com.pimpcar.UserAuthentication.eventbus.models.ShowToastEventModel;
import com.pimpcar.Widgets.InfiniteAdapter.InfiniteAdapter;
import com.pimpcar.utils.Constants;
import com.pimpcar.utils.HelperMethods;
import com.pimpcar.utils.NetworkResponseMessages;
import com.pimpcar.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import timber.log.Timber;

public class ListPicsFragment extends Fragment {

    @BindView(R.id.recycler_view_post_paging)
    RecyclerView recycler_view_post_paging;
    @BindView(R.id.shimmer_view_container)
    ShimmerFrameLayout mShimmerViewContainer;

    LinearLayoutManager linearLayoutManager;
    int clicktiem = 0;
    private Unbinder unbinder;
    private int is_next_page_available;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int START_PAGE = 1;
    private int current_page = START_PAGE;
    private ArrayList<UserPostBodyResponse.DataBean.PostsBean> user_post_data = new ArrayList<>();
    private UserPostRVAdapter userPostRVAdapter;
    private ApiService apiService;
    private CompositeDisposable disposable = new CompositeDisposable();
    private int first_next_page;


    //    @Override
//    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
//        super.onActivityCreated(savedInstanceState);
//        if (getActivity() != null) {
//
//        }
//    }
    private String user_type;
    private InfiniteAdapter.OnLoadMoreListener onLoadMoreListener =
            new InfiniteAdapter.OnLoadMoreListener() {
                @Override
                public void onLoadMore() {
                    UserPostsBodyRequest userPostsBodyRequest = HelperMethods.create_user_profile_post_get(first_next_page);
                    if (disposable != null && apiService != null) {
                        disposable.add(apiService.getUserPosts(HelperMethods.get_user_jwt_key(), userPostsBodyRequest)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribeWith(new DisposableSingleObserver<UserPostBodyResponse>() {
                                    @Override
                                    public void onSuccess(UserPostBodyResponse s) {
                                        if (s.getData().getPosts() != null && s.getData().getPosts().size() > 0) {
                                            for (int i = 0; i < s.getData().getPosts().size(); i++) {
                                                user_post_data.add(s.getData().getPosts().get(i));
                                                userPostRVAdapter.notifyItemInserted(user_post_data.size() - 1);
                                            }
                                            userPostRVAdapter.moreDataLoaded(user_post_data.size(), s.getData().getPosts().size());


                                            if (s.getData().getNext_page() == 1) {
                                                first_next_page = first_next_page + 1;
                                                userPostRVAdapter.setShouldLoadMore(true);
                                            } else {
                                                userPostRVAdapter.setShouldLoadMore(false);
                                            }

                                        } else {
                                            userPostRVAdapter.setShouldLoadMore(false);
                                        }
                                    }

                                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                                    @Override
                                    public void onError(Throwable e) {
                                        if (e instanceof HttpException) {
                                            ResponseBody body = ((HttpException) e).response().errorBody();
                                            try {
                                                String error_body = body.string();
                                                String error = Utils.get_error_from_response(error_body);

                                                if (error.contentEquals(NetworkResponseMessages.USER_EMAIL_NOT_VERIFIED)) {

                                                }

                                                EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, error.toUpperCase()));
                                            } catch (IOException e1) {
                                                e1.printStackTrace();
                                            }
                                        }
                                    }
                                }));


                    }
                }
            };
    private PostAllCommentResponseModel postAllCommentResponseModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root_view = inflater.inflate(R.layout.fragment_all_post, container, false);
        unbinder = ButterKnife.bind(this, root_view);
        apiService = ApiClient.getClient(getActivity().getApplicationContext()).create(ApiService.class);
        user_post_data = getArguments().getParcelableArrayList(Constants.USER_POST_LIST);
        is_next_page_available = getArguments().getInt(Constants.USER_NEXT_PAGE_DETAILS);
        setUser_type(getArguments().getString(Constants.USER_TYPE));

        if (user_post_data == null) {
            recycler_view_post_paging.setVisibility(View.GONE);
            mShimmerViewContainer.setVisibility(View.VISIBLE);
            mShimmerViewContainer.startShimmerAnimation();
        } else {
            mShimmerViewContainer.setVisibility(View.GONE);
            mShimmerViewContainer.stopShimmerAnimation();
            recycler_view_post_paging.setVisibility(View.VISIBLE);
        }

        setup_data_work();
        return root_view;
    }

    private void setup_data_work() {
        userPostRVAdapter = new UserPostRVAdapter(getActivity(), user_post_data);
        recycler_view_post_paging.setAdapter(userPostRVAdapter);
        recycler_view_post_paging.setLayoutManager(new LinearLayoutManager(getActivity()));
        userPostRVAdapter.setOnLoadMoreListener(onLoadMoreListener);
    }

    @Override
    public void onDestroyView() {
        if (unbinder != null) {
            unbinder.unbind();
        }
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Subscribe
    public void likeDislikePost(LikeOnPostEvent postLike) {
        clicktiem = clicktiem + 1;
        Timber.d("clicked for the " + clicktiem + " time" + postLike.getPost_id());

        PostLikeDislikeRequestBody post_like_dislike_body = HelperMethods.create_post_like_dislike_body(postLike.getPost_id());
        disposable.add(apiService.postLikeDislikeOnPost(HelperMethods.get_user_jwt_key(), post_like_dislike_body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<PostLikeDislikeResponse>() {
                    @Override
                    public void onSuccess(PostLikeDislikeResponse s) {
                        Timber.d("post like dislike done message " + s.getData().getMessage());
                        show_toast(s.getData().getMessage());
                    }

                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof HttpException) {
                            ResponseBody body = ((HttpException) e).response().errorBody();
                            try {
                                String error_body = body.string();
                                String error = Utils.get_error_from_response(error_body);

                                if (error.contentEquals(NetworkResponseMessages.USER_EMAIL_NOT_VERIFIED)) {

                                }

                                EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, error.toUpperCase()));
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                }));


    }

    private void show_toast(String message) {
        // Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmerAnimation();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        super.onPause();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }
}
