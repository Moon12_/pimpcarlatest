package com.pimpcar.HomeFeed.fragments.profile.profile_pager_fragments.viewholder;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import com.pimpcar.R;
import com.pimpcar.Widgets.AspectRatioImageViewView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GridFeedViewHolder extends RecyclerView.ViewHolder {


    @BindView(R.id.grid_view_image_view)
    public AspectRatioImageViewView grid_view_image_view;

    public GridFeedViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
