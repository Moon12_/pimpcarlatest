package com.pimpcar.HomeFeed.fragments.profile.profile_pager_fragments.viewholder;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import com.pimpcar.HomeFeed.fragments.profile.stories.view.StoryView;
import com.pimpcar.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UserStoriesViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.storyView)
    public StoryView user_stories_portfolio;

    @BindView(R.id.user_stories_caption)
    public AppCompatTextView user_stories_caption;


    public UserStoriesViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
