package com.pimpcar.HomeFeed.fragments.profile.stories.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.SeekBar;

import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.OnColorSelectedListener;
import com.flask.colorpicker.builder.ColorPickerClickListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;
import com.pimpcar.HomeFeed.fragments.profile.stories.models.PostUserStoriesRequestBody;
import com.pimpcar.HomeFeed.fragments.profile.stories.models.PostUserStoriesResponseBody;
import com.pimpcar.Network.ApiClient;
import com.pimpcar.Network.ApiService;
import com.pimpcar.R;
import com.pimpcar.Widgets.progressbar.KProgressHUD;
import com.pimpcar.utils.HelperMethods;
import com.pimpcar.utils.Utils;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;

public class StoryCreateActivity extends AppCompatActivity implements View.OnClickListener, View.OnTouchListener {


    public static int size;
    @BindView(R.id.edittext)
    ImageButton editButton;
    @BindView(R.id.textfont)
    ImageButton fontButton;
    @BindView(R.id.textcolor)
    ImageButton colorButton;
    @BindView(R.id.textnormal)
    ImageButton normalButton;
    @BindView(R.id.image_container)
    AppCompatImageView image_container;
    @BindView(R.id.txtView)
    AppCompatTextView txtView;
    @BindView(R.id.user_image_caption)
    AppCompatEditText user_image_caption;
    @BindView(R.id.upload_story_to_server)
    AppCompatTextView upload_story_to_server;
    @BindView(R.id.seekBar1)
    SeekBar textBar;
    @BindView(R.id.parent_relative_layout_for_image_edit)
    RelativeLayout parent_relative_layout_for_image_edit;
    float dX;
    float dY;
    int lastAction;
    private LayoutInflater layoutInflater;
    private String typefaceFont[] = {"Adrip.ttf", "algerian.ttf", "Ananda Akchyar bold.ttf", "Anglo.ttf", "B20Sans.ttf"};
    private Bitmap main_image;
    private String selected_image_path;
    private Bitmap main_bitmap_image;
    private ApiService apiService;
    private CompositeDisposable disposable = new CompositeDisposable();
    private KProgressHUD hud;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.story_image_editor);
        ButterKnife.bind(this);

        parent_relative_layout_for_image_edit.setDrawingCacheEnabled(true);
        parent_relative_layout_for_image_edit.buildDrawingCache(true);

        apiService = ApiClient.getClient(getApplicationContext()).create(ApiService.class);


        editButton.setOnClickListener(this);
        fontButton.setOnClickListener(this);
        colorButton.setOnClickListener(this);
        normalButton.setOnClickListener(this);


        byte[] byteArray = getIntent().getByteArrayExtra("story_image");

        Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);

        image_container.setImageBitmap(bmp);

//        Bundle bundle = getIntent().getExtras();
//
//        selected_image_path = bundle.getString("selected_image");
//
//
//        if (selected_image_path != null) {
//
//            File imageFile = new File(selected_image_path);
//            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
//            main_bitmap_image = BitmapFactory.decodeFile(imageFile.getPath(), bmOptions);
//        }
//
//        if (main_bitmap_image != null)
//            image_container.setImageBitmap(main_bitmap_image);
//

        txtView.setOnTouchListener(this);


        textBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                txtView.setTextSize(progress);
                size = progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        user_image_caption.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                txtView.setText("" + s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }

    @OnClick(R.id.upload_story_to_server)
    public void submit_story_to_server() {

        show_progress_dialog();
        String caption = user_image_caption.getText().toString();
        Bitmap image_bitmap = Bitmap.createBitmap(parent_relative_layout_for_image_edit.getDrawingCache());


        PostUserStoriesRequestBody request_body_for_stories_post = HelperMethods.create_request_body_for_stories_post(caption, image_bitmap);
        disposable.add(apiService.postUserStories(HelperMethods.get_user_jwt_key(), request_body_for_stories_post)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<PostUserStoriesResponseBody>() {
                    @Override
                    public void onSuccess(PostUserStoriesResponseBody s) {

                        dismiss_progress_dialog();
                        if (s.getData().getMessage().contentEquals("stories updated to user"))
                            onBackPressed();
                    }

                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onError(Throwable e) {

                        dismiss_progress_dialog();
                        if (e instanceof HttpException) {
                            ResponseBody body = ((HttpException) e).response().errorBody();
                            try {
                                String error_body = body.string();
                                String error = Utils.get_error_from_response(error_body);
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                }));


    }

    @Override
    public void onClick(View v) {


        switch (v.getId()) {
            case R.id.edittext:
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
                alertDialog.setMessage("Add Caption Text");

                final EditText input = new EditText(this);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT);
                input.setLayoutParams(lp);
                alertDialog.setView(input);
                CharSequence myinput = txtView.getText();
                input.setText(myinput);
                alertDialog.setPositiveButton("YES",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                CharSequence mynewIP = input.getText();
                                txtView.setText(mynewIP);
                            }
                        });

                alertDialog.setNegativeButton("NO",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                alertDialog.show();


                break;
            case R.id.textfont:
                break;
            case R.id.textcolor:
                ColorPickerDialogBuilder
                        .with(this)
                        .setTitle("Choose color")
                        .initialColor(Color.BLUE)
                        .wheelType(ColorPickerView.WHEEL_TYPE.FLOWER)
                        .density(12)
                        .setOnColorSelectedListener(new OnColorSelectedListener() {
                            @Override
                            public void onColorSelected(int selectedColor) {
                                txtView.setTextColor(selectedColor);
                            }
                        })
                        .setPositiveButton("ok", new ColorPickerClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int selectedColor, Integer[] allColors) {
                            }
                        })
                        .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .build()
                        .show();
                break;
            case R.id.textnormal:
                final Dialog dialog = new Dialog(this);
                dialog.setContentView(R.layout.normal_font_dialog);
                dialog.setTitle("Choose Font");
                dialog.setCancelable(true);
                final RadioButton rd1 = (RadioButton) dialog.findViewById(R.id.normal);
                final RadioButton rd2 = (RadioButton) dialog.findViewById(R.id.bold);
                final RadioButton rd3 = (RadioButton) dialog.findViewById(R.id.italic);
                final RadioButton rd4 = (RadioButton) dialog.findViewById(R.id.bolditalic);
                rd1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        txtView.setTypeface(Typeface.create(txtView.getTypeface(), Typeface.NORMAL));
                        dialog.dismiss();
                    }

                });
                rd2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        txtView.setTypeface(txtView.getTypeface(), Typeface.BOLD);
                        dialog.dismiss();
                    }
                });
                rd3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        txtView.setTypeface(txtView.getTypeface(), Typeface.ITALIC);
                        dialog.dismiss();
                    }
                });
                rd4.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        txtView.setTypeface(txtView.getTypeface(), Typeface.BOLD_ITALIC);
                        dialog.dismiss();
                    }
                });

                dialog.show();
                break;
        }

    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {

        switch (event.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
                dX = view.getX() - event.getRawX();
                dY = view.getY() - event.getRawY();
                lastAction = MotionEvent.ACTION_DOWN;
                break;

            case MotionEvent.ACTION_MOVE:
                view.setY(event.getRawY() + dY);
                view.setX(event.getRawX() + dX);
                lastAction = MotionEvent.ACTION_MOVE;
                break;

            case MotionEvent.ACTION_UP:
                if (lastAction == MotionEvent.ACTION_DOWN)
                    break;

            default:
                return false;
        }


        return true;
    }

    public void show_progress_dialog() {
        hud = KProgressHUD.create(StoryCreateActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE);
        hud.setSize(50, 50);
        hud.show();
    }

    public void dismiss_progress_dialog() {
        if (hud != null && hud.isShowing())
            hud.dismiss();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
