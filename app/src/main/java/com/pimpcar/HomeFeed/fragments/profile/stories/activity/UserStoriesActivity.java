package com.pimpcar.HomeFeed.fragments.profile.stories.activity;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.pimpcar.HomeFeed.fragments.profile.stories.view.StoryModel;
import com.pimpcar.HomeFeed.fragments.profile.stories.view.StoryView;
import com.pimpcar.R;
import com.pimpcar.utils.Constants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UserStoriesActivity extends AppCompatActivity {

    @BindView(R.id.storyView)
    StoryView storyView;

    ArrayList<String> all_user_stories;
    private String user_story_caption;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_stories_view);
        ButterKnife.bind(this);

        if (getIntent() != null) {
            all_user_stories = getIntent().getStringArrayListExtra(Constants.USER_STORY_ARRAY_LIST);
            user_story_caption = getIntent().getStringExtra(Constants.USER_STORY_CAPTION);
        }


        if (all_user_stories != null && all_user_stories.size() > 0) {
            storyView.resetStoryVisits();
            ArrayList<StoryModel> uris = new ArrayList<>();

            for (int i = 0; i < all_user_stories.size(); i++) {
                uris.add(new StoryModel(all_user_stories.get(i), user_story_caption, "12:00  PM"));
            }
            storyView.setImageUris(uris);

        }
    }
}
