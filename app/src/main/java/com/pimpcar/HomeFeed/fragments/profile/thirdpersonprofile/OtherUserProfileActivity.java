package com.pimpcar.HomeFeed.fragments.profile.thirdpersonprofile;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import com.google.android.material.navigation.NavigationView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;
import com.pimpcar.HomeFeed.fragments.profile.FollowerFollowing.FollowFollowingActivity;
import com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans.FollowUserRequestBodyModel;
import com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans.FollowUserResponseModel;
import com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans.UnFollowUserRequestBodyModel;
import com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans.UnFollowUserResponseModel;
import com.pimpcar.HomeFeed.fragments.profile.profile_pager_fragments.GridviewPicsFragment;
import com.pimpcar.HomeFeed.fragments.profile.profile_pager_fragments.UserTaggedFragment;
import com.pimpcar.HomeFeed.fragments.profile.profile_pager_fragments.adapters.UserStoriesAdapter;
import com.pimpcar.HomeFeed.fragments.profile.stories.models.GetUserStoriesRequestBody;
import com.pimpcar.HomeFeed.fragments.profile.stories.models.GetUserStoriesResponse;
import com.pimpcar.HomeFeed.fragments.profile.thirdpersonprofile.pojos.ThirdPersonPostBodyRequest;
import com.pimpcar.HomeFeed.fragments.profile.thirdpersonprofile.pojos.ThirdPersonProfileRequestBody;
import com.pimpcar.HomeFeed.fragments.profile.thirdpersonprofile.pojos.ThirdPersonProfileResponse;
import com.pimpcar.HomeFeed.fragments.profile.thirdpersonprofile.pojos.ThirdPersonStoriesBodyRequest;
import com.pimpcar.HomeFeed.fragments.profile.thirdpersonprofile.profile_pager_fragments.ListPicsFragment;
import com.pimpcar.Network.ApiClient;
import com.pimpcar.Network.ApiService;
import com.pimpcar.Network.model.posts.UserPostBodyResponse;
import com.pimpcar.R;
import com.pimpcar.UserAuthentication.eventbus.models.ShowToastEventModel;
import com.pimpcar.Widgets.CircleImageView;
import com.pimpcar.Widgets.InfiniteAdapter.InfiniteAdapter;
import com.pimpcar.Widgets.progressbar.KProgressHUD;
import com.pimpcar.Widgets.staticviewpager.NonSwipeableViewPager;
import com.pimpcar.utils.Constants;
import com.pimpcar.utils.HelperMethods;
import com.pimpcar.utils.NetworkResponseMessages;
import com.pimpcar.utils.ResourcesUtil;
import com.pimpcar.utils.Utils;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import es.dmoral.toasty.Toasty;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import timber.log.Timber;

public class OtherUserProfileActivity extends AppCompatActivity implements View.OnClickListener {


    @BindView(R.id.portfolio_pic)
    CircleImageView portfolio_pic;
    @BindView(R.id.user_name_tv)
    AppCompatTextView user_name_tv;
    @BindView(R.id.user_bio_quite_tv)
    AppCompatTextView user_bio_quite_tv;
    @BindView(R.id.user_follow_button)
    AppCompatTextView user_follow_button;
    @BindView(R.id.user_un_follow_button)
    AppCompatTextView user_un_follow_button;
    @BindView(R.id.total_post_tv)
    AppCompatTextView total_post_tv;
    @BindView(R.id.total_followers_tv)
    AppCompatTextView total_followers_tv;
    @BindView(R.id.total_following_tv)
    AppCompatTextView total_following_tv;
    @BindView(R.id.user_stories_rv)
    RecyclerView user_stories_rv;
    @BindView(R.id.ic_gridview_thumbnail_data)
    AppCompatImageView ic_gridview_thumbnail_data;
    @BindView(R.id.ic_list_user_data)
    AppCompatImageView ic_list_user_data;
    @BindView(R.id.user_tagged_posts)
    AppCompatImageView user_tagged_posts;
    @BindView(R.id.view_pager)
    NonSwipeableViewPager view_pager;
    @BindView(R.id.new_stories_create)
    RelativeLayout new_stories_create;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer_layout;
    @BindView(R.id.drawer_layout_open_close_button)
    AppCompatImageView drawer_layout_open_close_button;
    @BindView(R.id.add_remove_image_profile)
    View add_remove_image_profile;
    @BindView(R.id.navigation_view_drawer)
    NavigationView navigation_view_drawer;
    @BindView(R.id.user_name_edit)
    AppCompatImageView user_name_edit;
    @BindView(R.id.user_status_update)
    AppCompatImageView user_status_update;
    @BindView(R.id.backbutton_profile)
    AppCompatImageView backbutton_profile;
    @BindView(R.id.follower)
    LinearLayout follower;
    @BindView(R.id.following)
    LinearLayout following;

    private Unbinder unbinder;
    private String user_id;
    private ApiService apiService;
    private CompositeDisposable disposable = new CompositeDisposable();

    private List<Fragment> fragments;// used for ViewPager adapter
    private VpAdapter adapter;
    private int first_next_page;
    private ArrayList<UserPostBodyResponse.DataBean.PostsBean> total_user_post_infilated = new ArrayList<>();
    private ThirdPersonProfileResponse user_profile_data;
    private List<String> user_followers_list;
    private boolean is_user_follower;
    private int stories_next_page_number;
    private UserStoriesAdapter userStoriesAdapter;
    private ArrayList<GetUserStoriesResponse.DataBean.StoriesBean> user_all_stories;
    private KProgressHUD hud;

    String user_name="";
    private InfiniteAdapter.OnLoadMoreListener onLoadMoreListener =
            new InfiniteAdapter.OnLoadMoreListener() {
                @Override
                public void onLoadMore() {
                    GetUserStoriesRequestBody getUserStoriesRequestBody = HelperMethods.create_request_body_for_stories(getStories_next_page_number());
                    if (disposable != null && apiService != null) {
                        disposable.add(apiService.getUserStories(HelperMethods.get_user_jwt_key(), getUserStoriesRequestBody)
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribeWith(new DisposableSingleObserver<GetUserStoriesResponse>() {
                                    @Override
                                    public void onSuccess(GetUserStoriesResponse s) {

                                        if (s.getData().getStories().size() > 0) {
                                            for (int i = 0; i < s.getData().getStories().size(); i++) {
                                                user_all_stories.add(s.getData().getStories().get(i));

                                                userStoriesAdapter.notifyItemInserted(user_all_stories.size() - 1);
                                            }
                                            userStoriesAdapter.moreDataLoaded(user_all_stories.size(), s.getData().getStories().size());

                                            if (s.getData().getNext_page() == 1) {
                                                increase_page_number();
                                                userStoriesAdapter.setShouldLoadMore(true);
                                            } else {
                                                userStoriesAdapter.setShouldLoadMore(false);

                                            }

                                        } else {
                                            userStoriesAdapter.setShouldLoadMore(false);
                                        }
                                    }

                                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                                    @Override
                                    public void onError(Throwable e) {
                                        if (e instanceof HttpException) {
                                            ResponseBody body = ((HttpException) e).response().errorBody();
                                            try {
                                                String error_body = body.string();
                                                String error = Utils.get_error_from_response(error_body);

                                                if (error.contentEquals(NetworkResponseMessages.USER_EMAIL_NOT_VERIFIED)) {

                                                }

                                                EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, error.toUpperCase()));
                                            } catch (IOException e1) {
                                                e1.printStackTrace();
                                            }
                                        }
                                    }
                                }));


                    }
                }
            };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_layout_test);
        ButterKnife.bind(this);
        apiService = ApiClient.getClient(getApplicationContext()).create(ApiService.class);
        manage_view();
        if (getIntent() != null) {
            setUser_id(getIntent().getStringExtra(Constants.THIRD_USER_ID));
            Timber.d("g");
            Log.d("checking",""+getUser_id());
            get_user_profile(getUser_id());
            get_user_first_post_list();
            get_user_stories();
        }
        backbutton_profile.setOnClickListener(v -> {

            finish();

        });

//        follower.setOnClickListener(this);
//        following.setOnClickListener(this);
    }

    private void get_user_first_post_list() {


        ThirdPersonPostBodyRequest userPostsBodyRequest = HelperMethods.create_third_person_post_req(getUser_id(), first_next_page);
        disposable.add(apiService.getthirdPersonPosts(HelperMethods.get_user_jwt_key(), userPostsBodyRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<UserPostBodyResponse>() {
                    @Override
                    public void onSuccess(UserPostBodyResponse s) {
                        total_user_post_infilated = s.getData().getPosts();
                        first_next_page = s.getData().getNext_page();
                        init_view_pager_view();
                    }

                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof HttpException) {
                            ResponseBody body = ((HttpException) e).response().errorBody();
                            try {
                                String error_body = body.string();
                                String error = Utils.get_error_from_response(error_body);
                                EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, error.toUpperCase()));
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                }));

    }

    private void manage_view() {
        user_status_update.setVisibility(View.GONE);
        user_name_edit.setVisibility(View.GONE);
        user_follow_button.setVisibility(View.VISIBLE);
        drawer_layout_open_close_button.setVisibility(View.GONE);
        drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        add_remove_image_profile.setVisibility(View.GONE);
        new_stories_create.setVisibility(View.GONE);
        backbutton_profile.setVisibility(View.VISIBLE);
    }

    private void get_user_profile(String user_id) {
        ThirdPersonProfileRequestBody userPostsBodyRequest = HelperMethods.create_third_person_profile_req_body(user_id);
        disposable.add(apiService.getThirdPersonProfile(HelperMethods.get_user_jwt_key(), userPostsBodyRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<ThirdPersonProfileResponse>() {
                    @Override
                    public void onSuccess(ThirdPersonProfileResponse s) {

                        user_profile_data = s;
                        set_user_profile_data();

                    }

                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof HttpException) {
                            ResponseBody body = ((HttpException) e).response().errorBody();
                            try {
                                String error_body = body.string();
                                String error = Utils.get_error_from_response(error_body);
                                EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, error.toUpperCase()));
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                }));
    }

    private void set_user_profile_data() {
        String profile_name = user_profile_data.getData().getUser_profile().getFname() + " " + user_profile_data.getData().getUser_profile().getLname();
        user_name=profile_name;
        String user_profile_pic_url = user_profile_data.getData().getUser_profile().getProfile_pic();
        int user_followers = user_profile_data.getData().getUser_profile().getUser_followers().size();
        int user_posts = user_profile_data.getData().getUser_profile().getUser_total_posts();
        int user_following = user_profile_data.getData().getUser_profile().getUser_following().size();

        Timber.d("user id :: " + HelperMethods.get_user_id());


        user_followers_list = user_profile_data.getData().getUser_profile().getUser_followers();
        Timber.d("user followers : " + Arrays.toString(user_followers_list.toArray()));


        is_user_follower = is_user_already_is_follower(user_followers_list);

        if (is_user_follower) {

            user_follow_button.setVisibility(View.GONE);
            user_un_follow_button.setVisibility(View.VISIBLE);


        } else {
            user_follow_button.setVisibility(View.VISIBLE);
            user_un_follow_button.setVisibility(View.GONE);

        }


       String user_name=user_profile_data.getData().getUser_profile().getUser_name();
        if (!user_name.equalsIgnoreCase("")){
            user_name_tv.setText(user_name);
        }else {
            user_name_tv.setText(profile_name);
        }

        total_followers_tv.setText(user_followers + "");
        total_post_tv.setText(user_posts + "");
        total_following_tv.setText(user_following + "");

        if (user_profile_data.getData().getUser_profile().getStatusBean() != null &&
                user_profile_data.getData().getUser_profile().getStatusBean().getCurrent_status() != null) {
            user_bio_quite_tv.setText("' " + user_profile_data.getData().getUser_profile().getStatusBean().getCurrent_status() + " '");
            user_bio_quite_tv.setVisibility(View.VISIBLE);
        } else {
            user_bio_quite_tv.setVisibility(View.GONE);
        }


        Glide.with(OtherUserProfileActivity.this)
                .load(user_profile_pic_url)
                .into(portfolio_pic);


        ic_gridview_thumbnail_data.setColorFilter(ResourcesUtil.getColor(R.color.theme_red));
        ic_list_user_data.setColorFilter(ResourcesUtil.getColor(R.color.white));
        user_tagged_posts.setColorFilter(ResourcesUtil.getColor(R.color.white));

    }

    private boolean is_user_already_is_follower(List<String> user_followers_list) {
        for (String str : user_followers_list) {
            if (str != null) {
                if (str.trim().contains(HelperMethods.get_user_id())) {
                    return true;
                }
            }

        }
        return false;
    }

    @OnClick(R.id.ic_gridview_thumbnail_data)
    public void click_on_grid_view() {
        ic_gridview_thumbnail_data.setColorFilter(ResourcesUtil.getColor(R.color.theme_red));
        ic_list_user_data.setColorFilter(ResourcesUtil.getColor(R.color.white));
        user_tagged_posts.setColorFilter(ResourcesUtil.getColor(R.color.white));
        if (view_pager != null && view_pager.getCurrentItem() != 0) {
            view_pager.setCurrentItem(0);
        }
    }

    @OnClick(R.id.ic_list_user_data)
    public void click_on_list_view() {
        ic_gridview_thumbnail_data.setColorFilter(ResourcesUtil.getColor(R.color.white));
        ic_list_user_data.setColorFilter(ResourcesUtil.getColor(R.color.theme_red));
        user_tagged_posts.setColorFilter(ResourcesUtil.getColor(R.color.white));
        if (view_pager != null && view_pager.getCurrentItem() != 1) {
            view_pager.setCurrentItem(1);
        }
    }

    @OnClick(R.id.user_tagged_posts)
    public void click_on_tagged_post() {
        ic_gridview_thumbnail_data.setColorFilter(ResourcesUtil.getColor(R.color.white));
        ic_list_user_data.setColorFilter(ResourcesUtil.getColor(R.color.white));
        user_tagged_posts.setColorFilter(ResourcesUtil.getColor(R.color.theme_red));
        if (view_pager != null && view_pager.getCurrentItem() != 2) {
            view_pager.setCurrentItem(2);
        }
    }

    @OnClick(R.id.user_un_follow_button)
    public void unfollow_user() {
        emit_unfollow_user_event(getUser_id());
        user_un_follow_button.setVisibility(View.GONE);
        user_follow_button.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.user_follow_button)
    public void follow_user() {
        follow_event(getUser_id());
        user_follow_button.setVisibility(View.GONE);
        user_un_follow_button.setVisibility(View.VISIBLE);
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public void init_view_pager_view() {


        Timber.d("####################### " + total_user_post_infilated.size());


        fragments = new ArrayList<>(3);

        // create music fragment and add it
        GridviewPicsFragment gridviewPicsFragment = new GridviewPicsFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(Constants.USER_POST_LIST, total_user_post_infilated);
        bundle.putInt(Constants.USER_NEXT_PAGE_DETAILS, first_next_page);
        bundle.putString(Constants.USER_TYPE, Constants.USER_TYPE_THIRD);
        bundle.putString("title", getString(R.string.string_home));
        gridviewPicsFragment.setArguments(bundle);


        // create backup fragment and add it
        ListPicsFragment listPicsFragment = new ListPicsFragment();
        bundle = new Bundle();
        bundle.putParcelableArrayList(Constants.USER_POST_LIST, total_user_post_infilated);
        bundle.putInt(Constants.USER_NEXT_PAGE_DETAILS, first_next_page);
        bundle.putString(Constants.USER_TYPE, Constants.USER_TYPE_THIRD);
        bundle.putString("title", getString(R.string.string_search));
        listPicsFragment.setArguments(bundle);


        // create friends fragment and add it
        UserTaggedFragment user_tagged_view = new UserTaggedFragment();
        bundle = new Bundle();
        bundle.putString(Constants.USER_TYPE, Constants.USER_TYPE_THIRD);
        bundle.putString("title", getString(R.string.string_notification));
        user_tagged_view.setArguments(bundle);


        // add to fragments for adapter
        fragments.add(gridviewPicsFragment);
        fragments.add(listPicsFragment);
        fragments.add(user_tagged_view);


        adapter = new VpAdapter(getSupportFragmentManager(), fragments);
        view_pager.setAdapter(adapter);

    }

    private void get_user_stories() {
        show_progress_dialog();
        ThirdPersonStoriesBodyRequest request_body_for_stories = HelperMethods.create_third_person_request_body(getStories_next_page_number(), getUser_id());
        disposable.add(apiService.getThirdUserStories(HelperMethods.get_user_jwt_key(), request_body_for_stories)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<GetUserStoriesResponse>() {
                    @Override
                    public void onSuccess(GetUserStoriesResponse s) {
                        dismiss_progress_dialog();

                        if (s.getData().getStories() != null && s.getData().getStories().size() > 0) {
                            user_all_stories = s.getData().getStories();
                            userStoriesAdapter = new UserStoriesAdapter(OtherUserProfileActivity.this, user_all_stories);
                            user_stories_rv.setLayoutManager(new LinearLayoutManager(OtherUserProfileActivity.this, LinearLayoutManager.HORIZONTAL, true));
                            user_stories_rv.setAdapter(userStoriesAdapter);
                            user_stories_rv.getLayoutManager().scrollToPosition(0);
                            userStoriesAdapter.setOnLoadMoreListener(onLoadMoreListener);
                            if (s.getData().getNext_page() == 1) {
                                increase_page_number();
                                userStoriesAdapter.setShouldLoadMore(true);
                            } else {
                                userStoriesAdapter.setShouldLoadMore(false);
                            }
                        }

                    }

                    @Override
                    public void onError(Throwable e) {

                        dismiss_progress_dialog();
                        if (e instanceof HttpException) {
                            ResponseBody body = ((HttpException) e).response().errorBody();
                            try {
                                String error_body = body.string();
                                dismiss_progress_dialog();
                                String error = Utils.get_error_from_response(error_body);
                                Toasty.error(OtherUserProfileActivity.this, error + "");

                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                }));

    }

    public void show_progress_dialog() {
        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE);
        hud.setSize(50, 50);
        hud.show();
    }

    public void dismiss_progress_dialog() {
        if (hud != null && hud.isShowing())
            hud.dismiss();
    }

    public int getStories_next_page_number() {
        return stories_next_page_number;
    }

    public void setStories_next_page_number(int stories_next_page_number) {
        this.stories_next_page_number = stories_next_page_number;
    }

    private void increase_page_number() {
        first_next_page = first_next_page + 1;
    }

    private void follow_event(String user_id) {
        show_progress_dialog();
        FollowUserRequestBodyModel followUserRequestBodyModel = HelperMethods.create_follow_user_req_body(user_id);
        disposable.add(apiService.followUser(HelperMethods.get_user_jwt_key(), followUserRequestBodyModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<FollowUserResponseModel>() {
                    @Override
                    public void onSuccess(FollowUserResponseModel s) {
                        dismiss_progress_dialog();
                        get_user_profile(getUser_id());
                    }

                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onError(Throwable e) {
                        dismiss_progress_dialog();
                        if (e instanceof HttpException) {
                            ResponseBody body = ((HttpException) e).response().errorBody();
                            try {
                                String error_body = body.string();
                                String error = Utils.get_error_from_response(error_body);
                                EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, error.toUpperCase()));
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                }));
    }

    private void emit_unfollow_user_event(String user_id) {
        show_progress_dialog();
        UnFollowUserRequestBodyModel unfollowUserRequestBodyModel = HelperMethods.create_unfollow_user_req_body(user_id);
        disposable.add(apiService.unfollowUser(HelperMethods.get_user_jwt_key(), unfollowUserRequestBodyModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<UnFollowUserResponseModel>() {
                    @Override
                    public void onSuccess(UnFollowUserResponseModel s) {
                        dismiss_progress_dialog();
                        get_user_profile(getUser_id());
                    }

                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onError(Throwable e) {
                        dismiss_progress_dialog();
                        if (e instanceof HttpException) {
                            ResponseBody body = ((HttpException) e).response().errorBody();
                            try {
                                String error_body = body.string();
                                String error = Utils.get_error_from_response(error_body);
                                EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, error.toUpperCase()));
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                }));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.follower:{
               // openActivity("follwer");
                break;
            }
            case R.id.following:{
                //openActivity("following");
                break;
            }
        }
    }

    private void openActivity(String tag) {
        Intent i=new Intent(this, FollowFollowingActivity.class);
        i.putExtra("checkingState",tag);
        i.putExtra("userName",user_name);
        i.putExtra("userID",getUser_id());
        startActivity(i);

    }


    private static class VpAdapter extends FragmentStatePagerAdapter {
        private List<Fragment> data;

        public VpAdapter(FragmentManager fm, List<Fragment> data) {
            super(fm);
            this.data = data;
        }

        @Override
        public int getCount() {
            return data.size();
        }


        @Override
        public Fragment getItem(int position) {
            return data.get(position);
        }
    }

}


