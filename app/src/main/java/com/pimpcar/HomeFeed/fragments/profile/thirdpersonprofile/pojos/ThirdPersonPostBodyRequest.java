package com.pimpcar.HomeFeed.fragments.profile.thirdpersonprofile.pojos;

import com.google.gson.annotations.SerializedName;

public class ThirdPersonPostBodyRequest {
    /**
     * data : {"attributes":{"user_id":"b8a0cb9d-ea3d-5acc-9ef5-d1c73abe351c","page_number":0}}
     */

    @SerializedName("data")
    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * attributes : {"user_id":"b8a0cb9d-ea3d-5acc-9ef5-d1c73abe351c","page_number":0}
         */

        @SerializedName("attributes")
        private AttributesBean attributes;

        public AttributesBean getAttributes() {
            return attributes;
        }

        public void setAttributes(AttributesBean attributes) {
            this.attributes = attributes;
        }

        public static class AttributesBean {
            /**
             * user_id : b8a0cb9d-ea3d-5acc-9ef5-d1c73abe351c
             * page_number : 0
             */

            @SerializedName("user_id")
            private String user_id;
            @SerializedName("page_number")
            private int page_number;

            public String getUser_id() {
                return user_id;
            }

            public void setUser_id(String user_id) {
                this.user_id = user_id;
            }

            public int getPage_number() {
                return page_number;
            }

            public void setPage_number(int page_number) {
                this.page_number = page_number;
            }
        }
    }
}
