package com.pimpcar.HomeFeed.fragments.profile.thirdpersonprofile.pojos;

import com.google.gson.annotations.SerializedName;

public class ThirdPersonProfileRequestBody {
    /**
     * data : {"attributes":{"user_id":"b8a0cb9d-ea3d-5acc-9ef5-d1c73abe351c"}}
     */

    @SerializedName("data")
    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * attributes : {"user_id":"b8a0cb9d-ea3d-5acc-9ef5-d1c73abe351c"}
         */

        @SerializedName("attributes")
        private AttributesBean attributes;

        public AttributesBean getAttributes() {
            return attributes;
        }

        public void setAttributes(AttributesBean attributes) {
            this.attributes = attributes;
        }

        public static class AttributesBean {
            /**
             * user_id : b8a0cb9d-ea3d-5acc-9ef5-d1c73abe351c
             */

            @SerializedName("user_id")
            private String user_id;

            public String getUser_id() {
                return user_id;
            }

            public void setUser_id(String user_id) {
                this.user_id = user_id;
            }
        }
    }
}
