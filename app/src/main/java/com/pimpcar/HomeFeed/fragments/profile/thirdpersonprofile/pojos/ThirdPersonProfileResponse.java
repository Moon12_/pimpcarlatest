package com.pimpcar.HomeFeed.fragments.profile.thirdpersonprofile.pojos;

import com.google.gson.annotations.SerializedName;
import com.pimpcar.Network.model.userselfprofile.UserProfileFirstData;

import java.util.List;

public class ThirdPersonProfileResponse {
    /**
     * data : {"user_profile":{"uid":"b8a0cb9d-ea3d-5acc-9ef5-d1c73abe351c","fname":"Bruce","lname":"Wayne","gender":"male","profile_pic":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/b8a0cb9d-ea3d-5acc-9ef5-d1c73abe351c/1564141819-qAiyQ40LH02HyJeVJeiypnqh9G6OgZP9_b8a0cb9d-ea3d-5acc-9ef5-d1c73abe351c_b8a0cb9d-ea3d-5acc-9ef5-d1c73abe351c_Bruce.jpg","user_total_posts":0,"user_total_followers":0,"user_total_following":0,"user_stories":[],"user_followers":[],"user_following":["22855eb3-fe40-5832-b54e-6f8042ba0f6d","22855eb3-fe40-5832-b54e-6f8042ba0f6d"],"email":{"email_id":"batman@gmail.com"},"contact_number":{}}}
     */

    @SerializedName("data")
    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * user_profile : {"uid":"b8a0cb9d-ea3d-5acc-9ef5-d1c73abe351c","fname":"Bruce","lname":"Wayne","gender":"male","profile_pic":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/b8a0cb9d-ea3d-5acc-9ef5-d1c73abe351c/1564141819-qAiyQ40LH02HyJeVJeiypnqh9G6OgZP9_b8a0cb9d-ea3d-5acc-9ef5-d1c73abe351c_b8a0cb9d-ea3d-5acc-9ef5-d1c73abe351c_Bruce.jpg","user_total_posts":0,"user_total_followers":0,"user_total_following":0,"user_stories":[],"user_followers":[],"user_following":["22855eb3-fe40-5832-b54e-6f8042ba0f6d","22855eb3-fe40-5832-b54e-6f8042ba0f6d"],"email":{"email_id":"batman@gmail.com"},"contact_number":{}}
         */

        @SerializedName("user_profile")
        private UserProfileBean user_profile;

        public UserProfileBean getUser_profile() {
            return user_profile;
        }

        public void setUser_profile(UserProfileBean user_profile) {
            this.user_profile = user_profile;
        }

        public static class UserProfileBean {
            /**
             * uid : b8a0cb9d-ea3d-5acc-9ef5-d1c73abe351c
             * fname : Bruce
             * lname : Wayne
             * gender : male
             * profile_pic : https://s3.us-east-2.amazonaws.com/pimpcarprofile/b8a0cb9d-ea3d-5acc-9ef5-d1c73abe351c/1564141819-qAiyQ40LH02HyJeVJeiypnqh9G6OgZP9_b8a0cb9d-ea3d-5acc-9ef5-d1c73abe351c_b8a0cb9d-ea3d-5acc-9ef5-d1c73abe351c_Bruce.jpg
             * user_total_posts : 0
             * user_total_followers : 0
             * user_total_following : 0
             * user_stories : []
             * user_followers : []
             * user_following : ["22855eb3-fe40-5832-b54e-6f8042ba0f6d","22855eb3-fe40-5832-b54e-6f8042ba0f6d"]
             * email : {"email_id":"batman@gmail.com"}
             * contact_number : {}
             */

            @SerializedName("uid")
            private String uid;
            @SerializedName("fname")
            private String fname;
            @SerializedName("lname")
            private String lname;
            @SerializedName("gender")
            private String gender;
            @SerializedName("profile_pic")
            private String profile_pic;
            @SerializedName("user_total_posts")
            private int user_total_posts;
            @SerializedName("user_total_followers")
            private int user_total_followers;
            @SerializedName("user_total_following")
            private int user_total_following;
            @SerializedName("email")
            private EmailBean email;
            @SerializedName("contact_number")
            private ContactNumberBean contact_number;
            @SerializedName("user_stories")
            private List<String> user_stories;
            @SerializedName("user_followers")
            private List<String> user_followers;
            @SerializedName("user_following")
            private List<String> user_following;
            @SerializedName("user_status")
            private ThirdStatusBean statusBean;

            @SerializedName("user_name")
            private String user_name;

            public String getUser_name() {
                return user_name;
            }

            public void setUser_name(String user_name) {
                this.user_name = user_name;
            }

            public String getUid() {
                return uid;
            }

            public void setUid(String uid) {
                this.uid = uid;
            }

            public String getFname() {
                return fname;
            }

            public void setFname(String fname) {
                this.fname = fname;
            }

            public String getLname() {
                return lname;
            }

            public void setLname(String lname) {
                this.lname = lname;
            }

            public String getGender() {
                return gender;
            }

            public void setGender(String gender) {
                this.gender = gender;
            }

            public String getProfile_pic() {
                return profile_pic;
            }

            public void setProfile_pic(String profile_pic) {
                this.profile_pic = profile_pic;
            }

            public int getUser_total_posts() {
                return user_total_posts;
            }

            public void setUser_total_posts(int user_total_posts) {
                this.user_total_posts = user_total_posts;
            }

            public int getUser_total_followers() {
                return user_total_followers;
            }

            public void setUser_total_followers(int user_total_followers) {
                this.user_total_followers = user_total_followers;
            }

            public int getUser_total_following() {
                return user_total_following;
            }

            public void setUser_total_following(int user_total_following) {
                this.user_total_following = user_total_following;
            }

            public EmailBean getEmail() {
                return email;
            }

            public void setEmail(EmailBean email) {
                this.email = email;
            }

            public ContactNumberBean getContact_number() {
                return contact_number;
            }

            public void setContact_number(ContactNumberBean contact_number) {
                this.contact_number = contact_number;
            }

            public List<?> getUser_stories() {
                return user_stories;
            }

            public void setUser_stories(List<String> user_stories) {
                this.user_stories = user_stories;
            }

            public List<String> getUser_followers() {
                return user_followers;
            }

            public void setUser_followers(List<String> user_followers) {
                this.user_followers = user_followers;
            }

            public List<String> getUser_following() {
                return user_following;
            }

            public void setUser_following(List<String> user_following) {
                this.user_following = user_following;
            }

            public ThirdStatusBean getStatusBean() {
                return statusBean;
            }

            public void setStatusBean(ThirdStatusBean statusBean) {
                this.statusBean = statusBean;
            }

            public static class EmailBean {
            }

            public static class ContactNumberBean {
            }

            public static class ThirdStatusBean {

                @SerializedName("current_status")
                private String current_status;


                public String getCurrent_status() {
                    return current_status;
                }

                public void setCurrent_status(String current_status) {
                    this.current_status = current_status;
                }
            }
        }
    }
}
