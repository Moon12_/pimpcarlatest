package com.pimpcar.HomeFeed.fragments.profile.thirdpersonprofile.pojos;

import com.google.gson.annotations.SerializedName;

public class ThirdPersonStoriesBodyRequest {
    /**
     * data : {"attributes":{"page_number":0,"user_id":"3d248ae7-91c3-5d6e-898a-665435049dad"}}
     */

    @SerializedName("data")
    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * attributes : {"page_number":0,"user_id":"3d248ae7-91c3-5d6e-898a-665435049dad"}
         */

        @SerializedName("attributes")
        private AttributesBean attributes;

        public AttributesBean getAttributes() {
            return attributes;
        }

        public void setAttributes(AttributesBean attributes) {
            this.attributes = attributes;
        }

        public static class AttributesBean {
            /**
             * page_number : 0
             * user_id : 3d248ae7-91c3-5d6e-898a-665435049dad
             */

            @SerializedName("page_number")
            private int page_number;
            @SerializedName("user_id")
            private String user_id;

            public int getPage_number() {
                return page_number;
            }

            public void setPage_number(int page_number) {
                this.page_number = page_number;
            }

            public String getUser_id() {
                return user_id;
            }

            public void setUser_id(String user_id) {
                this.user_id = user_id;
            }
        }
    }
}
