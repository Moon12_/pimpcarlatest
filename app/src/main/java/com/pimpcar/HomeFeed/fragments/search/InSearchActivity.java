package com.pimpcar.HomeFeed.fragments.search;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.pimpcar.HomeFeed.fragments.search.Interface.IDataCallback;
import com.pimpcar.HomeFeed.fragments.search.Interface.IFragmentListener;
import com.pimpcar.HomeFeed.fragments.search.Interface.ISearch;
import com.pimpcar.R;

import java.util.ArrayList;

public class InSearchActivity extends AppCompatActivity implements IFragmentListener, View.OnClickListener {
    TabLayout tabLayout;
    ViewPager viewPager;
    EditText search_editText;
    String search_text = "";
    ArrayList<ISearch> iSearch = new ArrayList<>();


    ArrayList<String> listData = null;

    IDataCallback iDataCallback = null;

    private PageAdapter adapter;


    public void setiDataCallback(IDataCallback iDataCallback) {
        this.iDataCallback = iDataCallback;
        iDataCallback.onFragmentCreated(listData);
    }

    @Override
    public void addiSearch(ISearch iSearch) {
        this.iSearch.add(iSearch);
    }

    @Override
    public void removeISearch(ISearch iSearch) {
        this.iSearch.remove(iSearch);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insearch);

        listData = new ArrayList<>();

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.addTab(tabLayout.newTab().setText("Top"));
        tabLayout.addTab(tabLayout.newTab().setText("Accounts"));
        tabLayout.addTab(tabLayout.newTab().setText("Tag"));
        tabLayout.addTab(tabLayout.newTab().setText("Places"));


        ImageView back_button = findViewById(R.id.back_button);
        back_button.setOnClickListener(this);

        search_editText = findViewById(R.id.search_editText);
        search_editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                search_text = String.valueOf(s);
                if (search_text.length() > 1) {
                    adapter.setTextQueryChanged(search_text);

                    for (ISearch iSearchLocal : iSearch)
                        iSearchLocal.onTextQuery(search_text);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });

        viewPager = (ViewPager) findViewById(R.id.viewPager);

        adapter = new PageAdapter(getSupportFragmentManager(), search_text);
        viewPager.setAdapter(adapter);

        tabLayout.setupWithViewPager(viewPager);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_button: {
                onBackPressed();
                break;
            }
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
