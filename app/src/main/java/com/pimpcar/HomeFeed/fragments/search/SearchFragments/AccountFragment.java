package com.pimpcar.HomeFeed.fragments.search.SearchFragments;


import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pimpcar.R;

public class AccountFragment extends Fragment {
    private static final String ARG_SEARCHTERM = "search_term";

    //    @Override
//    public void updatedName(String newName) {
//        if (newName !=null){
//            Log.d("checkingDta",newName);
//        }
//    }
    public static AccountFragment newInstance(String searchTerm) {
        AccountFragment fragment = new AccountFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_SEARCHTERM, searchTerm);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_account, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        String data = DataClass.getInstance().getDistributor_id();
//        if (data!=null){
//            Log.d("checkingdata",data);
//        }
    }

}
