package com.pimpcar.HomeFeed.fragments.search.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pimpcar.HomeFeed.fragments.search.models.CategoryPOJO;
import com.pimpcar.R;

import java.util.ArrayList;
import java.util.List;

public class SearchCategoryAdapter extends RecyclerView.Adapter<SearchCategoryAdapter.MyViewHolder> {

    Context context;
    List<CategoryPOJO> categoryPOJOList = new ArrayList<>();
    private LayoutInflater _mLayout_item_inflater;

    public SearchCategoryAdapter(Context context, List<CategoryPOJO> categoryPOJOList) {
        this.context = context;
        this.categoryPOJOList = categoryPOJOList;
        this._mLayout_item_inflater = LayoutInflater.from(this.context);

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = _mLayout_item_inflater.inflate(R.layout.category_list_layout, viewGroup, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.imageView3.setImageResource(categoryPOJOList.get(position).getImage());
        holder.textView.setText(categoryPOJOList.get(position).getTitle());
    }

    @Override
    public int getItemCount() {
        return categoryPOJOList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView3;
        TextView textView;

        public MyViewHolder(View itemView) {
            super(itemView);

            imageView3 = itemView.findViewById(R.id.imageView3);
            textView = itemView.findViewById(R.id.textView5);

        }
    }
}
