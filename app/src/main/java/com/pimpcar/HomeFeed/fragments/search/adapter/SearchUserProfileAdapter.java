package com.pimpcar.HomeFeed.fragments.search.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;
import com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans.FollowUserRequestBodyModel;
import com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans.FollowUserResponseModel;
import com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans.UnFollowUserRequestBodyModel;
import com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans.UnFollowUserResponseModel;
import com.pimpcar.HomeFeed.fragments.profile.thirdpersonprofile.OtherUserProfileActivity;
import com.pimpcar.HomeFeed.fragments.search.models.SearchUserProfileViewHolder;
import com.pimpcar.Network.ApiClient;
import com.pimpcar.Network.ApiService;
import com.pimpcar.Network.model.user_search.UserSearchResponseBody;
import com.pimpcar.R;
import com.pimpcar.UserAuthentication.eventbus.models.ShowToastEventModel;
import com.pimpcar.utils.Constants;
import com.pimpcar.utils.HelperMethods;
import com.pimpcar.utils.ResourcesUtil;
import com.pimpcar.utils.Utils;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.util.List;

import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import timber.log.Timber;

public class SearchUserProfileAdapter extends RecyclerView.Adapter<SearchUserProfileViewHolder> {

    String name = "";
    private List<UserSearchResponseBody.DataBean.SerachedDataBean> _mSerachedDataBeans;
    private List<UserSearchResponseBody.DataBean.SerachedDataBean> contactListFiltered;
    private Context _mContext;
    private LayoutInflater _mLayoutInflater;
    private SearchUserProfileViewHolder view_user_search_holder;
    private ApiService apiService;
    private CompositeDisposable disposable = new CompositeDisposable();
    private int position = 0;

    public SearchUserProfileAdapter(Context context, List<UserSearchResponseBody.DataBean.SerachedDataBean> serached_data) {
        this._mContext = context;
        this._mSerachedDataBeans = serached_data;
        this.contactListFiltered = _mSerachedDataBeans;
        _mLayoutInflater = LayoutInflater.from(_mContext);
        if (_mContext != null) {
            apiService = ApiClient.getClient(_mContext.getApplicationContext()).create(ApiService.class);
        }
    }

    @NonNull
    @Override
    public SearchUserProfileViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = _mLayoutInflater.inflate(R.layout.user_serach_item_layout, viewGroup, false);
        ButterKnife.bind(this, itemView);
        view_user_search_holder = new SearchUserProfileViewHolder(itemView, _mContext, _mSerachedDataBeans);
        return view_user_search_holder;
    }

    @Override
    public void onBindViewHolder(@NonNull SearchUserProfileViewHolder searchUserViewHolder, int i) {

        if (!_mSerachedDataBeans.get(i).getUid().contentEquals(HelperMethods.get_user_id())) {
            view_user_search_holder.user_name_full_name.setVisibility(View.GONE);
            view_user_search_holder.user_name_full_name.setText(_mSerachedDataBeans.get(i).getFname() + " " + _mSerachedDataBeans.get(i).getLname());


            if (_mSerachedDataBeans.get(i).getUser_name() != null && !_mSerachedDataBeans.get(i).getUser_name().isEmpty()) {
                view_user_search_holder.user_name.setVisibility(View.VISIBLE);
                view_user_search_holder.user_name.setText("@" + _mSerachedDataBeans.get(i).getUser_name());
                name = _mSerachedDataBeans.get(i).getUser_name();
            } else {
                view_user_search_holder.user_name.setVisibility(View.INVISIBLE);
            }


            Timber.d("data to be put on ::: " + _mSerachedDataBeans.get(i).getProfile_pic() + " --");


            if (_mSerachedDataBeans.get(i).getProfile_pic() != null && !_mSerachedDataBeans.get(i).getProfile_pic().isEmpty()) {
                Picasso.with(_mContext)
                        .load(_mSerachedDataBeans.get(i).getProfile_pic())
                        .fit()
                        .into(view_user_search_holder.user_profile_pic);
            } else {
                view_user_search_holder.user_profile_pic.setImageDrawable(ResourcesUtil.getDrawableById(R.drawable.user_profile_place_holder));
            }


            if (_mSerachedDataBeans.get(i).getUser_followers() != null) {
                if (is_user_already_is_follower(_mSerachedDataBeans.get(i).getUser_followers())) {
                    view_user_search_holder.user_follow_button.setVisibility(View.GONE);
                    view_user_search_holder.user_un_follow_button.setVisibility(View.VISIBLE);
                } else {
                    view_user_search_holder.user_un_follow_button.setVisibility(View.GONE);
                    view_user_search_holder.user_follow_button.setVisibility(View.VISIBLE);
                }
            }


            view_user_search_holder.user_profile_pic.setOnClickListener(v -> {
                open_user_profile(_mSerachedDataBeans.get(i).getUid(), v.getContext());
            });

        }


    }

    private void open_user_profile(String uid, Context context) {
        Intent intent = new Intent(context, OtherUserProfileActivity.class);
        intent.putExtra(Constants.THIRD_USER_ID, uid);
        _mContext.startActivity(intent);
    }

    @Override
    public int getItemCount() {
        if (_mSerachedDataBeans != null && _mSerachedDataBeans.size() > 0)
            return _mSerachedDataBeans.size();
        return 0;
    }


    private boolean is_user_already_is_follower(List<String> user_followers_list) {

        if (user_followers_list.size() == 0) {
            return false;
        }

        for (String str : user_followers_list) {
            if (str != null && str.trim().contains(HelperMethods.get_user_id())) {
                return true;
            }
        }
        return false;
    }

    private void emit_unfollow_user_event(String user_id, int position) {
        UnFollowUserRequestBodyModel unfollowUserRequestBodyModel = HelperMethods.create_unfollow_user_req_body(user_id);
        disposable.add(apiService.unfollowUser(HelperMethods.get_user_jwt_key(), unfollowUserRequestBodyModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<UnFollowUserResponseModel>() {
                    @Override
                    public void onSuccess(UnFollowUserResponseModel s) {

                        notifyDataSetChanged();
                    }

                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof HttpException) {
                            ResponseBody body = ((HttpException) e).response().errorBody();
                            try {
                                String error_body = body.string();
                                String error = Utils.get_error_from_response(error_body);
                                EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, error.toUpperCase()));
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                }));
    }


    private void follow_event(String user_id, int position) {
        FollowUserRequestBodyModel followUserRequestBodyModel = HelperMethods.create_follow_user_req_body(user_id);
        disposable.add(apiService.followUser(HelperMethods.get_user_jwt_key(), followUserRequestBodyModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<FollowUserResponseModel>() {
                    @Override
                    public void onSuccess(FollowUserResponseModel s) {
                        notifyDataSetChanged();
                    }

                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof HttpException) {
                            ResponseBody body = ((HttpException) e).response().errorBody();
                            try {
                                String error_body = body.string();
                                String error = Utils.get_error_from_response(error_body);
                                EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, error.toUpperCase()));
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                }));
    }


}
