package com.pimpcar.HomeFeed.fragments.search.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pimpcar.Network.ApiClient;
import com.pimpcar.Network.ApiService;
import com.pimpcar.Network.model.user_search.UserSearchResponseBody;
import com.pimpcar.R;
import com.pimpcar.utils.HelperMethods;

import java.util.List;

import butterknife.ButterKnife;
import io.reactivex.disposables.CompositeDisposable;

public class SearchUserTagList extends RecyclerView.Adapter<SearchUserProfileTagViewHolder> {
    String name = "";
    private List<UserSearchResponseBody.DataBean.SerachedDataBean> _mSerachedDataBeans;
    private List<UserSearchResponseBody.DataBean.SerachedDataBean> contactListFiltered;
    private Context _mContext;
    private LayoutInflater _mLayoutInflater;
    private SearchUserProfileTagViewHolder view_user_search_holder;
    private ApiService apiService;
    private CompositeDisposable disposable = new CompositeDisposable();
    private int position = 0;

    public SearchUserTagList(Context context, List<UserSearchResponseBody.DataBean.SerachedDataBean> serached_data) {
        this._mContext = context;
        this._mSerachedDataBeans = serached_data;
        this.contactListFiltered = _mSerachedDataBeans;
        _mLayoutInflater = LayoutInflater.from(_mContext);
        if (_mContext != null) {
            apiService = ApiClient.getClient(_mContext.getApplicationContext()).create(ApiService.class);
        }
    }
    @NonNull
    @Override
    public SearchUserProfileTagViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = _mLayoutInflater.inflate(R.layout.tag_user_search_list, parent, false);
        ButterKnife.bind(this, itemView);
        view_user_search_holder = new SearchUserProfileTagViewHolder(itemView, _mContext, _mSerachedDataBeans);
        return view_user_search_holder;
    }

    @Override
    public void onBindViewHolder(@NonNull SearchUserProfileTagViewHolder searchUserProfileTagViewHolder, int i) {
        if (!_mSerachedDataBeans.get(i).getUid().contentEquals(HelperMethods.get_user_id())) {

            //view_user_search_holder.tag_name.setText(_mSerachedDataBeans.get(i).getUser_tagged().get(i).getName());
        }
    }

    @Override
    public int getItemCount() {
        if (_mSerachedDataBeans != null && _mSerachedDataBeans.size() > 0)
            return _mSerachedDataBeans.size();
        return 0;
    }
}
