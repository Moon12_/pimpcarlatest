package com.pimpcar.Network.model.contact_number_verification;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class OTPSendBody implements Parcelable {


    public static final Creator<OTPSendBody> CREATOR = new Creator<OTPSendBody>() {
        @Override
        public OTPSendBody createFromParcel(Parcel in) {
            return new OTPSendBody(in);
        }

        @Override
        public OTPSendBody[] newArray(int size) {
            return new OTPSendBody[size];
        }
    };
    /**
     * data : {"attributes":{"contact_number":"7506224347","country":"91"}}
     */

    @SerializedName("data")
    private DataBean data;

    public OTPSendBody() {

    }

    protected OTPSendBody(Parcel in) {
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public static class DataBean {
        /**
         * attributes : {"contact_number":"7506224347","country":"91"}
         */

        @SerializedName("attributes")
        private AttributesBean attributes;

        public AttributesBean getAttributes() {
            return attributes;
        }

        public void setAttributes(AttributesBean attributes) {
            this.attributes = attributes;
        }

        public static class AttributesBean {
            /**
             * contact_number : 7506224347
             * country : 91
             */

            @SerializedName("contact_number")
            private String contact_number;
            @SerializedName("country")
            private String country;

            public String getContact_number() {
                return contact_number;
            }

            public void setContact_number(String contact_number) {
                this.contact_number = contact_number;
            }

            public String getCountry() {
                return country;
            }

            public void setCountry(String country) {
                this.country = country;
            }
        }
    }
}
