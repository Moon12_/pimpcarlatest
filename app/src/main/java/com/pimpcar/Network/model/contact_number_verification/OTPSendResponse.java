package com.pimpcar.Network.model.contact_number_verification;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class OTPSendResponse implements Parcelable {
    /**
     * data : {"message":"otp sent to contact number 7506224347"}
     */

    @SerializedName("data")
    private DataBean data;

    protected OTPSendResponse(Parcel in) {
    }

    public static final Creator<OTPSendResponse> CREATOR = new Creator<OTPSendResponse>() {
        @Override
        public OTPSendResponse createFromParcel(Parcel in) {
            return new OTPSendResponse(in);
        }

        @Override
        public OTPSendResponse[] newArray(int size) {
            return new OTPSendResponse[size];
        }
    };

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public static class DataBean {
        /**
         * message : otp sent to contact number 7506224347
         */

        @SerializedName("message")
        private String message;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}
