package com.pimpcar.Network.model.login;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class LoginBody implements Parcelable {


    public LoginBody(){

    }
    /**
     * data : {"attributes":{"email":"sahityakumarsuman@gmail.com","password":"sahitya123"}}
     */

    @SerializedName("data")
    private DataBean data;

    protected LoginBody(Parcel in) {
    }

    public static final Creator<LoginBody> CREATOR = new Creator<LoginBody>() {
        @Override
        public LoginBody createFromParcel(Parcel in) {
            return new LoginBody(in);
        }

        @Override
        public LoginBody[] newArray(int size) {
            return new LoginBody[size];
        }
    };

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public static class DataBean {
        /**
         * attributes : {"email":"sahityakumarsuman@gmail.com","password":"sahitya123"}
         */

        @SerializedName("attributes")
        private AttributesBean attributes;

        public AttributesBean getAttributes() {
            return attributes;
        }

        public void setAttributes(AttributesBean attributes) {
            this.attributes = attributes;
        }

        public static class AttributesBean {
            /**
             * email : sahityakumarsuman@gmail.com
             * password : sahitya123
             */

            @SerializedName("email")
            private String email;
            @SerializedName("password")
            private String password;

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public String getPassword() {
                return password;
            }

            public void setPassword(String password) {
                this.password = password;
            }
        }
    }
}
