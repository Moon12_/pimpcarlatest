package com.pimpcar.Network.model.login;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LoginResponse {

    /**
     * data : {"message":"login successfully","jwt_token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjIyODU1ZWIzLWZlNDAtNTgzMi1iNTRlLTZmODA0MmJhMGY2ZCIsImlhdCI6MTU3NTM2NjIwNSwiZXhwIjoxNjA2OTIzODA1fQ.z_682VHzjspiJO9KDMQ0Nzd_Pycu1sxBkL0EBA3GS_I","uid":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","user":{"email":{"email_status":1,"email_id":"zack@gmail.com"},"contact_number":{"contact_no_status":-1},"user_status":{"current_status":"I am inevitable"},"counts":{"failed_login":0},"meta_data":{"password_change_otp":0,"contact_number_verification_otp":0,"email_verification_otp":0},"user_name":"knightfall_protocol_batman","user_blocked_list":[],"user_reports":[],"profile_pic":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565958853-LBffv9NdUGyJ9cqbRR94ruLF8sXoxk9v_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","user_total_posts":32,"user_total_followers":0,"user_total_following":0,"user_followers":["27f56235-371b-5106-a169-4cb86ff78d3e","37ce61f7-32d3-5c17-8d3d-e5c2d7c36b78","1ffa1d8b-249c-5aa5-9ef5-268d03d27507","47662af-cd07-50ab-b41d-6911662a37f2","c016cb14-10c2-500b-aaf6-6e2a629de04d","fea89666-b728-52e7-84ae-3e95bf8ffd00","c6bc4395-0b4c-5c9a-8c4a-27c9448fdacd","fe70bb50-26d8-555c-b84e-85cdaa47e800","d577f9e5-4ef9-518c-89b9-3be0dd5d4221","b8a0cb9d-ea3d-5acc-9ef5-d1c73abe351c","cd7dacd6-f577-5b6c-923b-0eb8a5cd8f96","d85b1696-8414-594b-bbae-fbeca04cb49a","581c5f66-257f-51a3-b7f1-cdc8101f7507","699205a1-7dff-5d73-ad2e-e3eca6041e64","854d1587-286e-523c-8d2b-2bb625c6fa97","01c8a8ef-7f29-5f26-8809-c1d81d4362b4","b76c6499-27c8-544c-b0a5-c7dfb5d07fa3","decd6131-1bc7-591e-a896-2388417189b6","147b9033-7292-5d82-a50e-7927a7b7eafd","ac8958e2-d2c5-5361-8eed-ca0e8b6d55a1","0d019265-a6db-50c5-9324-02ab7764249d",null,null,"fea89666-b728-52e7-84ae-3e95bf8ffd00","c6bc4395-0b4c-5c9a-8c4a-27c9448fdacd","c6bc4395-0b4c-5c9a-8c4a-27c9448fdacd","d577f9e5-4ef9-518c-89b9-3be0dd5d4221","b76c6499-27c8-544c-b0a5-c7dfb5d07fa3","e47662af-cd07-50ab-b41d-6911662a37f2","c6bc4395-0b4c-5c9a-8c4a-27c9448fdacd","581c5f66-257f-51a3-b7f1-cdc8101f7507","d85b1696-8414-594b-bbae-fbeca04cb49a","decd6131-1bc7-591e-a896-2388417189b6","cd7dacd6-f577-5b6c-923b-0eb8a5cd8f96","27f56235-371b-5106-a169-4cb86ff78d3e","27f56235-371b-5106-a169-4cb86ff78d3e","581c5f66-257f-51a3-b7f1-cdc8101f7507","b8a0cb9d-ea3d-5acc-9ef5-d1c73abe351c"],"user_following":[null,"c016cb14-10c2-500b-aaf6-6e2a629de04d","147b9033-7292-5d82-a50e-7927a7b7eafd","b76c6499-27c8-544c-b0a5-c7dfb5d07fa3","854d1587-286e-523c-8d2b-2bb625c6fa97","ac8958e2-d2c5-5361-8eed-ca0e8b6d55a1","27f56235-371b-5106-a169-4cb86ff78d3e","0d019265-a6db-50c5-9324-02ab7764249d","01c8a8ef-7f29-5f26-8809-c1d81d4362b4","699205a1-7dff-5d73-ad2e-e3eca6041e64","3d248ae7-91c3-5d6e-898a-665435049dad"],"user_stories":["$2a$10$9wbuzPZ5GRSKriGsuntCZeWjoarp8D0fxUWqQ/vy5O3iF3AuhdxPC","$2a$10$fbENxQ3qL1bbwZaHMqO30OECddW9Ht1RN/ENd0kE5coUljbnGK/.C","$2a$10$7MPgIsfIMlnWzJHY28yB0.n4ZWmwSyiYQUjg./OUGDiO2n6WT2yTW","$2a$10$mLsMvN2tKnSjuvFM.7KZwuIvdF1wRg6bwl91022uXfnWL7DN6zGiG","$2a$10$ohGvUGCqGWeOhKDNOMNe/OET8TntQksHICyxKeoqxcQJFkPbRKcTq","$2a$10$bRIUh1Y/u4FddH9j2qgUeOcwIoOleZ.kZ5Xx.GbCm38zW4NBlYT/u","$2a$10$UheZeVpKC1kxvfuM2B5hWuDr7mw8TJhjlhB/A7z3CcPf7kHTO3hjy","$2a$10$c3DSGVhqqrWuIQeNRLwqruX/WMiScwVf5d89nPayP9iyyVkua2Xz6","$2a$10$iaCaED48UoA9EsBCFl2NCOhUuZXw2vj5Zz9z.DsuuqwDk5QD8sXN.","$2a$10$uhPyArSJfVEoEPl5KYliAOir/YusVK9DckFqx2GtHNt8w0hfbpr2m"],"notification":["X3NZWAD2q1575219856272","myuTngROJ1575271716756"],"_id":"5d3ae8820b9de120544d93e3","uid":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","fname":"Zack","lname":"Snyder","login_type":"email","__v":1,"hasshed_password":"$2a$13$1RnEDdWCc68vFBqZIfL4f.5Wa2nhCz/2qOyEcmGGxpC2pfZS.g/Ui","gender":"male"}}
     */

    @SerializedName("data")
    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * message : login successfully
         * jwt_token : eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjIyODU1ZWIzLWZlNDAtNTgzMi1iNTRlLTZmODA0MmJhMGY2ZCIsImlhdCI6MTU3NTM2NjIwNSwiZXhwIjoxNjA2OTIzODA1fQ.z_682VHzjspiJO9KDMQ0Nzd_Pycu1sxBkL0EBA3GS_I
         * uid : 22855eb3-fe40-5832-b54e-6f8042ba0f6d
         * user : {"email":{"email_status":1,"email_id":"zack@gmail.com"},"contact_number":{"contact_no_status":-1},"user_status":{"current_status":"I am inevitable"},"counts":{"failed_login":0},"meta_data":{"password_change_otp":0,"contact_number_verification_otp":0,"email_verification_otp":0},"user_name":"knightfall_protocol_batman","user_blocked_list":[],"user_reports":[],"profile_pic":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565958853-LBffv9NdUGyJ9cqbRR94ruLF8sXoxk9v_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","user_total_posts":32,"user_total_followers":0,"user_total_following":0,"user_followers":["27f56235-371b-5106-a169-4cb86ff78d3e","37ce61f7-32d3-5c17-8d3d-e5c2d7c36b78","1ffa1d8b-249c-5aa5-9ef5-268d03d27507","47662af-cd07-50ab-b41d-6911662a37f2","c016cb14-10c2-500b-aaf6-6e2a629de04d","fea89666-b728-52e7-84ae-3e95bf8ffd00","c6bc4395-0b4c-5c9a-8c4a-27c9448fdacd","fe70bb50-26d8-555c-b84e-85cdaa47e800","d577f9e5-4ef9-518c-89b9-3be0dd5d4221","b8a0cb9d-ea3d-5acc-9ef5-d1c73abe351c","cd7dacd6-f577-5b6c-923b-0eb8a5cd8f96","d85b1696-8414-594b-bbae-fbeca04cb49a","581c5f66-257f-51a3-b7f1-cdc8101f7507","699205a1-7dff-5d73-ad2e-e3eca6041e64","854d1587-286e-523c-8d2b-2bb625c6fa97","01c8a8ef-7f29-5f26-8809-c1d81d4362b4","b76c6499-27c8-544c-b0a5-c7dfb5d07fa3","decd6131-1bc7-591e-a896-2388417189b6","147b9033-7292-5d82-a50e-7927a7b7eafd","ac8958e2-d2c5-5361-8eed-ca0e8b6d55a1","0d019265-a6db-50c5-9324-02ab7764249d",null,null,"fea89666-b728-52e7-84ae-3e95bf8ffd00","c6bc4395-0b4c-5c9a-8c4a-27c9448fdacd","c6bc4395-0b4c-5c9a-8c4a-27c9448fdacd","d577f9e5-4ef9-518c-89b9-3be0dd5d4221","b76c6499-27c8-544c-b0a5-c7dfb5d07fa3","e47662af-cd07-50ab-b41d-6911662a37f2","c6bc4395-0b4c-5c9a-8c4a-27c9448fdacd","581c5f66-257f-51a3-b7f1-cdc8101f7507","d85b1696-8414-594b-bbae-fbeca04cb49a","decd6131-1bc7-591e-a896-2388417189b6","cd7dacd6-f577-5b6c-923b-0eb8a5cd8f96","27f56235-371b-5106-a169-4cb86ff78d3e","27f56235-371b-5106-a169-4cb86ff78d3e","581c5f66-257f-51a3-b7f1-cdc8101f7507","b8a0cb9d-ea3d-5acc-9ef5-d1c73abe351c"],"user_following":[null,"c016cb14-10c2-500b-aaf6-6e2a629de04d","147b9033-7292-5d82-a50e-7927a7b7eafd","b76c6499-27c8-544c-b0a5-c7dfb5d07fa3","854d1587-286e-523c-8d2b-2bb625c6fa97","ac8958e2-d2c5-5361-8eed-ca0e8b6d55a1","27f56235-371b-5106-a169-4cb86ff78d3e","0d019265-a6db-50c5-9324-02ab7764249d","01c8a8ef-7f29-5f26-8809-c1d81d4362b4","699205a1-7dff-5d73-ad2e-e3eca6041e64","3d248ae7-91c3-5d6e-898a-665435049dad"],"user_stories":["$2a$10$9wbuzPZ5GRSKriGsuntCZeWjoarp8D0fxUWqQ/vy5O3iF3AuhdxPC","$2a$10$fbENxQ3qL1bbwZaHMqO30OECddW9Ht1RN/ENd0kE5coUljbnGK/.C","$2a$10$7MPgIsfIMlnWzJHY28yB0.n4ZWmwSyiYQUjg./OUGDiO2n6WT2yTW","$2a$10$mLsMvN2tKnSjuvFM.7KZwuIvdF1wRg6bwl91022uXfnWL7DN6zGiG","$2a$10$ohGvUGCqGWeOhKDNOMNe/OET8TntQksHICyxKeoqxcQJFkPbRKcTq","$2a$10$bRIUh1Y/u4FddH9j2qgUeOcwIoOleZ.kZ5Xx.GbCm38zW4NBlYT/u","$2a$10$UheZeVpKC1kxvfuM2B5hWuDr7mw8TJhjlhB/A7z3CcPf7kHTO3hjy","$2a$10$c3DSGVhqqrWuIQeNRLwqruX/WMiScwVf5d89nPayP9iyyVkua2Xz6","$2a$10$iaCaED48UoA9EsBCFl2NCOhUuZXw2vj5Zz9z.DsuuqwDk5QD8sXN.","$2a$10$uhPyArSJfVEoEPl5KYliAOir/YusVK9DckFqx2GtHNt8w0hfbpr2m"],"notification":["X3NZWAD2q1575219856272","myuTngROJ1575271716756"],"_id":"5d3ae8820b9de120544d93e3","uid":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","fname":"Zack","lname":"Snyder","login_type":"email","__v":1,"hasshed_password":"$2a$13$1RnEDdWCc68vFBqZIfL4f.5Wa2nhCz/2qOyEcmGGxpC2pfZS.g/Ui","gender":"male"}
         */

        @SerializedName("message")
        private String message;
        @SerializedName("jwt_token")
        private String jwt_token;
        @SerializedName("uid")
        private String uid;
        @SerializedName("user")
        private UserBean user;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getJwt_token() {
            return jwt_token;
        }

        public void setJwt_token(String jwt_token) {
            this.jwt_token = jwt_token;
        }

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public UserBean getUser() {
            return user;
        }

        public void setUser(UserBean user) {
            this.user = user;
        }

        public static class UserBean {
            /**
             * email : {"email_status":1,"email_id":"zack@gmail.com"}
             * contact_number : {"contact_no_status":-1}
             * user_status : {"current_status":"I am inevitable"}
             * counts : {"failed_login":0}
             * meta_data : {"password_change_otp":0,"contact_number_verification_otp":0,"email_verification_otp":0}
             * user_name : knightfall_protocol_batman
             * user_blocked_list : []
             * user_reports : []
             * profile_pic : https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565958853-LBffv9NdUGyJ9cqbRR94ruLF8sXoxk9v_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg
             * user_total_posts : 32
             * user_total_followers : 0
             * user_total_following : 0
             * user_followers : ["27f56235-371b-5106-a169-4cb86ff78d3e","37ce61f7-32d3-5c17-8d3d-e5c2d7c36b78","1ffa1d8b-249c-5aa5-9ef5-268d03d27507","47662af-cd07-50ab-b41d-6911662a37f2","c016cb14-10c2-500b-aaf6-6e2a629de04d","fea89666-b728-52e7-84ae-3e95bf8ffd00","c6bc4395-0b4c-5c9a-8c4a-27c9448fdacd","fe70bb50-26d8-555c-b84e-85cdaa47e800","d577f9e5-4ef9-518c-89b9-3be0dd5d4221","b8a0cb9d-ea3d-5acc-9ef5-d1c73abe351c","cd7dacd6-f577-5b6c-923b-0eb8a5cd8f96","d85b1696-8414-594b-bbae-fbeca04cb49a","581c5f66-257f-51a3-b7f1-cdc8101f7507","699205a1-7dff-5d73-ad2e-e3eca6041e64","854d1587-286e-523c-8d2b-2bb625c6fa97","01c8a8ef-7f29-5f26-8809-c1d81d4362b4","b76c6499-27c8-544c-b0a5-c7dfb5d07fa3","decd6131-1bc7-591e-a896-2388417189b6","147b9033-7292-5d82-a50e-7927a7b7eafd","ac8958e2-d2c5-5361-8eed-ca0e8b6d55a1","0d019265-a6db-50c5-9324-02ab7764249d",null,null,"fea89666-b728-52e7-84ae-3e95bf8ffd00","c6bc4395-0b4c-5c9a-8c4a-27c9448fdacd","c6bc4395-0b4c-5c9a-8c4a-27c9448fdacd","d577f9e5-4ef9-518c-89b9-3be0dd5d4221","b76c6499-27c8-544c-b0a5-c7dfb5d07fa3","e47662af-cd07-50ab-b41d-6911662a37f2","c6bc4395-0b4c-5c9a-8c4a-27c9448fdacd","581c5f66-257f-51a3-b7f1-cdc8101f7507","d85b1696-8414-594b-bbae-fbeca04cb49a","decd6131-1bc7-591e-a896-2388417189b6","cd7dacd6-f577-5b6c-923b-0eb8a5cd8f96","27f56235-371b-5106-a169-4cb86ff78d3e","27f56235-371b-5106-a169-4cb86ff78d3e","581c5f66-257f-51a3-b7f1-cdc8101f7507","b8a0cb9d-ea3d-5acc-9ef5-d1c73abe351c"]
             * user_following : [null,"c016cb14-10c2-500b-aaf6-6e2a629de04d","147b9033-7292-5d82-a50e-7927a7b7eafd","b76c6499-27c8-544c-b0a5-c7dfb5d07fa3","854d1587-286e-523c-8d2b-2bb625c6fa97","ac8958e2-d2c5-5361-8eed-ca0e8b6d55a1","27f56235-371b-5106-a169-4cb86ff78d3e","0d019265-a6db-50c5-9324-02ab7764249d","01c8a8ef-7f29-5f26-8809-c1d81d4362b4","699205a1-7dff-5d73-ad2e-e3eca6041e64","3d248ae7-91c3-5d6e-898a-665435049dad"]
             * user_stories : ["$2a$10$9wbuzPZ5GRSKriGsuntCZeWjoarp8D0fxUWqQ/vy5O3iF3AuhdxPC","$2a$10$fbENxQ3qL1bbwZaHMqO30OECddW9Ht1RN/ENd0kE5coUljbnGK/.C","$2a$10$7MPgIsfIMlnWzJHY28yB0.n4ZWmwSyiYQUjg./OUGDiO2n6WT2yTW","$2a$10$mLsMvN2tKnSjuvFM.7KZwuIvdF1wRg6bwl91022uXfnWL7DN6zGiG","$2a$10$ohGvUGCqGWeOhKDNOMNe/OET8TntQksHICyxKeoqxcQJFkPbRKcTq","$2a$10$bRIUh1Y/u4FddH9j2qgUeOcwIoOleZ.kZ5Xx.GbCm38zW4NBlYT/u","$2a$10$UheZeVpKC1kxvfuM2B5hWuDr7mw8TJhjlhB/A7z3CcPf7kHTO3hjy","$2a$10$c3DSGVhqqrWuIQeNRLwqruX/WMiScwVf5d89nPayP9iyyVkua2Xz6","$2a$10$iaCaED48UoA9EsBCFl2NCOhUuZXw2vj5Zz9z.DsuuqwDk5QD8sXN.","$2a$10$uhPyArSJfVEoEPl5KYliAOir/YusVK9DckFqx2GtHNt8w0hfbpr2m"]
             * notification : ["X3NZWAD2q1575219856272","myuTngROJ1575271716756"]
             * _id : 5d3ae8820b9de120544d93e3
             * uid : 22855eb3-fe40-5832-b54e-6f8042ba0f6d
             * fname : Zack
             * lname : Snyder
             * login_type : email
             * __v : 1
             * gender : male
             */

            @SerializedName("email")
            private EmailBean email;
            @SerializedName("contact_number")
            private ContactNumberBean contact_number;
            @SerializedName("user_status")
            private UserStatusBean user_status;
            @SerializedName("meta_data")
            private MetaDataBean meta_data;
            @SerializedName("user_name")
            private String user_name;
            @SerializedName("profile_pic")
            private String profile_pic;
            @SerializedName("user_total_posts")
            private int user_total_posts;
            @SerializedName("user_total_followers")
            private int user_total_followers;
            @SerializedName("user_total_following")
            private int user_total_following;
            @SerializedName("uid")
            private String uid;
            @SerializedName("_id")
            private String underScoreid;
            @SerializedName("fname")
            private String fname;
            @SerializedName("lname")
            private String lname;
            @SerializedName("login_type")
            private String login_type;
            @SerializedName("gender")
            private String gender;
            @SerializedName("user_blocked_list")
            private List<String> user_blocked_list;
            @SerializedName("user_reports")
            private List<String> user_reports;
            @SerializedName("user_followers")
            private List<String> user_followers;
            @SerializedName("user_following")
            private List<String> user_following;
            @SerializedName("user_stories")
            private List<String> user_stories;
            @SerializedName("notification")
            private List<String> notification;

            public EmailBean getEmail() {
                return email;
            }

            public void setEmail(EmailBean email) {
                this.email = email;
            }

            public ContactNumberBean getContact_number() {
                return contact_number;
            }

            public void setContact_number(ContactNumberBean contact_number) {
                this.contact_number = contact_number;
            }

            public String getUnderScoreid() {
                return underScoreid;
            }

            public void setUnderScoreid(String underScoreid) {
                this.underScoreid = underScoreid;
            }

            public UserStatusBean getUser_status() {
                return user_status;
            }

            public void setUser_status(UserStatusBean user_status) {
                this.user_status = user_status;
            }


            public MetaDataBean getMeta_data() {
                return meta_data;
            }

            public void setMeta_data(MetaDataBean meta_data) {
                this.meta_data = meta_data;
            }

            public String getUser_name() {
                return user_name;
            }

            public void setUser_name(String user_name) {
                this.user_name = user_name;
            }

            public String getProfile_pic() {
                return profile_pic;
            }

            public void setProfile_pic(String profile_pic) {
                this.profile_pic = profile_pic;
            }

            public int getUser_total_posts() {
                return user_total_posts;
            }

            public void setUser_total_posts(int user_total_posts) {
                this.user_total_posts = user_total_posts;
            }

            public int getUser_total_followers() {
                return user_total_followers;
            }

            public void setUser_total_followers(int user_total_followers) {
                this.user_total_followers = user_total_followers;
            }

            public int getUser_total_following() {
                return user_total_following;
            }

            public void setUser_total_following(int user_total_following) {
                this.user_total_following = user_total_following;
            }


            public String getUid() {
                return uid;
            }

            public void setUid(String uid) {
                this.uid = uid;
            }

            public String getFname() {
                return fname;
            }

            public void setFname(String fname) {
                this.fname = fname;
            }

            public String getLname() {
                return lname;
            }

            public void setLname(String lname) {
                this.lname = lname;
            }

            public String getLogin_type() {
                return login_type;
            }

            public void setLogin_type(String login_type) {
                this.login_type = login_type;
            }


            public String getGender() {
                return gender;
            }

            public void setGender(String gender) {
                this.gender = gender;
            }

            public List<String> getUser_blocked_list() {
                return user_blocked_list;
            }

            public void setUser_blocked_list(List<String> user_blocked_list) {
                this.user_blocked_list = user_blocked_list;
            }

            public List<String> getUser_reports() {
                return user_reports;
            }

            public void setUser_reports(List<String> user_reports) {
                this.user_reports = user_reports;
            }

            public List<String> getUser_followers() {
                return user_followers;
            }

            public void setUser_followers(List<String> user_followers) {
                this.user_followers = user_followers;
            }

            public List<String> getUser_following() {
                return user_following;
            }

            public void setUser_following(List<String> user_following) {
                this.user_following = user_following;
            }

            public List<String> getUser_stories() {
                return user_stories;
            }

            public void setUser_stories(List<String> user_stories) {
                this.user_stories = user_stories;
            }

            public List<String> getNotification() {
                return notification;
            }

            public void setNotification(List<String> notification) {
                this.notification = notification;
            }

            public static class EmailBean {
                /**
                 * email_status : 1
                 * email_id : zack@gmail.com
                 */

                @SerializedName("email_status")
                private int email_status;
                @SerializedName("email_id")
                private String email_id;

                public int getEmail_status() {
                    return email_status;
                }

                public void setEmail_status(int email_status) {
                    this.email_status = email_status;
                }

                public String getEmail_id() {
                    return email_id;
                }

                public void setEmail_id(String email_id) {
                    this.email_id = email_id;
                }
            }

            public static class ContactNumberBean {
                /**
                 * contact_no_status : -1
                 */

                private int contact_no_status;

                public int getContact_no_status() {
                    return contact_no_status;
                }

                public void setContact_no_status(int contact_no_status) {
                    this.contact_no_status = contact_no_status;
                }
            }

            public static class UserStatusBean {
                /**
                 * current_status : I am inevitable
                 */

                private String current_status;

                public String getCurrent_status() {
                    return current_status;
                }

                public void setCurrent_status(String current_status) {
                    this.current_status = current_status;
                }
            }

            public static class CountsBean {
                /**
                 * failed_login : 0
                 */

                @SerializedName("failed_login")
                private int failed_login;

                public int getFailed_login() {
                    return failed_login;
                }

                public void setFailed_login(int failed_login) {
                    this.failed_login = failed_login;
                }
            }

            public static class MetaDataBean {
                /**
                 * password_change_otp : 0
                 * contact_number_verification_otp : 0
                 * email_verification_otp : 0
                 */

                @SerializedName("password_change_otp")
                private int password_change_otp;
                @SerializedName("contact_number_verification_otp")
                private int contact_number_verification_otp;
                @SerializedName("email_verification_otp")
                private int email_verification_otp;

                public int getPassword_change_otp() {
                    return password_change_otp;
                }

                public void setPassword_change_otp(int password_change_otp) {
                    this.password_change_otp = password_change_otp;
                }

                public int getContact_number_verification_otp() {
                    return contact_number_verification_otp;
                }

                public void setContact_number_verification_otp(int contact_number_verification_otp) {
                    this.contact_number_verification_otp = contact_number_verification_otp;
                }

                public int getEmail_verification_otp() {
                    return email_verification_otp;
                }

                public void setEmail_verification_otp(int email_verification_otp) {
                    this.email_verification_otp = email_verification_otp;
                }
            }
        }
    }
}
