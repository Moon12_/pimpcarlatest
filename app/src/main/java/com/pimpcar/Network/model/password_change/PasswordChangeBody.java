package com.pimpcar.Network.model.password_change;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class PasswordChangeBody implements Parcelable {


    public PasswordChangeBody() {
    }

    /**
     * data : {"attributes":{"password":"test12345"}}
     */

    @SerializedName("data")
    private DataBean data;

    protected PasswordChangeBody(Parcel in) {
    }

    public static final Creator<PasswordChangeBody> CREATOR = new Creator<PasswordChangeBody>() {
        @Override
        public PasswordChangeBody createFromParcel(Parcel in) {
            return new PasswordChangeBody(in);
        }

        @Override
        public PasswordChangeBody[] newArray(int size) {
            return new PasswordChangeBody[size];
        }
    };

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public static class DataBean {
        /**
         * attributes : {"password":"test12345"}
         */

        @SerializedName("attributes")
        private AttributesBean attributes;

        public AttributesBean getAttributes() {
            return attributes;
        }

        public void setAttributes(AttributesBean attributes) {
            this.attributes = attributes;
        }

        public static class AttributesBean {
            /**
             * password : test12345
             */

            @SerializedName("password")
            private String password;

            public String getPassword() {
                return password;
            }

            public void setPassword(String password) {
                this.password = password;
            }
        }
    }
}
