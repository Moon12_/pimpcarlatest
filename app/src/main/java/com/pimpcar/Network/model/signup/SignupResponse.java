package com.pimpcar.Network.model.signup;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class SignupResponse implements Parcelable {
    /**
     * data : {"message":"user created successfully","jwt_token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6Ijg5ZGUyN2FmLWZiNGItNTQxMC04YzU3LTc4ODY3YTg1ODdjYSIsImlhdCI6MTU1OTg4NDIxNiwiZXhwIjoxNTkxNDQxODE2fQ.no1uMHwth_HnF5WYbNIId5hZxWCPP7tg4jm1mro09Xk"}
     */

    @SerializedName("data")
    private DataBean data;

    protected SignupResponse(Parcel in) {
    }

    public static final Creator<SignupResponse> CREATOR = new Creator<SignupResponse>() {
        @Override
        public SignupResponse createFromParcel(Parcel in) {
            return new SignupResponse(in);
        }

        @Override
        public SignupResponse[] newArray(int size) {
            return new SignupResponse[size];
        }
    };

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public static class DataBean {
        /**
         * message : user created successfully
         * uid: f7b16823-dd4b-5e8d-a06a-f220b26bb91e
         * jwt_token : eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6Ijg5ZGUyN2FmLWZiNGItNTQxMC04YzU3LTc4ODY3YTg1ODdjYSIsImlhdCI6MTU1OTg4NDIxNiwiZXhwIjoxNTkxNDQxODE2fQ.no1uMHwth_HnF5WYbNIId5hZxWCPP7tg4jm1mro09Xk
         */

        @SerializedName("message")
        private String message;
        @SerializedName("jwt_token")
        private String jwt_token;
        @SerializedName("uid")
        private String uid;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getJwt_token() {
            return jwt_token;
        }

        public void setJwt_token(String jwt_token) {
            this.jwt_token = jwt_token;
        }

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }
    }
}
