package com.pimpcar.Network.model.user_dob;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class UserDOBResponse implements Parcelable {
    /**
     * data : {"message":"user date of birth updated successfully"}
     */

    @SerializedName("data")
    private DataBean data;

    protected UserDOBResponse(Parcel in) {
    }

    public static final Creator<UserDOBResponse> CREATOR = new Creator<UserDOBResponse>() {
        @Override
        public UserDOBResponse createFromParcel(Parcel in) {
            return new UserDOBResponse(in);
        }

        @Override
        public UserDOBResponse[] newArray(int size) {
            return new UserDOBResponse[size];
        }
    };

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public static class DataBean {
        /**
         * z
         * message : user date of birth updated successfully
         */

        @SerializedName("message")
        private String message;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}
