package com.pimpcar.Network.model.user_search;

import com.google.gson.annotations.SerializedName;

public class UserSearchRequestBody {
    /**
     * data : {"attributes":{"search_keyword":"wat"}}
     */

    @SerializedName("data")
    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * attributes : {"search_keyword":"wat"}
         */

        @SerializedName("attributes")
        private AttributesBean attributes;

        public AttributesBean getAttributes() {
            return attributes;
        }

        public void setAttributes(AttributesBean attributes) {
            this.attributes = attributes;
        }

        public static class AttributesBean {
            /**
             * search_keyword : wat
             */

            @SerializedName("search_keyword")
            private String search_keyword;

            public String getSearch_keyword() {
                return search_keyword;
            }

            public void setSearch_keyword(String search_keyword) {
                this.search_keyword = search_keyword;
            }
        }
    }
}
