package com.pimpcar.Network.model.useremailverification;

import com.google.gson.annotations.SerializedName;

public class UserEmailOTPVerifyResponse {
    /**
     * data : {"message":"otp sent to email sahityakumarsuman@gmail.com"}
     */

    @SerializedName("data")
    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * message : otp sent to email sahityakumarsuman@gmail.com
         */

        @SerializedName("message")
        private String message;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}
