package com.pimpcar.Network.model.usergender;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class UserGenderProfileResponse implements Parcelable {
    /**
     * data : {"message":"profile image uploaded successfully","image_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/5b48f793-76e4-5908-b0b7-d5da37fed68e/1560248840-ovzhiYNAmEqjcsH0oQUjK6LYdFwPutNo_5b48f793-76e4-5908-b0b7-d5da37fed68e_5b48f793-76e4-5908-b0b7-d5da37fed68e_sahitya_kumar.jpg"}
     */

    public UserGenderProfileResponse() {

    }

    @SerializedName("data")
    private DataBean data;

    protected UserGenderProfileResponse(Parcel in) {
    }

    public static final Creator<UserGenderProfileResponse> CREATOR = new Creator<UserGenderProfileResponse>() {
        @Override
        public UserGenderProfileResponse createFromParcel(Parcel in) {
            return new UserGenderProfileResponse(in);
        }

        @Override
        public UserGenderProfileResponse[] newArray(int size) {
            return new UserGenderProfileResponse[size];
        }
    };

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public static class DataBean {
        /**
         * message : profile image uploaded successfully
         * image_url : https://s3.us-east-2.amazonaws.com/pimpcarprofile/5b48f793-76e4-5908-b0b7-d5da37fed68e/1560248840-ovzhiYNAmEqjcsH0oQUjK6LYdFwPutNo_5b48f793-76e4-5908-b0b7-d5da37fed68e_5b48f793-76e4-5908-b0b7-d5da37fed68e_sahitya_kumar.jpg
         */

        @SerializedName("message")
        private String message;
        @SerializedName("image_url")
        private String image_url;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getImage_url() {
            return image_url;
        }

        public void setImage_url(String image_url) {
            this.image_url = image_url;
        }
    }
}
