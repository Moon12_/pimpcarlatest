package com.pimpcar.PostEditting.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.Spannable;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.FragmentManager;

import com.ironsource.mediationsdk.IronSource;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.model.Placement;
import com.ironsource.mediationsdk.sdk.RewardedVideoListener;
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;
import com.pimpcar.Network.ApiClient;
import com.pimpcar.Network.ApiService;
import com.pimpcar.Network.model.posts.SubmitPostRequestBody;
import com.pimpcar.Network.model.posts.SubmitPostResponseBody;
import com.pimpcar.PostEditting.activity.models.TaggedUserData;
import com.pimpcar.PostEditting.fragments.UserSearchDialogFragment;
import com.pimpcar.PostEditting.fragments.events.RecyclerViewItemClickMode;
import com.pimpcar.R;
import com.pimpcar.UserAuthentication.eventbus.models.ShowToastEventModel;
import com.pimpcar.Widgets.CircleImageView;
import com.pimpcar.Widgets.ProportionalImageView;
import com.pimpcar.Widgets.progressbar.KProgressHUD;
import com.pimpcar.utils.Constants;
import com.pimpcar.utils.HelperMethods;
import com.pimpcar.utils.NetworkResponseMessages;
import com.pimpcar.utils.ResourcesUtil;
import com.pimpcar.utils.Utils;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;

public class PostEditActivity extends AppCompatActivity implements UserSearchDialogFragment.OnCompleteListener, View.OnTouchListener {


    private static final int TAG_USER_REQUEST_CODE = 6574;
    @BindView(R.id.done_post)
    AppCompatImageView done_post;

    @BindView(R.id.image_container)
    ProportionalImageView image_container;

    @BindView(R.id.post_hashtags)
    AppCompatEditText post_hashtags;

    @BindView(R.id.first_person_image)
    AppCompatImageView first_person_image;

    @BindView(R.id.second_person_dp)
    AppCompatImageView second_person_dp;

    @BindView(R.id.third_person_dp)
    AppCompatImageView third_person_dp;

    @BindView(R.id.fourth_person_dp)
    AppCompatImageView fourth_person_dp;

    @BindView(R.id.number_of_people_tagged_count_Tv)
    AppCompatTextView number_of_people_tagged_count_Tv;

    @BindView(R.id.post_tags_ET)
    AppCompatEditText post_tags_ET;

    @BindView(R.id.post_location)
    AppCompatEditText post_location;


    @BindView(R.id.back_button)
    AppCompatImageView back_button;

    @BindView(R.id.image_add_on_relative_layout)
    RelativeLayout image_add_on_relative_layout;

    @BindView(R.id.image_rotate_FB)
    AppCompatImageView image_rotate_FB;

    @BindView(R.id.save_image_in_drive)
    AppCompatImageView save_image_in_drive;
    UserSearchDialogFragment dialogFragment;
    int angle = 0;
    int taggedUser = 0;
    String startChar = "";
    String locationString = "";
    private Bitmap mainBitmapImage;
    private String selected_image_path;
    private Bitmap main_bitmap_image;
    private List<TaggedUserData> taggedUserDataList = new ArrayList<>();
    private Spannable mspanable;
    private int hashTagIsComing = 0;
    private ApiService apiService;
    private CompositeDisposable disposable = new CompositeDisposable();
    private KProgressHUD hud;
    private int _xDelta;
    private int _yDelta;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pimp_image_postdetails);
        IronSource.init(this, "11e2d1a55", IronSource.AD_UNIT.REWARDED_VIDEO);
        ButterKnife.bind(this);
        apiService = ApiClient.getClient(getApplicationContext()).create(ApiService.class);

        IronSource.showRewardedVideo("DefaultRewardedVideo");

        Bundle bundle = getIntent().getExtras();

        selected_image_path = bundle.getString("finished_image");
        angle = bundle.getInt("angleValue");

        String data = bundle.getString("home");


        Log.d("checkingValue", "" + angle);
        File sd = Environment.getExternalStorageDirectory();
        File imageFile = new File(selected_image_path);
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        main_bitmap_image = BitmapFactory.decodeFile(imageFile.getPath(), bmOptions);


        int width = main_bitmap_image.getWidth();
        Log.d("widthImage", " " + width);
        int height = main_bitmap_image.getHeight();
        Log.d("widthImage", " " + height);


        if (height > width) {
            if (data != null) {
                main_bitmap_image = rotate_bitmap(main_bitmap_image, -90);
            } else {
                if (angle == 270) {
                    main_bitmap_image = rotate_bitmap(main_bitmap_image, -270);
                } else {
                    main_bitmap_image = rotate_bitmap(main_bitmap_image, -90);
                }
            }

            image_container.setImageBitmap(main_bitmap_image);

        } else {
            image_container.setImageBitmap(main_bitmap_image);
        }

        image_container.setOnClickListener(v -> {


            if (taggedUserDataList.size() < 10) {
                FragmentManager fm = getSupportFragmentManager();
                dialogFragment = UserSearchDialogFragment.newInstance("Some title");
                dialogFragment.show(fm, "fragment_alert");

                if (image_add_on_relative_layout.getChildCount() > 1) {
                    for (int i = 0; i < taggedUserDataList.size(); i++) {
                        float x_pivot = image_add_on_relative_layout.getChildAt(i + 1).getPivotX();
                        float y_pivot = image_add_on_relative_layout.getChildAt(i + 1).getPivotY();
                        taggedUserDataList.get(i).setPivot_x_piont(x_pivot);
                        taggedUserDataList.get(i).setPivot_y_point(y_pivot);
                        Log.d("checkingTaged", " " + taggedUserDataList);
                    }
                }
            } else {
                Toast.makeText(PostEditActivity.this, ResourcesUtil.getString(R.string.string_you_cant_tag_more), Toast.LENGTH_LONG);
            }
        });


        post_location.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                locationString = String.valueOf(s);

            }
        });

        post_hashtags.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                try {
                    //  startChar = Character.toString(s.charAt(s));
                    startChar = String.valueOf(s);
                    Log.d("checkString", "CHARACTER OF NEW WORD: " + startChar);
                } catch (Exception ex) {
                    startChar = "";
                }

            }
        });
        IronSource.setRewardedVideoListener(new RewardedVideoListener() {
            /**
             * Invoked when the RewardedVideo ad view has opened.
             * Your Activity will lose focus. Please avoid performing heavy
             * tasks till the video ad will be closed.
             */
            @Override
            public void onRewardedVideoAdOpened() {
                Toast.makeText(PostEditActivity.this, "Rewarded ad opened", Toast.LENGTH_SHORT).show();

            }

            /*Invoked when the RewardedVideo ad view is about to be closed.
            Your activity will now regain its focus.*/
            @Override
            public void onRewardedVideoAdClosed() {
                Toast.makeText(PostEditActivity.this, "ad closed", Toast.LENGTH_SHORT).show();
//                String file_path = Environment.getExternalStorageDirectory().getAbsolutePath() +
//                        "/PimpCar";
//                File dir = new File(file_path);
//                if (!dir.exists())
//                    dir.mkdirs();
//                File file = new File(dir, "pimped_image" + System.currentTimeMillis() + ".png");
//                FileOutputStream fOut = null;
//                try {
//                    fOut = new FileOutputStream(file);
//                    main_bitmap_image.compress(Bitmap.CompressFormat.PNG, 85, fOut);
//                    fOut.flush();
//                    fOut.close();
//                    HelperMethods.showToastbar(PostEditActivity.this, "Image Saved Successfully");
//                } catch (FileNotFoundException e) {
//                    e.printStackTrace();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }

            }

            /**
             * Invoked when there is a change in the ad availability status.
             *
             * @param - available - value will change to true when rewarded videos are *available.
             *          You can then show the video by calling showRewardedVideo().
             *          Value will change to false when no videos are available.
             */
            @Override
            public void onRewardedVideoAvailabilityChanged(boolean available) {
                //Change the in-app 'Traffic Driver' state according to availability.
            }

            /**
             /**
             * Invoked when the user completed the video and should be rewarded.
             * If using server-to-server callbacks you may ignore this events and wait *for the callback from the ironSource server.
             *
             * @param - placement - the Placement the user completed a video from.
             */
            @Override
            public void onRewardedVideoAdRewarded(Placement placement) {

            }

            /* Invoked when RewardedVideo call to show a rewarded video has failed
             * IronSourceError contains the reason for the failure.
             */
            @Override
            public void onRewardedVideoAdShowFailed(IronSourceError error) {
            }

            /*Invoked when the end user clicked on the RewardedVideo ad
             */
            @Override
            public void onRewardedVideoAdClicked(Placement placement) {

            }

            @Override
            public void onRewardedVideoAdStarted() {
                Toast.makeText(PostEditActivity.this, "Rewarded ad started", Toast.LENGTH_SHORT).show();
            }

            /* Invoked when the video ad finishes plating. */
            @Override
            public void onRewardedVideoAdEnded() {
            }
        });
    }


    @OnClick(R.id.image_rotate_FB)
    public void rotate_image() {
        Bitmap major_bitmap = rotate_bitmap(main_bitmap_image, 90);
        main_bitmap_image = major_bitmap;
        image_container.setImageBitmap(main_bitmap_image);
    }

    private void changeTheColor(String s, int start, int end) {
        mspanable.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.theme_blue)), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
    }

    @OnClick(R.id.done_post)
    public void submit_post() {
        HelperMethods.LogValue("checkingValuesImages", " " + main_bitmap_image + " \n" + startChar + " \n" + locationString + "\n" + taggedUserDataList.size());

        put_it_to_server(main_bitmap_image, startChar, locationString);
//        Log.d("checkingValuesImages",""+main_bitmap_image+" \n"+startChar+" \n"+locationString);


    }

    @OnClick(R.id.save_image_in_drive)
    public void save_image_drive() {

        IronSource.showRewardedVideo("DefaultRewardedVideo");
        String file_path = Environment.getExternalStorageDirectory().getAbsolutePath() +
                "/PimpCar";
        File dir = new File(file_path);
        if (!dir.exists())
            dir.mkdirs();
        File file = new File(dir, "pimped_image" + System.currentTimeMillis() + ".png");
        FileOutputStream fOut = null;
        try {
            fOut = new FileOutputStream(file);
            main_bitmap_image.compress(Bitmap.CompressFormat.PNG, 85, fOut);
            fOut.flush();
            fOut.close();
            HelperMethods.showToastbar(this, "Image Saved Successfully");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private void saveBitmap(Bitmap bitmap, String path) {
        if (bitmap != null) {
            try {
                FileOutputStream outputStream = null;
                try {
                    outputStream = new FileOutputStream(path); //here is set your file path where you want to save or also here you can set file object directly

                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream); // bitmap is your Bitmap instance, if you want to compress it you can compress reduce percentage
                    // PNG is a lossless format, the compression factor (100) is ignored
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (outputStream != null) {
                            outputStream.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public Bitmap rotate_bitmap(Bitmap bitmap_org, int ror) {
        Matrix matrix = new Matrix();
        matrix.postRotate(ror);

        Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap_org, bitmap_org.getWidth(), bitmap_org.getHeight(), true);

        Bitmap rotatedBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);

        return rotatedBitmap;
    }

    private void put_it_to_server(Bitmap main_bitmap_image, String hashTag, String location) {

        show_progress_dialog();

        SubmitPostRequestBody body_for_submit_post = Utils.create_body_for_submit_post(main_bitmap_image, hashTag, location, taggedUserDataList);
        disposable.add(apiService.submitPost(HelperMethods.get_user_jwt_key(), body_for_submit_post)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<SubmitPostResponseBody>() {
                    @Override
                    public void onSuccess(SubmitPostResponseBody s) {
                        if (s != null && s.getData().getMessage().contentEquals(NetworkResponseMessages.POST_SUBMIT_MESSAGE)) {
                            HelperMethods.showToastbar(PostEditActivity.this, ResourcesUtil.getString(R.string.post_submitted_successfully));
                            dismiss_progress_dialog();
                            Intent returnIntent = new Intent();
                            setResult(Activity.RESULT_OK, returnIntent);
                            finish();
                        }
                    }

                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onError(Throwable e) {

                        dismiss_progress_dialog();

                        if (e instanceof HttpException) {
                            ResponseBody body = ((HttpException) e).response().errorBody();
                            try {
                                String error_body = body.string();
                                String error = Utils.get_error_from_response(error_body);


                                EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, error.toUpperCase()));
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                }));
    }

    public void show_progress_dialog() {
        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE);
        hud.setSize(50, 50);
        hud.show();
    }

    public void dismiss_progress_dialog() {
        if (hud != null && hud.isShowing())
            hud.dismiss();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @OnClick(R.id.back_button)
    public void onclick_back_button() {

        onBackPressed();
    }

    public void add_view_image_view(String user_profile_pic, String user_name) {
        View tag_view = get_tag_view(user_name, user_profile_pic);
        tag_view.setOnTouchListener(this);
        image_add_on_relative_layout.addView(tag_view, 1);

    }

    public boolean onTouch(View view, MotionEvent event) {
        final int X = (int) event.getRawX();
        final int Y = (int) event.getRawY();
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                RelativeLayout.LayoutParams lParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
                _xDelta = X - lParams.leftMargin;
                _yDelta = Y - lParams.topMargin;
                break;
            case MotionEvent.ACTION_UP:
                break;
            case MotionEvent.ACTION_POINTER_DOWN:
                break;
            case MotionEvent.ACTION_POINTER_UP:
                break;
            case MotionEvent.ACTION_MOVE:
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
                layoutParams.leftMargin = X - _xDelta;
                layoutParams.topMargin = Y - _yDelta;
                layoutParams.rightMargin = -250;
                layoutParams.bottomMargin = -250;
                view.setLayoutParams(layoutParams);
                break;
        }
        image_add_on_relative_layout.invalidate();
        return true;
    }

    public View get_tag_view(String user_name, String user_profile_pic) {
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View rootView = layoutInflater.inflate(R.layout.touch_tag_view, null);

        CircleImageView pic_view = (CircleImageView) rootView.findViewById(R.id.tag_person_pic);
        AppCompatTextView u_name = (AppCompatTextView) rootView.findViewById(R.id.user_name_tagged);

        u_name.setText("@" + user_name);

        if (user_profile_pic != null && !user_profile_pic.isEmpty()) {
            Picasso.with(this)
                    .load(user_profile_pic)
                    .fit()
                    .into(pic_view);
        }
        return rootView;
    }

    @Override
    public void onComplete(RecyclerViewItemClickMode recyclerViewItemClickMode) {

        taggedUserDataList.add(new TaggedUserData(recyclerViewItemClickMode.getUser_id(), recyclerViewItemClickMode.getUser_profile_pic(), recyclerViewItemClickMode.getUser_name()));

        taggedUser = taggedUser + taggedUserDataList.size();

        number_of_people_tagged_count_Tv.setText(taggedUser + " friends");

        if (taggedUserDataList.size() > 0) {

            if (taggedUserDataList.size() == 1) {
                first_person_image.setVisibility(View.VISIBLE);
                second_person_dp.setVisibility(View.INVISIBLE);
                third_person_dp.setVisibility(View.INVISIBLE);
                fourth_person_dp.setVisibility(View.INVISIBLE);
            }

            if (taggedUserDataList.size() == 2) {
                first_person_image.setVisibility(View.VISIBLE);
                second_person_dp.setVisibility(View.VISIBLE);
                third_person_dp.setVisibility(View.INVISIBLE);
                fourth_person_dp.setVisibility(View.INVISIBLE);

            }

            if (taggedUserDataList.size() == 3) {
                first_person_image.setVisibility(View.VISIBLE);
                second_person_dp.setVisibility(View.VISIBLE);
                third_person_dp.setVisibility(View.VISIBLE);
                fourth_person_dp.setVisibility(View.INVISIBLE);
            }

            if (taggedUserDataList.size() == 4) {
                first_person_image.setVisibility(View.VISIBLE);
                second_person_dp.setVisibility(View.VISIBLE);
                third_person_dp.setVisibility(View.VISIBLE);
                fourth_person_dp.setVisibility(View.VISIBLE);
            }

        } else {
            first_person_image.setVisibility(View.INVISIBLE);
            second_person_dp.setVisibility(View.INVISIBLE);
            third_person_dp.setVisibility(View.INVISIBLE);
            fourth_person_dp.setVisibility(View.INVISIBLE);
        }


        if (taggedUserDataList.size() > 1) {
            check_if_anyone_is_already_tagged();
        }

        if (taggedUserDataList.size() == 1) {
            add_view_image_view(recyclerViewItemClickMode.getUser_profile_pic(), recyclerViewItemClickMode.getUser_name());
        }


        for (int i = 0; i < taggedUserDataList.size(); i++) {

            if (i == 0) {
                if (taggedUserDataList.get(i).getUser_profile_pic() != null && !taggedUserDataList.get(i).getUser_profile_pic().isEmpty()) {
                    Picasso.with(this)
                            .load(taggedUserDataList.get(i).getUser_profile_pic())
                            .fit()
                            .into(first_person_image);
                }
            }

            if (i == 1) {
                if (taggedUserDataList.get(i).getUser_profile_pic() != null && !taggedUserDataList.get(i).getUser_profile_pic().isEmpty()) {
                    Picasso.with(this)
                            .load(taggedUserDataList.get(i).getUser_profile_pic())
                            .fit()
                            .into(second_person_dp);
                }
            }
            if (i == 2) {
                if (taggedUserDataList.get(i).getUser_profile_pic() != null && !taggedUserDataList.get(i).getUser_profile_pic().isEmpty()) {
                    Picasso.with(this)
                            .load(taggedUserDataList.get(i).getUser_profile_pic())
                            .fit()
                            .into(third_person_dp);
                }
            }
            if (i == 3) {
                if (taggedUserDataList.get(i).getUser_profile_pic() != null && !taggedUserDataList.get(i).getUser_profile_pic().isEmpty()) {
                    Picasso.with(this)
                            .load(taggedUserDataList.get(i).getUser_profile_pic())
                            .fit()
                            .into(fourth_person_dp);
                }
            }

        }
    }

    private void check_if_anyone_is_already_tagged() {
        if (taggedUserDataList.size() > 0) {
            for (int i = 0; i < taggedUserDataList.size(); i++) {
                View tagged_view = get_tag_view(taggedUserDataList.get(i).getUser_username(), taggedUserDataList.get(i).getUser_profile_pic());
                if (taggedUserDataList.get(i).getPivot_x_piont() > 0) {
                    tagged_view.setPivotX(taggedUserDataList.get(i).getPivot_x_piont());
                    tagged_view.setPivotY(taggedUserDataList.get(i).getPivot_y_point());
                }
                tagged_view.setOnTouchListener(this);
                image_add_on_relative_layout.addView(tagged_view);
            }
        }
    }

    protected void onResume() {
        super.onResume();
        IronSource.onResume(this);
    }

    protected void onPause() {
        super.onPause();
        IronSource.onPause(this);
    }
}
