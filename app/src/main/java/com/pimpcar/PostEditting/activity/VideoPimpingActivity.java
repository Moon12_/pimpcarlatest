package com.pimpcar.PostEditting.activity;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;

import com.pimpcar.HomeFeed.activity.HomeFeedActivity;
import com.pimpcar.Network.ApiClient;
import com.pimpcar.R;
import com.pimpcar.Widgets.progressbar.KProgressHUD;
import com.pimpcar.utils.HelperMethods;
import com.pimpcar.utils.Utilities;
import com.pimpcar.utils.Utils;
import com.pimpcar.videoPart.VideoInterface;
import com.varunjohn1990.iosdialogs4android.IOSDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class VideoPimpingActivity extends AppCompatActivity implements View.OnClickListener {

    private String url = "https://www.google.com";
    private static final int BUFFER_SIZE = 1024 * 2;
    private static final String IMAGE_DIRECTORY = "/demonuts_upload_gallery";

    VideoView videoView;
    Button button2;
    View rewind, fwd;
    Double videoDurationss;
    RelativeLayout rela;
    ImageView icoeOne, icoeTwo, videoBtn;
    KProgressHUD hud;
    SeekBar seekBar;
    int progress;
    TextView tvUplaod, tvUrl;
    AppCompatEditText etPost_hashtags, etPostUserTags, etDescritopns;
    RelativeLayout rlUplaodBtn;
    ImageView resume, pause, back;
    MediaController mediaController;
    RelativeLayout relativeLayout2;
    private String selected_image_path = "", getOriginalPath = "";
    String data = "", path = "";
    Uri getUri, getOriginalUri;
    private Handler mHandler = new Handler();
    private Runnable updateTimeTask = new Runnable() {

        @Override
        public void run() {
            long totalDuration = videoView.getDuration();
            long currentDuration = videoView.getCurrentPosition();

            progress = (int) (Utils.getProgressPercentage(currentDuration,
                    totalDuration));
            seekBar.setProgress(progress);

            if (progress == 100) {
                videoView.start();
                resume.setVisibility(View.INVISIBLE);
                pause.setVisibility(View.VISIBLE);
            }
            mHandler.postDelayed(this, 100);

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_pimping);

        Bundle bundle = getIntent().getExtras();
        getOriginalPath = bundle.getString("selected_video");
        getOriginalUri = Uri.parse(bundle.getString("selected_uri"));
        selected_image_path = Utilities.getString(VideoPimpingActivity.this, "selected_video");
        getUri = Uri.parse(Utilities.getString(VideoPimpingActivity.this, "selected_uri"));
        data = Utilities.getString(VideoPimpingActivity.this, "data");
        selected_image_path = getOriginalPath;
        getUri = getOriginalUri;
        Log.d("checkingValue", "" + selected_image_path);
        data = bundle.getString("data");
        path = getFilePathFromURI(VideoPimpingActivity.this, getUri);
//        Toast.makeText(VideoPimpingActivity.this, HelperMethods.get_underScoreId() , Toast.LENGTH_SHORT).show();
        videoView = findViewById(R.id.videoView);
        videoBtn = findViewById(R.id.videoUplaod);
        etDescritopns = findViewById(R.id.etDescritopns);
        etPostUserTags = findViewById(R.id.etPostUserTags);
        rlUplaodBtn = findViewById(R.id.rlUplaodBtn);
        etPost_hashtags = findViewById(R.id.etPost_hashtags);
        tvUplaod = findViewById(R.id.tvUplaod);
        icoeOne = findViewById(R.id.icoeOne);
        tvUrl = findViewById(R.id.tvUrl);
        icoeTwo = findViewById(R.id.iconTwo);
        seekBar = findViewById(R.id.progress);
        resume = findViewById(R.id.resume);
        pause = findViewById(R.id.pause);
        back = findViewById(R.id.back);
        relativeLayout2 = findViewById(R.id.relativeLayout2);
        back.setOnClickListener(this);
        pause.setOnClickListener(this);
        resume.setOnClickListener(this);
        rlUplaodBtn.setOnClickListener(this);
        tvUrl.setText(path);

        if (selected_image_path != null) {
            if (data.equals("camera")) {
                videoView.setVideoPath(selected_image_path);
            } else {
                videoView.setVideoURI(Uri.parse(selected_image_path));
            }
            start_Video();
        }


        RequestBody requestBody = RequestBody.create(MediaType.parse("video/*"), selected_image_path);
        MultipartBody.Part part = MultipartBody.Part.createFormData("abc[]", selected_image_path, requestBody);

        Log.d("CheckingVider", "" + part);

        rewind = findViewById(R.id.rewind);
        fwd = findViewById(R.id.fwd);
        fwd.setOnClickListener(new DoubleClickListener() {
            @Override
            public void onSingleClick(View v) {
                mediaController.show();
            }

            @Override
            public void onDoubleClick(View v) {

                videoSeeker("fwd");
            }
        });
        videoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.setType("video/*");
                startActivityForResult(intent, 1);
            }
        });

        rewind.setOnClickListener(new DoubleClickListener() {
            @Override
            public void onSingleClick(View v) {
                mediaController.show();
            }

            @Override
            public void onDoubleClick(View v) {
                videoSeeker("rwd");
            }
        });

    }

    private void videoSeeker(String type) {
        int pos = videoView.getCurrentPosition();
        if (type.equals("rwd")) {
            pos -= 5000;
            icoeOne.setVisibility(View.VISIBLE);
        } else {
            pos += 5000;
            icoeTwo.setVisibility(View.VISIBLE);
        }
        videoView.seekTo(pos);

        new Handler().postDelayed(() -> {
            if (type.equals("rwd")) {
                icoeOne.setVisibility(View.INVISIBLE);
            } else {
                icoeTwo.setVisibility(View.INVISIBLE);
            }

        }, 1000);
    }

    private void start_Video() {
        mediaController = new MediaController(this);
        mediaController.setAnchorView(videoView);
        videoView.setMediaController(mediaController);
        videoView.start();
        mHandler.postDelayed(updateTimeTask, 100);
        getDuration(VideoPimpingActivity.this, path);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.pause: {
                //  pauseVideo();
                break;
            }
            case R.id.rlUplaodBtn: {
                String postUserTags = etPostUserTags.getText().toString();
                String hashTags = etPost_hashtags.getText().toString();
                String descriptions = etDescritopns.getText().toString();
                String timeStamp = String.valueOf(Calendar.getInstance().getTimeInMillis());
//                String timeStamp = String.valueOf(TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()));
//                Toast.makeText(VideoPimpingActivity.this, HelperMethods.get_underScoreId(), Toast.LENGTH_SHORT).show();
                videoView.pause();
                if (!hashTags.equals("")) {
                    if (!postUserTags.equals("")) {
                        if (!descriptions.equals("")) {

                            if (videoDurationss <= 30) {
                                uploadVideo(path, hashTags, postUserTags, descriptions, timeStamp);

                            } else {
                                showAlertMessageDialog("Maximum video duration is 30s, Retry?");
                            }
                        } else {
                            Toast.makeText(VideoPimpingActivity.this, "Enter Description", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(VideoPimpingActivity.this, "Enter Post user Tags", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(VideoPimpingActivity.this, "Enter Hash Tags", Toast.LENGTH_SHORT).show();
                }

                break;
            }

            case R.id.resume: {
                //   resumeVideo();
                break;
            }

            case R.id.back: {
                finish();
                break;
            }
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        videoView.pause();

    }

    @Override
    protected void onResume() {
        super.onResume();
        videoView.resume();
    }

    private void uploadVideo(String path, String hashTgas, String userTags, String descriptions, String timeStamp) {
        hud = KProgressHUD.create(VideoPimpingActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE);
        hud.setSize(50, 50);
        hud.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(VideoInterface.VIDEOURL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();

        //Create a file object using file path
        File file = new File(path);
        // Parsing any Media type file
        RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"), file);
        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("file", file.getName(), requestBody);
        RequestBody description = RequestBody.create(MediaType.parse("text/plain"), descriptions);
//        RequestBody filename = RequestBody.create(MediaType.parse("text/plain"), pdfname);
        RequestBody hash_tags = RequestBody.create(MediaType.parse("text/plain"), hashTgas);
        RequestBody post_users_tag = RequestBody.create(MediaType.parse("text/plain"), userTags);
        RequestBody user_id = RequestBody.create(MediaType.parse("text/plain"), HelperMethods.get_underScoreId());
        RequestBody post_timestamp = RequestBody.create(MediaType.parse("text/plain"), timeStamp);

//        VideoInterface service = ApiClient.getClientUpdated(VideoPimpingActivity.this).create(VideoInterface.class);
//        Call<String> call = service.uploadImage(fileToUpload, description, hash_tags, post_users_tag, user_id, post_timestamp);

        VideoInterface getResponse = retrofit.create(VideoInterface.class);
//        Call<String> call = getResponse.uploadImage(fileToUpload,"descriptions","hashtags","userTags","6184be3e6274da6a8865e3ed","1637392740");
        Call<String> call = getResponse.uploadImage(fileToUpload, description, hash_tags, post_users_tag, user_id, post_timestamp);
        Log.d("assss", "asss");
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Log.d("mullllll", response.body().toString());
                if (response.code() == 200) {
                    hud.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().toString());
//                        Toast.makeText(getApplicationContext(),, Toast.LENGTH_SHORT).show();
                        hud.dismiss();
                        jsonObject.toString().replace("\\\\", "");
                        showAlertMessageDialog(jsonObject.getString("message"));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }//                    jsonObject.toString().replace("\\\\","");
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                hud.dismiss();

                Log.d("gttt", call.toString());
                Toast.makeText(getApplicationContext(), "failed", Toast.LENGTH_SHORT).show();

            }
        });

    }

    public static String getFilePathFromURI(Context context, Uri contentUri) {
        //copy file and send new file path

        File wallpaperDirectory = new File(
                String.valueOf(Environment.getExternalStorageDirectory()));
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }

        File copyFile = new File(wallpaperDirectory + File.separator + Calendar.getInstance()
                .getTimeInMillis() + ".mp4");
        // create folder if not exists

        copy(context, contentUri, copyFile);
        Log.d("vPath--->", copyFile.getAbsolutePath());

        return copyFile.getAbsolutePath();

    }

    public static void copy(Context context, Uri srcUri, File dstFile) {
        try {
            InputStream inputStream = context.getContentResolver().openInputStream(srcUri);
            if (inputStream == null) return;
            OutputStream outputStream = new FileOutputStream(dstFile);
            copystream(inputStream, outputStream);
            inputStream.close();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int copystream(InputStream input, OutputStream output) throws Exception, IOException {
        byte[] buffer = new byte[BUFFER_SIZE];

        BufferedInputStream in = new BufferedInputStream(input, BUFFER_SIZE);
        BufferedOutputStream out = new BufferedOutputStream(output, BUFFER_SIZE);
        int count = 0, n = 0;
        try {
            while ((n = in.read(buffer, 0, BUFFER_SIZE)) != -1) {
                out.write(buffer, 0, n);
                count += n;
            }
            out.flush();
        } finally {
            try {
                out.close();
            } catch (IOException e) {
                Log.e(e.getMessage(), String.valueOf(e));
            }
            try {
                in.close();
            } catch (IOException e) {
                Log.e(e.getMessage(), String.valueOf(e));
            }
        }
        return count;
    }

    public void showAlertMessageDialog(String message) {

        new IOSDialog.Builder(VideoPimpingActivity.this)
                .title(R.string.app_name)              // String or String Resource ID
                .message(message)  // String or String Resource ID
                .positiveButtonText("Ok")  // String or String Resource ID
                .negativeButtonText("Cancel")   // String or String Resource ID
                .positiveClickListener(new IOSDialog.Listener() {
                    @Override
                    public void onClick(IOSDialog iosDialog) {
                        iosDialog.dismiss();
                        startActivity(new Intent(VideoPimpingActivity.this, HomeFeedActivity.class));
                        finish();
                        Utilities.saveString(VideoPimpingActivity.this, "videoUploaded", "yes");
//                        Intent in1 = new Intent(VideoPimpingActivity.this, HomeFeedActivity.class);
//                        in1.putExtra("videoUploaded", "yes");
                    }
                }).negativeClickListener(new IOSDialog.Listener() {
            @Override
            public void onClick(IOSDialog iosDialog) {
                iosDialog.dismiss();

            }
        })
                .build()
                .show();
    }

    public long getDuration(Context context, String path) {
        MediaPlayer mMediaPlayer = null;
        long duration = 0;
        try {
            mMediaPlayer = new MediaPlayer();
            mMediaPlayer.setDataSource(context, Uri.parse(path));
            mMediaPlayer.prepare();
            duration = mMediaPlayer.getDuration();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mMediaPlayer != null) {
                mMediaPlayer.reset();
                mMediaPlayer.release();
                mMediaPlayer = null;
            }

        }
        long timeInMillisec = Long.parseLong(String.valueOf(duration));
        videoDurationss = Double.valueOf(convertMillieToHMmSs(timeInMillisec));
        Toast.makeText(VideoPimpingActivity.this, "video duration is" + videoDurationss.toString(), Toast.LENGTH_SHORT).show();
        return duration;
    }

    public String convertMillieToHMmSs(long millie) {
        long seconds = (millie / 1000);
        long second = seconds % 60;
        long minute = (seconds / 60) % 60;
        long hour = (seconds / (60 * 60)) % 24;
//        Toast.makeText(VideoPimpingActivity.this, "only seconds"+seconds, Toast.LENGTH_SHORT).show();
//        Double result = Double.valueOf(second);
//        if (minute > 0) {
        return String.format("%02d", seconds);
//        } else {
//            return String.format("%02d", second);
//        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            // Get the Uri of the selected file
            getUri = data.getData();
            String selectedFilePath = HelperMethods.getPath(getApplicationContext(), getUri);
            File file = new File(selectedFilePath);
            selected_image_path = file.getAbsolutePath();

            if (selected_image_path != null) {
                if (data.equals("camera")) {
                    videoView.setVideoPath(selected_image_path);
                } else {
                    videoView.setVideoURI(Uri.parse(selected_image_path));
                }
                Utilities.saveString(VideoPimpingActivity.this, "selected_video", selected_image_path);
                Utilities.saveString(VideoPimpingActivity.this, "selected_uri", getUri.toString());
                Utilities.saveString(VideoPimpingActivity.this, "data", "camera");
                tvUrl.setText(selected_image_path);
                start_Video();
            }
            Log.d("ioooossss", path);
//            uploadVideo(path);
        }

        super.onActivityResult(requestCode, resultCode, data);

    }
}
