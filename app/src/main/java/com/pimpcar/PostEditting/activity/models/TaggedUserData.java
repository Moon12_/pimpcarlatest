package com.pimpcar.PostEditting.activity.models;

public class TaggedUserData {

    private String user_id;
    private String user_profile_pic;
    private String user_username;
    private float pivot_x_piont = 0;
    private float pivot_y_point =0;

    public TaggedUserData(String id, String pic, String name) {
        this.user_id = id;
        this.user_profile_pic = pic;
        this.user_username = name;

    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_profile_pic() {
        return user_profile_pic;
    }

    public void setUser_profile_pic(String user_profile_pic) {
        this.user_profile_pic = user_profile_pic;
    }

    public String getUser_username() {
        return user_username;
    }

    public void setUser_username(String user_username) {
        this.user_username = user_username;
    }

    public float getPivot_x_piont() {
        return pivot_x_piont;
    }

    public void setPivot_x_piont(float pivot_x_piont) {
        this.pivot_x_piont = pivot_x_piont;
    }

    public float getPivot_y_point() {
        return pivot_y_point;
    }

    public void setPivot_y_point(float pivot_y_point) {
        this.pivot_y_point = pivot_y_point;
    }
}
