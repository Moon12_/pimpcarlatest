package com.pimpcar.PostEditting.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.pimpcar.Network.model.user_search.UserSearchResponseBody;
import com.pimpcar.PostEditting.fragments.events.RecyclerViewItemClickMode;
import com.pimpcar.R;
import com.pimpcar.Widgets.CircleImageView;
import com.pimpcar.utils.ResourcesUtil;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchUserAdapter extends RecyclerView.Adapter<SearchUserAdapter.UserSearchViewHolder> {


    private List<UserSearchResponseBody.DataBean.SerachedDataBean> _mSerachedDataBeans;
    private Context _mContext;
    private LayoutInflater _mLayoutInflater;
    private SearchUserAdapter.UserSearchViewHolder view_user_search_holder;
    private boolean do_we_need_to_show;


    public SearchUserAdapter(Context context, List<UserSearchResponseBody.DataBean.SerachedDataBean> serached_data, boolean is_need_to_show) {
        this._mContext = context;
        this._mSerachedDataBeans = serached_data;
        _mLayoutInflater = LayoutInflater.from(_mContext);
        this.do_we_need_to_show = is_need_to_show;
    }

    @NonNull
    @Override
    public UserSearchViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = _mLayoutInflater.inflate(R.layout.user_serach_item_layout, viewGroup, false);
        ButterKnife.bind(this, itemView);
        view_user_search_holder = new UserSearchViewHolder(itemView);
        return view_user_search_holder;
    }


    @Override
    public void onBindViewHolder(@NonNull UserSearchViewHolder userSearchViewHolder, int i) {


        String full_name = "";
        if (_mSerachedDataBeans.get(i).getFname() != null && !_mSerachedDataBeans.get(i).getFname().isEmpty())
            full_name = _mSerachedDataBeans.get(i).getFname();

        if (_mSerachedDataBeans.get(i).getLname() != null && !_mSerachedDataBeans.get(i).getLname().isEmpty())
            full_name = full_name + " " + _mSerachedDataBeans.get(i).getLname();


        userSearchViewHolder.user_name_full_name.setText(full_name);

        if (do_we_need_to_show) {
            view_user_search_holder.user_follow_button.setVisibility(View.VISIBLE);
        } else {
            view_user_search_holder.user_follow_button.setVisibility(View.GONE);
        }


        if (_mSerachedDataBeans.get(i).getProfile_pic() != null && !_mSerachedDataBeans.get(i).getProfile_pic().isEmpty()) {
            Picasso.with(_mContext)
                    .load(_mSerachedDataBeans.get(i).getProfile_pic())
                    .fit()
                    .into(view_user_search_holder.user_profile_pic);
        } else {

            view_user_search_holder.user_profile_pic.setImageDrawable(ResourcesUtil.getDrawableById(R.drawable.user_profile_place_holder));

        }


        view_user_search_holder.itemView.setOnClickListener(v -> {
            EventBus.getDefault().post(new RecyclerViewItemClickMode(_mSerachedDataBeans.get(i).getUid(), _mSerachedDataBeans.get(i).getProfile_pic(), userSearchViewHolder.user_name_full_name.getText().toString()));
        });


    }

    @Override
    public int getItemCount() {
        if (_mSerachedDataBeans != null && _mSerachedDataBeans.size() > 0)
            return _mSerachedDataBeans.size();
        return 0;
    }


    public class UserSearchViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.user_profile_pic)
        CircleImageView user_profile_pic;

        @BindView(R.id.user_name_full_name)
        AppCompatTextView user_name_full_name;

        @BindView(R.id.user_name)
        AppCompatTextView user_name;


        @BindView(R.id.user_follow_button)
        AppCompatTextView user_follow_button;

        public UserSearchViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
