package com.pimpcar.PostEditting.custom_interfaces;

import android.view.View;

public interface RecyclerViewClickListener {
    void onClick(View view, int position);
}
