package com.pimpcar.PostEditting.fragments.events;

public class RecyclerViewItemClickMode {

    private String user_id;
    private String user_profile_pic;
    private String user_name;


    public RecyclerViewItemClickMode(String uid, String pic, String uName) {

        this.user_id = uid;
        this.user_profile_pic = pic;
        this.user_name = uName;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_profile_pic() {
        return user_profile_pic;
    }

    public void setUser_profile_pic(String user_profile_pic) {
        this.user_profile_pic = user_profile_pic;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }
}
