package com.pimpcar.PostEditting.fragments.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class StickerApiResponse {
    @SerializedName("data")
    @Expose
    private StickerApiPOJO data;

    public StickerApiPOJO getData() {
        return data;
    }

    public void setData(StickerApiPOJO data) {
        this.data = data;
    }

    public class StickerApiPOJO {
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("stickers")
        @Expose
        private List<String> sticker = null;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<String> getSticker() {
            return sticker;
        }

        public void setSticker(List<String> wheels) {
            this.sticker = wheels;
        }
    }
}
