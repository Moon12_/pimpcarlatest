package com.pimpcar.SplashScreen.activity;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.pimpcar.HomeFeed.activity.HomeFeedActivity;
import com.pimpcar.R;
import com.pimpcar.UserAuthentication.activity.UserAuthentication;
import com.pimpcar.utils.HelperMethods;

public class SplashScreen extends AppCompatActivity {
    FirebaseAnalytics firebaseAnalytics;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.splash_activity);

        firebaseAnalytics = FirebaseAnalytics.getInstance(this);

        new Handler().postDelayed(() -> {

            if (HelperMethods.is_user_logged_in()) {
                Intent intent = new Intent(SplashScreen.this, HomeFeedActivity.class);
                startActivity(intent);
                finish();
            } else {
                Intent intent = new Intent(SplashScreen.this, UserAuthentication.class);
                startActivity(intent);
                finish();
            }


        }, 3000);
    }




}
