package com.pimpcar.UserAuthentication.eventbus.models;



public class CloseButtonEventModel {

    private String fragment_name_closed;

    public CloseButtonEventModel(String _fragment_name_closed) {
        this.fragment_name_closed = _fragment_name_closed;

    }

    public String getFragment_name_closed() {
        return fragment_name_closed;
    }

    public void setFragment_name_closed(String fragment_name_closed) {
        this.fragment_name_closed = fragment_name_closed;
    }
}
