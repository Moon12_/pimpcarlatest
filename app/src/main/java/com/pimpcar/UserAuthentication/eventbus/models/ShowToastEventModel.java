package com.pimpcar.UserAuthentication.eventbus.models;

public class ShowToastEventModel {

    private String toasttype;
    private String message;

    public ShowToastEventModel(String toastype, String messge) {
        this.toasttype = toastype;
        this.message = messge;
    }

    public String getToasttype() {
        return toasttype;
    }

    public void setToasttype(String toasttype) {
        this.toasttype = toasttype;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
