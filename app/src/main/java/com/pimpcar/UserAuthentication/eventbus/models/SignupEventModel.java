package com.pimpcar.UserAuthentication.eventbus.models;

import org.json.JSONObject;

public class SignupEventModel {

    private String login_type;
    private JSONObject facebook_user_data;

    public SignupEventModel(JSONObject _facebook_user_dat, String logintype) {

        this.login_type = logintype;
        this.facebook_user_data = _facebook_user_dat;
    }

    public String getLogin_type() {
        return login_type;
    }

    public void setLogin_type(String login_type) {
        this.login_type = login_type;
    }

    public JSONObject getFacebook_user_data() {
        return facebook_user_data;
    }

    public void setFacebook_user_data(JSONObject facebook_user_data) {
        this.facebook_user_data = facebook_user_data;
    }
}
