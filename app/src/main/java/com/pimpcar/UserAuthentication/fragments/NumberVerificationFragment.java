package com.pimpcar.UserAuthentication.fragments;

import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hbb20.CountryCodePicker;
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;
import com.pimpcar.Network.ApiClient;
import com.pimpcar.Network.ApiService;
import com.pimpcar.Network.model.contact_number_verification.OTPSendBody;
import com.pimpcar.Network.model.contact_number_verification.OTPSendResponse;
import com.pimpcar.R;
import com.pimpcar.UserAuthentication.eventbus.models.CloseButtonEventModel;
import com.pimpcar.UserAuthentication.eventbus.models.CongratulationEventModel;
import com.pimpcar.UserAuthentication.eventbus.models.ShowToastEventModel;
import com.pimpcar.Widgets.otpview.OtpTextView;
import com.pimpcar.Widgets.progressbar.KProgressHUD;
import com.pimpcar.utils.Constants;
import com.pimpcar.utils.HelperMethods;
import com.pimpcar.utils.NetworkResponseMessages;
import com.pimpcar.utils.ResourcesUtil;
import com.pimpcar.utils.Utils;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;

public class NumberVerificationFragment extends Fragment {

    public final String TAG = NumberVerificationFragment.class.toString();
    @BindView(R.id.otp_view)
    OtpTextView otp_view;
    @BindView(R.id.ccp)
    CountryCodePicker ccp;
    @BindView(R.id.user_contact_number)
    AppCompatEditText user_contact_number;
    @BindView(R.id.login_tv)
    AppCompatTextView login_tv;
    @BindView(R.id.signup_next_button)
    FloatingActionButton signup_next_button;
    @BindView(R.id.close_button)
    AppCompatImageView close_button;
    @BindView(R.id.toolbar_title_tv)
    AppCompatTextView toolbar_title_tv;
    @BindView(R.id.resend_otp_to_user_tv)
    AppCompatTextView resend_otp_to_user_tv;
    private Unbinder unbinder = null;
    private boolean is_verification_code_sent = false;

    private ApiService apiService;
    private CompositeDisposable disposable = new CompositeDisposable();
    private KProgressHUD hud;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getActivity() != null) {
            apiService = ApiClient.getClient(getActivity().getApplicationContext()).create(ApiService.class);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root_view = inflater.inflate(R.layout.number_verification_fragment, container, false);
        unbinder = ButterKnife.bind(this, root_view);
        toolbar_title_tv.setText(ResourcesUtil.getString(R.string.signup_string));

        login_tv.setVisibility(View.GONE);
        resend_otp_to_user_tv.setVisibility(View.GONE);
        check_if_verification_sent();
        return root_view;
    }

    private void check_if_verification_sent() {
        if (is_verification_code_sent) {
            signup_next_button.setTag("VERIFY_OTP_API_CALL");
            otp_view.setVisibility(View.VISIBLE);
        } else {
            signup_next_button.setTag("SEND_OTP_API_CALL");
            otp_view.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.resend_otp_to_user_tv)
    public void click_on_resend_otp() {


        if (HelperMethods.isConnectedToInternet(getActivity())) {

            String number = user_contact_number.getText().toString();
            String countryCode = ccp.getSelectedCountryNameCode();

            if (!number.isEmpty() && !countryCode.isEmpty()) {
                send_otp_to_contact_number(number, countryCode);
            }
        } else {
            EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_WARNING, ResourcesUtil.getString(R.string.error_internet_not_available)));

        }


    }

    @OnClick(R.id.close_button)
    public void click_on_close_button() {
        EventBus.getDefault().post(new CloseButtonEventModel(NumberVerificationFragment.class.toString()));
    }

    @OnClick(R.id.signup_next_button)
    public void click_on_next_button() {


        if (HelperMethods.isConnectedToInternet(getActivity())) {
            if (signup_next_button.getTag().toString().contentEquals("VERIFY_OTP_API_CALL")) {

                String otp = otp_view.getOTP();


//                verify_otp("1234");
                EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_SUCCESS, "Success"));
                click_on_close_button();
                EventBus.getDefault().post(new CongratulationEventModel());

            } else {
                String number = user_contact_number.getText().toString();
                String countryCode = ccp.getSelectedCountryNameCode();

                send_otp_to_contact_number(number, countryCode);

            }
        } else {
            EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_WARNING, ResourcesUtil.getString(R.string.error_internet_not_available)));

        }


    }

    private void send_otp_to_contact_number(String number, String countryCode) {

        show_progress_dialog();
        String jwt_key = HelperMethods.get_user_jwt_key();
        OTPSendBody otpSendBody = Utils.create_body_for_contact_number_body(number, countryCode);
        disposable.add(apiService.sendOTPContactNumber(jwt_key, otpSendBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<OTPSendResponse>() {
                    @Override
                    public void onSuccess(OTPSendResponse s) {
                        if (s.getData().getMessage().contentEquals(NetworkResponseMessages.MOBILE_NUMBER_OTP_SENT_TO + " " + number)) {
                            dismiss_progress_dialog();
                            EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_SUCCESS, s.getData().getMessage().toUpperCase()));
                            user_contact_number.setEnabled(false);
                            user_contact_number.setKeyListener(null);
                            user_contact_number.getBackground().setAlpha(51);
                            user_contact_number.setTextColor(ResourcesUtil.getColor(R.color.gray));
                            otp_view.setVisibility(View.VISIBLE);
                            otp_view.setFocusable(true);
                            resend_otp_to_user_tv.setVisibility(View.VISIBLE);
                            signup_next_button.setTag("VERIFY_OTP_API_CALL");
                        }
                    }

                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onError(Throwable e) {

                        dismiss_progress_dialog();
                        if (e instanceof HttpException) {
                            ResponseBody body = ((HttpException) e).response().errorBody();
                            try {
                                //
                                otp_view.setVisibility(View.VISIBLE);
                                otp_view.setFocusable(true);
                                resend_otp_to_user_tv.setVisibility(View.VISIBLE);
                                signup_next_button.setTag("VERIFY_OTP_API_CALL");
                                //
                                String error_body = body.string();
                                String error = Utils.get_error_from_response(error_body);
                                EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, error.toUpperCase()));
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                }));

    }

    private void verify_otp(String otp) {


//        show_progress_dialog();
//        String jwt_key = HelperMethods.get_user_jwt_key();
//        OTPVerifyBody otpVerifyBody = Utils.create_body_for_verify_contact_number_otp(Integer.parseInt(otp));
//
//        disposable.add(apiService.verifyOTPContactNumber(jwt_key, otpVerifyBody)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribeWith(new DisposableSingleObserver<OTPVerifyResponse>() {
//                    @Override
//                    public void onSuccess(OTPVerifyResponse s) {
//
//                        dismiss_progress_dialog();
//                        Log.d(TAG, "Message" + s.getData().getMessage());
//                        EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_SUCCESS, s.getData().getMessage().toUpperCase()));
//                        click_on_close_button();
//                        EventBus.getDefault().post(new CongratulationEventModel());

//                    }
//
//                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
//                    @Override
//                    public void onError(Throwable e) {
//                        dismiss_progress_dialog();
//                        if (e instanceof HttpException) {
//                            ResponseBody body = ((HttpException) e).response().errorBody();
//                            try {
//                                String error_body = body.string();
//                                String error = Utils.get_error_from_response(error_body);
//                                EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, error.toUpperCase()));
//                            } catch (IOException e1) {
//                                e1.printStackTrace();
//                            }
//                        }
//                    }
//                }));


    }

    public void show_progress_dialog() {
        hud = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE);
        hud.setSize(50, 50);

        hud.show();
    }

    public void dismiss_progress_dialog() {
        if (hud != null && hud.isShowing())
            hud.dismiss();
    }


    @Override
    public void onDestroyView() {
        if (unbinder != null)
            unbinder.unbind();
        super.onDestroyView();

    }
}
