package com.pimpcar.UserAuthentication.fragments;

import android.graphics.Paint;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;
import com.pimpcar.HomeFeed.fragments.profile.pojos.ChangeUserNameRequestBody;
import com.pimpcar.HomeFeed.fragments.profile.pojos.ChangeUserNameResponse;
import com.pimpcar.Network.ApiClient;
import com.pimpcar.Network.ApiService;
import com.pimpcar.Network.model.signup.SignupBody;
import com.pimpcar.Network.model.signup.SignupResponse;
import com.pimpcar.Network.model.userselfprofile.UserProfileFirstData;
import com.pimpcar.R;
import com.pimpcar.UserAuthentication.eventbus.models.CloseButtonEventModel;
import com.pimpcar.UserAuthentication.eventbus.models.CongratulationEventModel;
import com.pimpcar.UserAuthentication.eventbus.models.EmailVerificationEventModel;
import com.pimpcar.UserAuthentication.eventbus.models.LoginEventModel;
import com.pimpcar.UserAuthentication.eventbus.models.ShowToastEventModel;
import com.pimpcar.Widgets.progressbar.KProgressHUD;
import com.pimpcar.utils.Constants;
import com.pimpcar.utils.HelperMethods;
import com.pimpcar.utils.NetworkResponseMessages;
import com.pimpcar.utils.ResourcesUtil;
import com.pimpcar.utils.Utilities;
import com.pimpcar.utils.Utils;
import com.pimpcar.utils.validation.InputValidator;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import es.dmoral.toasty.Toasty;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import timber.log.Timber;

public class SignupFragment extends Fragment {

    @BindView(R.id.user_first_name_et)
    AppCompatEditText user_first_name_et;
    @BindView(R.id.user_last_name_et)
    AppCompatEditText user_last_name_et;
    @BindView(R.id.etUserName)
    AppCompatEditText etUserName;
    @BindView(R.id.user_password_et)
    AppCompatEditText user_password_et;
    @BindView(R.id.confirm_password_et)
    AppCompatEditText confirm_password_et;
    @BindView(R.id.user_email_id)
    AppCompatEditText user_email_id;
    @BindView(R.id.login_tv)
    AppCompatTextView login_tv;
    @BindView(R.id.proceed_next_button)
    FloatingActionButton proceed_next_button;
    @BindView(R.id.close_button)
    AppCompatImageView close_button;
    @BindView(R.id.toolbar_title_tv)
    AppCompatTextView toolbar_title_tv;
    private Unbinder unbinder = null;
    private ApiService apiService;
    private CompositeDisposable disposable = new CompositeDisposable();

    private UserProfileFirstData user_profile_data;
    private InputValidator validator_first_name;
    private InputValidator validator_last_name;
    private InputValidator validator_user_name;
    private InputValidator validator_password;
    private InputValidator validator_email;
    private InputValidator validator_confirm_password;

    private String signup_login_type = null;
    private String facebook_id = null;
    private KProgressHUD hud;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getActivity() != null) {
            Timber.d("getActivity() is not null in [onCreate] fragment");
        } else {
            Timber.d("getActivity() is null in [onCreate] fragment");
        }

    }

    private void change_user_name_at_server(String user_name,String jwtToken) {

//        show_progress_dialog();

        ChangeUserNameRequestBody changeUserNameRequestBody = HelperMethods.create_change_username_body(user_name);
        disposable.add(apiService.changeUsername(jwtToken, changeUserNameRequestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<ChangeUserNameResponse>() {
                    @Override
                    public void onSuccess(ChangeUserNameResponse s) {
//                        dismiss_progress_dialog();
//                        Toasty.success(getActivity().getApplicationContext(), s.getData().getMessage() + "");
                        Utilities.saveString(getActivity(),"userName",s.getData().getUser_name().getUser_name());
//                        Toast.makeText(getActivity(), ""+s.getData().getUser_name().getUser_name(), Toast.LENGTH_SHORT).show();
//                        user_profile_data.getData().getUser_data().getUsr_info().setUser_name( s.getData().getUser_name().getUser_name());

//                        username_tv.setText(s.getData().getUser_name().getUser_name() + "");
//                        user_name_tv.setText(s.getData().getUser_name().getUser_name() + "");
                        HelperMethods.save_user_name(s.getData().getUser_name().getUser_name() + "");

                    }

                    @Override
                    public void onError(Throwable e) {

                        dismiss_progress_dialog();
                        if (e instanceof HttpException) {
                            ResponseBody body = ((HttpException) e).response().errorBody();
                            try {
                                String error_body = body.string();
                                dismiss_progress_dialog();
                                String error = Utils.get_error_from_response(error_body);
                                Toasty.error(getActivity().getApplicationContext(), error + "");


                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                }));

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (getActivity() != null) {
            Timber.d("getActivity() is not null right now [onActivityCreated] fragment");
            apiService = ApiClient.getClient(getActivity().getApplicationContext()).create(ApiService.class);

        } else {
            Timber.d("getActivity() is null right now [onActivityCreated] fragment");

        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root_view = inflater.inflate(R.layout.signup_fragment, container, false);
        unbinder = ButterKnife.bind(this, root_view);
        signup_login_type = getArguments().getString(Constants.LOGIN_TYPE);
        login_tv.setPaintFlags(login_tv.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        if (signup_login_type.contentEquals("facebook")) {

            String facebook_data_json_object = getArguments().getString(Constants.USER_FACEBOOK_JSON_OBJECT);
            Timber.d("facebook json data " + facebook_data_json_object);


            try {
                JSONObject user_json_object = new JSONObject(facebook_data_json_object);


                String facebook_id = user_json_object.has("id") ? user_json_object.getString("id") : "";
                if (facebook_id != null && !facebook_id.isEmpty()) {
                    setFacebook_id(facebook_id);
                }

                String facebook_name = user_json_object.has("name") ? user_json_object.getString("name") : "";
                String facebook_email = user_json_object.has("email") ? user_json_object.getString("email") : "";


                Timber.d("facebook name :: " + facebook_name);

                final Pattern SPACE = Pattern.compile(" ");
                String[] arr = SPACE.split(facebook_name);


                Timber.d("serparte name " + arr.length);
                Timber.d("first name " + arr[0] + " -----" + arr[1]);


                if (arr.length > 0) {
                    user_first_name_et.setText(arr[0]);
                    user_first_name_et.setEnabled(false);
                    user_first_name_et.setKeyListener(null);
                    user_first_name_et.getBackground().setAlpha(51);
                    user_first_name_et.setTextColor(ResourcesUtil.getColor(R.color.gray));

                    if (arr.length > 1) {
                        user_last_name_et.setText(arr[1]);
                        user_last_name_et.setEnabled(false);
                        user_last_name_et.setKeyListener(null);
                        user_last_name_et.getBackground().setAlpha(51);
                        user_last_name_et.setTextColor(ResourcesUtil.getColor(R.color.gray));
                    }

                    if (arr.length > 1) {
                        etUserName.setText(arr[1]);
                        etUserName.setEnabled(false);
                        etUserName.setKeyListener(null);
                        etUserName.getBackground().setAlpha(51);
                        etUserName.setTextColor(ResourcesUtil.getColor(R.color.gray));
                    }


                }


                if (!facebook_email.isEmpty() && HelperMethods.isValidEmailId(facebook_email)) {

                    user_email_id.setText(facebook_email);
                    user_email_id.setEnabled(false);
                    user_email_id.setKeyListener(null);
                    user_email_id.getBackground().setAlpha(51);
                    user_email_id.setTextColor(ResourcesUtil.getColor(R.color.gray));


                }


            } catch (JSONException e) {
                e.printStackTrace();
            }


        }

        toolbar_title_tv.setText(ResourcesUtil.getString(R.string.signup_string));
        set_validator();
        return root_view;
    }

    private void set_validator() {
        validator_first_name = new InputValidator.Builder(getActivity())
                .on(user_first_name_et)
                .required(true)
                .errorMessage(ResourcesUtil.getString(R.string.error_field_required))
                .showError(true)
                .build();

        validator_last_name = new InputValidator.Builder(getActivity())
                .on(user_last_name_et)
                .required(true)
                .errorMessage(ResourcesUtil.getString(R.string.error_field_required))
                .showError(true)
                .build();

        validator_user_name = new InputValidator.Builder(getActivity())
                .on(etUserName)
                .required(true)
                .errorMessage(ResourcesUtil.getString(R.string.error_field_required))
                .showError(true)
                .build();


        validator_email = new InputValidator.Builder(getActivity())
                .on(user_email_id)
                .required(true)
                .errorMessage(ResourcesUtil.getString(R.string.error_invalid_email))
                .validatorType(InputValidator.IS_EMAIL)
                .showError(true)
                .build();


        validator_password = new InputValidator.Builder(getActivity())
                .on(user_password_et)
                .required(true)
                .errorMessage(ResourcesUtil.getString(R.string.invalid_password))
                .minLength(8)
                .showError(true)
                .build();

        validator_confirm_password = new InputValidator.Builder(getActivity())
                .on(confirm_password_et)
                .required(true)
                .identicalAs(user_password_et)
                .errorMessage(ResourcesUtil.getString(R.string.invalid_password))
                .minLength(8)
                .showError(true)
                .build();

    }

    @OnClick(R.id.login_tv)
    public void click_on_login_tv() {
        click_on_close_button();
        EventBus.getDefault().post(new LoginEventModel());
    }

    @OnClick(R.id.proceed_next_button)
    public void click_on_singup_next_button() {


        if (HelperMethods.isConnectedToInternet(getActivity())) {


            if (validator_first_name.isValid() && validator_last_name.isValid() && validator_user_name.isValid()
                    && validator_password.isValid() && validator_confirm_password.isValid()) {
                if (HelperMethods.isValidEmailId(user_email_id.getText().toString())) {
                    if (user_password_et.getText().toString().trim().contentEquals(confirm_password_et.getText().toString().trim())) {
                        SignupBody signupBody = null;

                        if (signup_login_type.contentEquals("facebook")) {

                            signupBody = Utils.create_body_for_register_facebook_user(user_first_name_et.getText().toString(),
                                    user_last_name_et.getText().toString(), user_email_id.getText().toString(),
                                    confirm_password_et.getText().toString(), "facebook", getFacebook_id(), HelperMethods.get_facebook_token_key(), HelperMethods.get_facebook_profile_url_link(getFacebook_id()),
                                    "", "");

                        }

                        if (signup_login_type.contentEquals("email")) {
                            signupBody = Utils.create_body_for_register_user(user_first_name_et.getText().toString().trim(),
                                    user_last_name_et.getText().toString().trim(), user_email_id.getText().toString().trim(),
                                    confirm_password_et.getText().toString().trim(), "email");

                        }

                        register_user(signupBody);
                    } else {
                        EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, ResourcesUtil.getString(R.string.error_password_did_not_matched)));
                    }

                } else {
                    EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, ResourcesUtil.getString(R.string.error_invalid_email)));
                }

            }

        } else {
            EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_WARNING, ResourcesUtil.getString(R.string.error_internet_not_available)));

        }

    }

    private void register_user(SignupBody signupBody) {
        show_progress_dialog();


        disposable.add(apiService.register(signupBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<SignupResponse>() {
                    @Override
                    public void onSuccess(SignupResponse s) {
                        if (s.getData().getMessage().contentEquals(NetworkResponseMessages.USER_CREATED)) {
                            dismiss_progress_dialog();
                            Log.d(SignupFragment.class.getSimpleName(), "throwable fees " + s.getData().getMessage());
                            HelperMethods.save_user_jwt_key(s.getData().getJwt_token());
//                            String auth_key = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjIyODU1ZWIzLWZlNDAtNTgzMi1iNTRlLTZmODA0MmJhMGY2ZCIsImlhdCI6MTU3ODAzNDQ4MSwiZXhwIjoxNjA5NTkyMDgxfQ.6DR2PsveYy2dJlGTNGXstabxbu1JInzEyE7O9e33m44";
//                            HelperMethods.save_user_jwt_key(auth_key);
//                            Toast.makeText(getActivity(), ""+s.getData().getJwt_token(), Toast.LENGTH_SHORT).show();

                            change_user_name_at_server(etUserName.getText().toString(),s.getData().getJwt_token());
//                            HelperMethods.set_user_login_status(true);
                            HelperMethods.save_user_id(s.getData().getUid());
                            HelperMethods.save_user_email_id(user_email_id.getText().toString());
                            EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_SUCCESS, s.getData().getMessage().toUpperCase()));
                            click_on_close_button();
                            if (signup_login_type.contentEquals("facebook"))
                                EventBus.getDefault().post(new CongratulationEventModel());

                            if (signup_login_type.contentEquals("email"))
                                EventBus.getDefault().post(new EmailVerificationEventModel(Constants.EMAIL_VERIFICATION_FIRST_TIME, ""));
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        dismiss_progress_dialog();
                        if (e instanceof HttpException) {
                            ResponseBody body = ((HttpException) e).response().errorBody();
                            try {
                                String error_body = body.string();
                                String error = Utils.get_error_from_response(error_body);
                                EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, error.toUpperCase()));
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                }));

    }

    @OnClick(R.id.close_button)
    public void click_on_close_button() {
        EventBus.getDefault().post(new CloseButtonEventModel(SignupFragment.class.toString()));
    }

    public void show_progress_dialog() {
        hud = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE);
        hud.setSize(50, 50);

        hud.show();
    }

    public void dismiss_progress_dialog() {
        if (hud != null && hud.isShowing())
            hud.dismiss();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (disposable != null)
            disposable.dispose();
        if (unbinder != null)
            unbinder.unbind();

    }

    public String getFacebook_id() {
        return facebook_id;
    }

    public void setFacebook_id(String facebook_id) {
        this.facebook_id = facebook_id;
    }
}
