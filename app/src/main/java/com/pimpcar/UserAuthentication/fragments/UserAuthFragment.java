package com.pimpcar.UserAuthentication.fragments;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.pimpcar.R;
import com.pimpcar.UserAuthentication.eventbus.models.LoginEventModel;
import com.pimpcar.UserAuthentication.eventbus.models.ShowToastEventModel;
import com.pimpcar.UserAuthentication.eventbus.models.SignupEventModel;
import com.pimpcar.utils.Constants;
import com.pimpcar.utils.HelperMethods;
import com.pimpcar.utils.ResourcesUtil;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import timber.log.Timber;

public class UserAuthFragment extends Fragment {

    @BindView(R.id.login_button)
    AppCompatTextView login_button;
    @BindView(R.id.signup_button)
    AppCompatTextView signup_button;
    @BindView(R.id.facebook_login_button)
    AppCompatImageView facebook_login_button;
    private Unbinder unbinder = null;
    private CallbackManager callbackManager = null;
    private LoginManager loginManager;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root_view = inflater.inflate(R.layout.user_auth_layout, container, false);
        unbinder = ButterKnife.bind(this, root_view);
        generate_hash_key_of_facebook();
        return root_view;
    }


    @OnClick(R.id.facebook_login_button)
    public void click_on_facebook_login() {


        if (HelperMethods.isConnectedToInternet(getActivity())) {
            callbackManager = CallbackManager.Factory.create();
            LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email", "user_gender", "user_birthday"));
            LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(final LoginResult loginResult) {
                    AccessToken token = loginResult.getAccessToken();
                    Timber.d("token key :: " + token.getToken());
                    HelperMethods.set_facebook_token_key(loginResult.getAccessToken().getToken());
                    final GraphRequest request = GraphRequest.newMeRequest(token, (object, response) -> {
                        if (object != null) {
                            Timber.d("facebook data " + object.toString());
                            check_user_facebook_data(object);
                        }
                    });
                    Bundle parameters = new Bundle();
                    parameters.putString("fields", "id,name,email,link,gender,birthday");
                    request.setParameters(parameters);
                    request.executeAsync();
                }

                @Override
                public void onCancel() {
                    EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_WARNING, ResourcesUtil.getString(R.string.error_facebook_login_intruppted)));
                }

                @Override
                public void onError(FacebookException error) {
                    Timber.e("Error ::" + error.toString());
                    EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, ResourcesUtil.getString(R.string.error_facebook_login)));
                }
            });
        } else {
            EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_WARNING, ResourcesUtil.getString(R.string.error_internet_not_available)));

        }


    }


    private void check_user_facebook_data(JSONObject _facebook_user_dat) {
        Timber.d("User facebook data " + _facebook_user_dat.toString());

        EventBus.getDefault().post(new SignupEventModel(_facebook_user_dat, "facebook"));

    }


    @OnClick(R.id.signup_button)
    public void click_on_signup_button() {
        Timber.d("clickced on signup");
        EventBus.getDefault().post(new SignupEventModel(null, "email"));
    }


    @OnClick(R.id.login_button)
    public void click_on_login_button() {
        Timber.d("clickced on login");
        EventBus.getDefault().post(new LoginEventModel());

    }

    @OnClick(R.id.facebook_login_button)
    public void click_on_facebook_login_button() {

    }


    private void generate_hash_key_of_facebook() {
        try {
            PackageInfo info = getActivity().getPackageManager().getPackageInfo(
                    "com.pimpcar",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

            Timber.d(" [PackageManager.NameNotFoundException]error : : " + e.getMessage());
        } catch (NoSuchAlgorithmException e) {
            Timber.d(" [NoSuchAlgorithmException]error : : " + e.getMessage());
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (callbackManager != null && data != null)
            callbackManager.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public void onDestroyView() {
        if (unbinder != null)
            unbinder.unbind();
        super.onDestroyView();

    }


}
