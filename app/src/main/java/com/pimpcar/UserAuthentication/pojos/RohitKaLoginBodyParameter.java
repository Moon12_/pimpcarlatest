package com.pimpcar.UserAuthentication.pojos;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class RohitKaLoginBodyParameter implements Parcelable {

    public static final Creator<RohitKaLoginBodyParameter> CREATOR = new Creator<RohitKaLoginBodyParameter>() {
        @Override
        public RohitKaLoginBodyParameter createFromParcel(Parcel in) {
            return new RohitKaLoginBodyParameter(in);
        }

        @Override
        public RohitKaLoginBodyParameter[] newArray(int size) {
            return new RohitKaLoginBodyParameter[size];
        }
    };
    @SerializedName("user")
    private UserBean user;

    /**
     * user : {"email":"rohit@elgroupinternational.com","password":"123456","role":"dj","device_token":"12345678"}
     */


    public RohitKaLoginBodyParameter() {

    }

    protected RohitKaLoginBodyParameter(Parcel in) {
    }

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public static class UserBean {
        /**
         * email : rohit@elgroupinternational.com
         * password : 123456
         * role : dj
         * device_token : 12345678
         */

        @SerializedName("email")
        private String email;
        @SerializedName("password")
        private String password;
        @SerializedName("role")
        private String role;
        @SerializedName("device_token")
        private String device_token;

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }

        public String getDevice_token() {
            return device_token;
        }

        public void setDevice_token(String device_token) {
            this.device_token = device_token;
        }
    }
}
