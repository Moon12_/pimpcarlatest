package com.pimpcar.Widgets.staggered_grid;

import android.os.Parcelable;

public interface AsymmetricItem extends Parcelable {
    int getColumnSpan();

    int getRowSpan();
}
