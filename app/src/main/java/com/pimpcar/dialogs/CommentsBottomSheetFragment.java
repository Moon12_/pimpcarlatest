package com.pimpcar.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.pimpcar.R;
import com.pimpcar.models.comments.ChildComments;
import com.pimpcar.models.comments.ParentChildComments;

import java.util.ArrayList;

public class CommentsBottomSheetFragment extends BottomSheetDialogFragment
        implements View.OnClickListener {
    public static final String TAG = "ActionBottomDialog";
    private ItemClickListener mListener;
    View view;
    TextView tvTryMantiest;

    public static CommentsBottomSheetFragment newInstance() {
        return new CommentsBottomSheetFragment();
    }
    private RecyclerView recyclerViewParent;

    ArrayList<ParentChildComments> parentChildObj;
    ImageView ivBack;
    int id;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.comments_dialog, container, false);
        ivBack = view.findViewById(R.id.ivBack);
//        tvTryMantiest = view.findViewById(R.id.tvTryMantiest);
        recyclerViewParent = (RecyclerView)view. findViewById(R.id.rv_parent);
        getActivity().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        LinearLayoutManager manager = new LinearLayoutManager(getActivity());
//        manager.setOrientation(LinearLayoutManager.VERTICAL);
//        recyclerViewParent.setLayoutManager(manager);
//        recyclerViewParent.setHasFixedSize(true);
//
//        ParentAdapter parentAdapter = new ParentAdapter(getActivity(), createData());
//        recyclerViewParent.setAdapter(parentAdapter);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        getDialog().getWindow()
                .getAttributes().windowAnimations = R.style.DialogAnimation;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ItemClickListener) {
            mListener = (ItemClickListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement ItemClickListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        TextView tvSelected = (TextView) view;
        mListener.onItemClick(tvSelected.getText().toString());

        dismiss();


    }

    public interface ItemClickListener {
        void onItemClick(String item);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                BottomSheetDialog bottomSheetDialog = (BottomSheetDialog) dialogInterface;
                setupFullHeight(bottomSheetDialog);
            }
        });
        return dialog;
    }

    private void setupFullHeight(BottomSheetDialog bottomSheetDialog) {
        FrameLayout bottomSheet = (FrameLayout) bottomSheetDialog.findViewById(R.id.design_bottom_sheet);
        BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
        ViewGroup.LayoutParams layoutParams = bottomSheet.getLayoutParams();

        int windowHeight = getWindowHeight();
        if (layoutParams != null) {
            layoutParams.height = windowHeight;
        }
        bottomSheet.setLayoutParams(layoutParams);
        behavior.setState(BottomSheetBehavior.STATE_HALF_EXPANDED);
    }

    private int getWindowHeight() {
        // Calculate window height for fullscreen use
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) getContext()).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }
    private ArrayList<ParentChildComments> createData() {
        parentChildObj = new ArrayList<>();
        ArrayList<ChildComments> list1 = new ArrayList<>();
        ArrayList<ChildComments> list2 = new ArrayList<>();
        ArrayList<ChildComments> list3 = new ArrayList<>();
        ArrayList<ChildComments> list4 = new ArrayList<>();
        ArrayList<ChildComments> list5 = new ArrayList<>();

        for (int i = 0; i < 3; i++) {
            ChildComments c1 = new ChildComments();
            c1.setChild_name("Child 1." + (i + 1));
            list1.add(c1);
        }

        for (int i = 0; i < 5; i++) {
            ChildComments c2 = new ChildComments();
            c2.setChild_name("Child 2." + (i + 1));
            list2.add(c2);
        }


        for (int i = 0; i < 2; i++) {
            ChildComments c3 = new ChildComments();
            c3.setChild_name("Child 3." + (i + 1));
            list3.add(c3);
        }


        for (int i = 0; i < 4; i++) {
            ChildComments c4 = new ChildComments();
            c4.setChild_name("Child 4." + (i + 1));
            list4.add(c4);
        }

        for (int i = 0; i < 2; i++) {
            ChildComments c5 = new ChildComments();
            c5.setChild_name("Child 5." + (i + 1));
            list5.add(c5);
        }


        ParentChildComments pc1 = new ParentChildComments();
        pc1.setChild(list1);
        parentChildObj.add(pc1);

        ParentChildComments pc2 = new ParentChildComments();
        pc2.setChild(list2);
        parentChildObj.add(pc2);


        ParentChildComments pc3 = new ParentChildComments();
        pc3.setChild(list3);
        parentChildObj.add(pc3);

        ParentChildComments pc4 = new ParentChildComments();
        pc4.setChild(list4);
        parentChildObj.add(pc4);

        ParentChildComments pc5 = new ParentChildComments();
        pc5.setChild(list5);
        parentChildObj.add(pc5);


        return parentChildObj;
    }
}
