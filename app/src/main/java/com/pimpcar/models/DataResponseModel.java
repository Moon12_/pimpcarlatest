package com.pimpcar.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataResponseModel {
    @SerializedName("message")
    private String message;
    @SerializedName("next_page")
    private int next_page;
    @SerializedName("posts")
    private List<VideoDataModel> posts;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<VideoDataModel> getPosts() {
        return posts;
    }

    public void setPosts(List<VideoDataModel> posts) {
        this.posts = posts;
    }

    public int getNext_page() {
        return next_page;
    }

    public void setNext_page(int next_page) {
        this.next_page = next_page;
    }
}
