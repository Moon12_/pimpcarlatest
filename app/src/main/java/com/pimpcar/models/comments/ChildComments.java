package com.pimpcar.models.comments;

public class ChildComments {
    String child_name;

    public ChildComments() {
    }

    public String getChild_name() {
        return child_name;
    }

    public void setChild_name(String child_name) {
        this.child_name = child_name;
    }
}