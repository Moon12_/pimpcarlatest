package com.pimpcar.models.comments;

import java.util.ArrayList;

public class ParentChildComments {
    ArrayList<ChildComments> child;

    public ParentChildComments() {}

    public ArrayList<ChildComments> getChild() {
        return child;
    }

    public void setChild(ArrayList<ChildComments> child) {
        this.child = child;
    }
}