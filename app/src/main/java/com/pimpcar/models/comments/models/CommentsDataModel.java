package com.pimpcar.models.comments.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CommentsDataModel {
    @SerializedName("_id")
    private String _id;
    @SerializedName("user_id")
    private String user_id;
    @SerializedName("user_name")
    private String user_name;

    @SerializedName("profile_pic")
    private String profile_pic;
    @SerializedName("comment_statement")
    private String comment_statement;
   @SerializedName("comment_timestamp")
    private String comment_timestamp;
    @SerializedName("comment_likes")
    private List<CommentsLikesModel> comment_likes;

    @SerializedName("comment_replies")
    private List<CommentsReplyTextModel> comment_replies;

    public CommentsDataModel() {
    }

    public CommentsDataModel( String user_id, String comment_statement, String comment_timestamp) {
        this.user_id = user_id;
        this.comment_statement = comment_statement;
        this.comment_timestamp = comment_timestamp;

    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String get_id() {
        return _id;
    }

    public String getComment_statement() {
        return comment_statement;
    }

    public void setComment_statement(String comment_statement) {
        this.comment_statement = comment_statement;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getComment_timestamp() {
        return comment_timestamp;
    }

    public void setComment_timestamp(String comment_timestamp) {
        this.comment_timestamp = comment_timestamp;
    }

    public List<CommentsLikesModel> getComment_likes() {
        return comment_likes;
    }

    public void setComment_likes(List<CommentsLikesModel> comment_likes) {
        this.comment_likes = comment_likes;
    }

    public List<CommentsReplyTextModel> getComment_replies() {
        return comment_replies;
    }

    public void setComment_replies(List<CommentsReplyTextModel> comment_replies) {
        this.comment_replies = comment_replies;
    }
}
