package com.pimpcar.models.comments.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CommentsResponseModel {
    @SerializedName("comments")
    private List<CommentsDataModel> comeentdata;

    public List<CommentsDataModel> getComeentdata() {
        return comeentdata;
    }

    public void setComeentdata(List<CommentsDataModel> comeentdata) {
        this.comeentdata = comeentdata;
    }
}
