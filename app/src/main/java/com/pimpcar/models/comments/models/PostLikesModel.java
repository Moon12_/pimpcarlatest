package com.pimpcar.models.comments.models;

import com.google.gson.annotations.SerializedName;

public class PostLikesModel {
    @SerializedName("_id")
    private String _id;
    @SerializedName("user_id")
    private String user_id;
   @SerializedName("timestamp")
    private String timestamp;
   @SerializedName("user_name")
    private String user_name;
   @SerializedName("like_timestamp")
    private String like_timestamp;

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getLike_timestamp() {
        return like_timestamp;
    }

    public void setLike_timestamp(String like_timestamp) {
        this.like_timestamp = like_timestamp;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
