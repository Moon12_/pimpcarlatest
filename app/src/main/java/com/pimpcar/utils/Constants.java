package com.pimpcar.utils;

public class Constants {
    public static final String DEFAULT_PROFILE_PIC = "user_profile_pic_url";
    public static final String DEFAULT_EMAIL_ID = "user_email_id";
    public static final String DEFAULT_PASSWORD = "user_account_password";
    public static final String EMAIL_VERIFICATION_FOR_PASSWORD_CHANGE = "email_verification_for_password_change";
    public static final String EMAIL_VERIFICATION_FIRST_TIME = "email_verification_first_time";
    public static final String EMAIL_REASON = "email_sending_reason";
    public static final String TOAST_TYPE_SUCCESS = "success";
    public static final String TOAST_TYPE_FAILURE = "failure";
    public static final String TOAST_TYPE_WARNING = "warning";
    public static final String TOAST_TYPE_INFO = "info";
    public static final String EMAIL_GIVEN = "email_given";
    public static final String LOGIN_TYPE = "user_signup_login_type";
    public static final String USER_FACEBOOK_JSON_OBJECT = "user_facebook_data_json_object";
    public static final String USER_POST_LIST = "user_post_list";
    public static final String USER_NEXT_PAGE_DETAILS = "next_page_data";
    public static final String USER_TOTAL_POST_FROM_HOME_TO_PROFILE = "user_total_post_from_activity_from_fragment";
    public static final String USER_NEXT_PAGE_DETAILS_FROM_HOME_TO_PROFILE = "user_total_number_of_pages_loaded";
    public static final String SINGLE_POST_DATA = "single_post_data";
    public static final String POST_ID_SENT = "single_post_id";
    public static final String POST_NUMBER_OF_LIKES = "number_of_single_post_likes";
    public static final String USER_TYPE = "user_type";
    public static final String USER_TYPE_THIRD = "third_user";
    public static final String USER_TYPE_SELF = "self_user";
    public static final String THIRD_USER_ID = "third_profile_user_id";
    public static final String USER_STORY_ARRAY_LIST = "user_stories_array_list";
    public static final String USER_STORY_CAPTION = "user_story_caption_text";
    public static final String COMMENTS_COMING_FROM = "coming_from_activity";
    public static final String FEED_COMMENTS = "coming_from_feed";
    public static final String PROFILE_COMING = "coming_From_profile";
    public static final String YEAH_COMMENTS_UPDATED = "yes_made_a_comment_successfull";
    public static final String YEAH_UPDATED_BRO = "comment_made";
    public static final String API_KEY_YANDEX = "4d1cd7f1-a1a8-42e2-90f0-e15ad1364d73";
}
