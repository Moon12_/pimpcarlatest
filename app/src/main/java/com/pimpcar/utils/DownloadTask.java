package com.pimpcar.utils;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class DownloadTask {


    private static final String TAG = "Download Task";
    private Context context;

    private String downloadUrl = "";
    private String downloadFileName = "";
    MyProgressDialog myProgressDialog;
    String fileType;
    public static String filenPath;

    public DownloadTask(@NotNull Context context, @NotNull String music, @NotNull String s, @NotNull URLSharePath activity) {
        this.context = context;

        this.downloadUrl = music;
        myProgressDialog = new MyProgressDialog(context);
        this.fileType = s;

        this.URLSharePath = activity;
        downloadFileName = downloadUrl.substring(downloadUrl.lastIndexOf('/'), downloadUrl.length());
        Log.e(TAG, downloadFileName);

        new DownloadingTask().execute();
    }

    private class DownloadingTask extends AsyncTask<Void, Void, Void> {

        File apkStorage = null;
        File outputFile = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            myProgressDialog.show();
        }

        @Override
        protected void onPostExecute(Void result) {
            try {
                if (outputFile != null) {
                    myProgressDialog.dismiss();

                    gettheFilePath(outputFile.getAbsolutePath(), downloadUrl);
                } else {

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {

                        }
                    }, 3000);

//                    new NotificationClass(context, outputFile.getAbsolutePath(), fileType);
                    Log.e(TAG, "Download Failed");
                    myProgressDialog.dismiss();
                    //  Toast.makeText(context, "Download Failed", Toast.LENGTH_LONG).show();

                }
            } catch (Exception e) {
                e.printStackTrace();

                //Change button text if exception occurs

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                    }
                }, 3000);
                Log.e(TAG, "Download Failed with Exception - " + e.getLocalizedMessage());

            }

            super.onPostExecute(result);
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            try {
                String folederName = "Pimpcar";
                URL url = new URL(downloadUrl);//Create Download URl
                HttpURLConnection c = (HttpURLConnection) url.openConnection();//Open Url Connection
                c.setRequestProperty("Accept-Encoding", "identity");
                c.setRequestMethod("GET");//Set Request Method to "GET" since we are grtting data
                c.connect();//connect the URL Connection

                //If Connection response is not OK then show Logs
                if (c.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    Log.e(TAG, "Server returned HTTP " + c.getResponseCode()
                            + " " + c.getResponseMessage());

                }

                //Get File if SD card is present
                if (isSDCardPresent()) {

                    apkStorage = new File(
                            Environment.getExternalStorageDirectory().getPath() + "/"
                                    + folederName);

                } else
                    Toast.makeText(context, "Oops!! There is no SD Card.", Toast.LENGTH_SHORT).show();

                //If File is not present create directory
                if (!apkStorage.exists()) {
                    apkStorage.mkdir();
                    Log.e(TAG, "Directory Created.");
                }

                outputFile = new File(apkStorage, downloadFileName);//Create Output file in Main File

                //Create New File if not present
                if (!outputFile.exists()) {
                    outputFile.createNewFile();
                    Log.e(TAG, "File Created");
                }

                int fileLength = c.getContentLength();

                FileOutputStream fos = new FileOutputStream(outputFile);//Get OutputStream for NewFile Location

                InputStream is = c.getInputStream();//Get InputStream for connection

                byte[] buffer = new byte[4096];//Set buffer type
                int len1 = 0;//init length
                while ((len1 = is.read(buffer)) != -1) {
                    fos.write(buffer, 0, len1);//Write new file
                }

                //Close all connection after doing task
                fos.close();
                is.close();
                myProgressDialog.dismiss();


            } catch (Exception e) {

                //Read exception if something went wrong
                e.printStackTrace();
                outputFile = null;
                Log.e(TAG, "Download Error Exception " + e.getMessage());
            }

            return null;
        }


    }

    private void gettheFilePath(String absolutePath, String downloadUrl) {

        URLSharePath.OnURLSharePathListner(absolutePath, downloadUrl);
    }


    public boolean isSDCardPresent() {
        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            return true;
        }
        return false;
    }

    public interface URLSharePath {
        void OnURLSharePathListner(String path, String url);
    }

    URLSharePath URLSharePath;
}
