package com.pimpcar.utils;

public class NetworkResponseMessages {

    public static final String USER_CREATED = "user created successfully";
    public static final String REQUEST_FAILED = "server request failed";
    public static final String PROFILE_PIC_UPDATED = "profile image uploaded successfully";
    public static final String USER_OTP_SENT_TO_EMAIL = "otp sent to email";

    public static final String USER_LOGGED_IN_SUCCESSFULLY = "login successfully";
    public static final String USER_PASSWORD_CHANGE_SUCCESSFULLY = "password changed successfully";

    public static final String USER_AUTHENTICATED_SUCCESSFULLY = "user authenticated successfully";
    public static final String MOBILE_NUMBER_OTP_SENT_TO = "otp sent to contact number";
    public static final String USER_EMAIL_NOT_VERIFIED = "verify your email to proceed";
    public static final String USER_PROFILE_DATA = "user profile data";
    public static final CharSequence COMMENT_SUCESSFULLY_ON_POST = "commented successfully";
    public static final CharSequence POST_SUBMIT_MESSAGE = "post created successfully";
    public static final String USER_NOTIFICATION_API_END = "/user/activity-notification";
}
