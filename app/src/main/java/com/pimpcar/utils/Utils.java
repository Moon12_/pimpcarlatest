package com.pimpcar.utils;


import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Base64;
import android.util.TypedValue;

import com.pimpcar.Network.model.contact_number_verification.OTPSendBody;
import com.pimpcar.Network.model.contact_number_verification.OTPVerifyBody;
import com.pimpcar.Network.model.login.LoginBody;
import com.pimpcar.Network.model.password_change.PasswordChangeBody;
import com.pimpcar.Network.model.posts.SubmitPostRequestBody;
import com.pimpcar.Network.model.posts.UserPostDetailsRequestBody;
import com.pimpcar.Network.model.signup.SignupBody;
import com.pimpcar.Network.model.user_dob.UserDOBUpdateBody;
import com.pimpcar.Network.model.user_search.UserSearchRequestBody;
import com.pimpcar.Network.model.useremailverification.UserEmailVerificationOTPBody;
import com.pimpcar.Network.model.useremailverification.UserEmailVerifyOTPBody;
import com.pimpcar.Network.model.usergender.UserGenderProfileUpdateBody;
import com.pimpcar.PostEditting.activity.models.TaggedUserData;
import com.pimpcar.UserAuthentication.pojos.RohitKaLoginBodyParameter;
import com.pimpcar.utils.compressor.ImageUtil;

import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import timber.log.Timber;

public class Utils {

    public static final List<Long> times = Arrays.asList(
            TimeUnit.DAYS.toMillis(365),
            TimeUnit.DAYS.toMillis(30),
            TimeUnit.DAYS.toMillis(1),
            TimeUnit.HOURS.toMillis(1),
            TimeUnit.MINUTES.toMillis(1),
            TimeUnit.SECONDS.toMillis(1));
    public static final List<String> timesString = Arrays.asList("year", "month", "day", "hour", "minute", "second");
    private static float scale;

    public static String toDuration(long duration) {

        StringBuffer res = new StringBuffer();
        for (int i = 0; i < Utils.times.size(); i++) {
            Long current = Utils.times.get(i);
            long temp = duration / current;
            if (temp > 0) {
                res.append(temp).append(" ").append(Utils.timesString.get(i)).append(temp != 1 ? "s" : "");
                break;
            }
        }
        if ("".equals(res.toString()))
            return "0 seconds ago";
        else
            return res.toString();
    }

    public static int dpToPixel(float dp, Context context) {
        if (scale == 0) {
            scale = context.getResources().getDisplayMetrics().density;
        }
        return (int) (dp * scale);
    }


    public static SignupBody create_body_for_register_user(String fname, String lname, String email, String password, String loginType) {
        SignupBody signupBody = new SignupBody();
        SignupBody.DataBean dataBean = new SignupBody.DataBean();
        SignupBody.DataBean.AttributesBean arrtributes = new SignupBody.DataBean.AttributesBean();
        arrtributes.setEmail(email);
        arrtributes.setFname(fname);
        arrtributes.setLname(lname);
        arrtributes.setPassword(password);
        arrtributes.setLogin_type(loginType);
        dataBean.setAttributes(arrtributes);
        signupBody.setData(dataBean);
        return signupBody;
    }

    public static UserGenderProfileUpdateBody create_body_for_gender_update(Bitmap compressed_bitmap, String gender) {
        UserGenderProfileUpdateBody userGenderProfileUpdateBody = new UserGenderProfileUpdateBody();
        UserGenderProfileUpdateBody.DataBean dataBean = new UserGenderProfileUpdateBody.DataBean();
        UserGenderProfileUpdateBody.DataBean.AttributesBean attributesBean = new UserGenderProfileUpdateBody.DataBean.AttributesBean();
        String base64_image = ImageUtil.convert_bitmap_to_base64(compressed_bitmap);
        attributesBean.setPic_base64(base64_image);
        attributesBean.setGender(gender);
        dataBean.setAttributes(attributesBean);
        userGenderProfileUpdateBody.setData(dataBean);
        return userGenderProfileUpdateBody;
    }

    public static UserEmailVerificationOTPBody create_body_for_email_otp(String email) {
        UserEmailVerificationOTPBody userEmailVerificationOTPBody = new UserEmailVerificationOTPBody();
        UserEmailVerificationOTPBody.DataBean dataBean = new UserEmailVerificationOTPBody.DataBean();
        UserEmailVerificationOTPBody.DataBean.AttributesBean attributesBean = new UserEmailVerificationOTPBody.DataBean.AttributesBean();
        attributesBean.setEmail(email);
        dataBean.setAttributes(attributesBean);
        userEmailVerificationOTPBody.setData(dataBean);
        return userEmailVerificationOTPBody;

    }

    public static UserEmailVerifyOTPBody create_body_for_email_otp_verify(String email, int otp) {

        UserEmailVerifyOTPBody userEmailVerifyOTPBody = new UserEmailVerifyOTPBody();
        UserEmailVerifyOTPBody.DataBean dataBean = new UserEmailVerifyOTPBody.DataBean();
        UserEmailVerifyOTPBody.DataBean.AttributesBean attributesBean = new UserEmailVerifyOTPBody.DataBean.AttributesBean();

        attributesBean.setEmail(email);
        attributesBean.setOtp_verify(otp);
        dataBean.setAttributes(attributesBean);
        userEmailVerifyOTPBody.setData(dataBean);
        return userEmailVerifyOTPBody;

    }

    public static UserDOBUpdateBody create_body_for_dob(String dob) {
        UserDOBUpdateBody userDOBUpdateBody = new UserDOBUpdateBody();
        UserDOBUpdateBody.DataBean dataBean = new UserDOBUpdateBody.DataBean();
        UserDOBUpdateBody.DataBean.AttributesBean attributesBean = new UserDOBUpdateBody.DataBean.AttributesBean();
        attributesBean.setUser_dob(dob);

        dataBean.setAttributes(attributesBean);

        userDOBUpdateBody.setData(dataBean);
        return userDOBUpdateBody;
    }


    public static LoginBody create_body_for_login(String email_id, String password) {

        LoginBody loginBody = new LoginBody();

        LoginBody.DataBean dataBean = new LoginBody.DataBean();

        LoginBody.DataBean.AttributesBean attributesBean = new LoginBody.DataBean.AttributesBean();

        attributesBean.setEmail(email_id);

        attributesBean.setPassword(password);

        dataBean.setAttributes(attributesBean);

        loginBody.setData(dataBean);

        return loginBody;
    }

    public static RohitKaLoginBodyParameter create_body_paramter_for_rohit_login(String email, String password, String role, String deivcetoken) {

        RohitKaLoginBodyParameter rohitKaLoginBodyParameter = new RohitKaLoginBodyParameter();

        RohitKaLoginBodyParameter.UserBean userBean = new RohitKaLoginBodyParameter.UserBean();

        userBean.setEmail(email);
        userBean.setPassword(password);
        userBean.setDevice_token(deivcetoken);
        userBean.setRole(role);

        rohitKaLoginBodyParameter.setUser(userBean);

        return rohitKaLoginBodyParameter;

    }


    public static UserSearchRequestBody create_user_search_body(String search_keyword) {
        UserSearchRequestBody userSearchRequestBody = new UserSearchRequestBody();
        UserSearchRequestBody.DataBean dataBean = new UserSearchRequestBody.DataBean();
        UserSearchRequestBody.DataBean.AttributesBean attributesBean = new UserSearchRequestBody.DataBean.AttributesBean();
        attributesBean.setSearch_keyword(search_keyword);
        dataBean.setAttributes(attributesBean);
        userSearchRequestBody.setData(dataBean);
        return userSearchRequestBody;

    }


//    public static WheelApiPOJO wheelApiResponse() {
//
//        return new WheelApiPOJO();
//    }

    public static PasswordChangeBody create_body_for_change_password(String usr_pwd) {

        PasswordChangeBody passwordChangeBody = new PasswordChangeBody();

        PasswordChangeBody.DataBean dataBean = new PasswordChangeBody.DataBean();

        PasswordChangeBody.DataBean.AttributesBean attributesBean = new PasswordChangeBody.DataBean.AttributesBean();

        attributesBean.setPassword(usr_pwd);

        dataBean.setAttributes(attributesBean);

        passwordChangeBody.setData(dataBean);

        return passwordChangeBody;
    }


    public static OTPSendBody create_body_for_contact_number_body(String number, String countryCode) {

        OTPSendBody otpSendBody = new OTPSendBody();
        OTPSendBody.DataBean dataBean = new OTPSendBody.DataBean();
        OTPSendBody.DataBean.AttributesBean attributesBean = new OTPSendBody.DataBean.AttributesBean();
        attributesBean.setContact_number(number);
        attributesBean.setCountry(countryCode);
        dataBean.setAttributes(attributesBean);
        otpSendBody.setData(dataBean);
        return otpSendBody;
    }


    public static OTPVerifyBody create_body_for_verify_contact_number_otp(int otp_verify) {

        OTPVerifyBody otpVerifyBody = new OTPVerifyBody();
        OTPVerifyBody.DataBean dataBean = new OTPVerifyBody.DataBean();
        OTPVerifyBody.DataBean.AttributesBean attributesBean = new OTPVerifyBody.DataBean.AttributesBean();

        attributesBean.setOtp_verify(otp_verify);

        dataBean.setAttributes(attributesBean);

        otpVerifyBody.setData(dataBean);


        return otpVerifyBody;
    }

    public static int getPixels(Context context, int valueInDp) {
        Resources r = context.getResources();
        float px =
                TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, valueInDp, r.getDisplayMetrics());
        return (int) px;
    }

    public static int getPixels(Context context, float valueInDp) {
        Resources r = context.getResources();
        float px =
                TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, valueInDp, r.getDisplayMetrics());
        return (int) px;
    }

    public static int getPixelsSp(Context context, int valueInSp) {
        Resources r = context.getResources();
        float px =
                TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, valueInSp, r.getDisplayMetrics());
        return (int) px;
    }

    public static int getPixelsSp(Context context, float valueInSp) {
        Resources r = context.getResources();
        float px =
                TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, valueInSp, r.getDisplayMetrics());
        return (int) px;
    }

    public static String get_error_from_response(String errorbody) {
        try {
            JSONObject jsonObject = new JSONObject(errorbody);
            JSONArray errorJsonArray = jsonObject.has("errors") ? jsonObject.getJSONArray("errors") : null;
            Timber.d("array ::" + errorJsonArray.toString());
            JSONObject error_object = errorJsonArray.getJSONObject(0);
            String error = error_object.has("err") ? error_object.getString("err") : "invalid fields";
            return error;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static SignupBody create_body_for_register_facebook_user(String fname, String lname, String email_id,
                                                                    String password, String login_type, String facebook_id,
                                                                    String facebook_token_key, String facebook_profile_url_link,
                                                                    String gender, String dob) {

        SignupBody signupBody = new SignupBody();
        SignupBody.DataBean dataBean = new SignupBody.DataBean();
        SignupBody.DataBean.AttributesBean attributesBean = new SignupBody.DataBean.AttributesBean();

        attributesBean.setFname(fname);
        attributesBean.setLname(lname);
        attributesBean.setEmail(email_id);
        attributesBean.setPassword(password);
        attributesBean.setLogin_type(login_type);
        attributesBean.setFacebook_user_id(facebook_id);
        attributesBean.setFacebook_token_access(facebook_token_key);
        attributesBean.setProfile_pic(facebook_profile_url_link);
        attributesBean.setGender(gender);
        attributesBean.setDob(dob);
        dataBean.setAttributes(attributesBean);

        signupBody.setData(dataBean);

        return signupBody;
    }


    public static int getDeviceWidth(Context context) {
        return context.getResources().getDisplayMetrics().widthPixels;
    }

    public static int getDeviceHeight(Context context) {
        return context.getResources().getDisplayMetrics().heightPixels;
    }

    public static boolean isAboveApi21() {
        return Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP;
    }

    public static int dpToPx(Context context, int dp) {
        return (int) (dp * context.getResources().getDisplayMetrics().density);
    }

    public static int dpToPx(float dp) {
        float density = Resources.getSystem().getDisplayMetrics().density;
        return Math.round(dp * density);
    }

    public static String bitmapToBase64(Bitmap bitmap) {

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
        return encoded;
    }

    public static UserPostDetailsRequestBody create_post_details_request_body(String post_id) {
        UserPostDetailsRequestBody userPostDetailsRequestBody = new UserPostDetailsRequestBody();
        UserPostDetailsRequestBody.DataBean dataBean = new UserPostDetailsRequestBody.DataBean();
        UserPostDetailsRequestBody.DataBean.AttributesBean attributesBean = new UserPostDetailsRequestBody.DataBean.AttributesBean();
        attributesBean.setPost_id(post_id);

        dataBean.setAttributes(attributesBean);
        userPostDetailsRequestBody.setData(dataBean);
        return userPostDetailsRequestBody;
    }

    public static Drawable drawableFromUrl(String url) throws IOException {
        Bitmap x;

        HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
        connection.connect();
        InputStream input = connection.getInputStream();

        x = BitmapFactory.decodeStream(input);
        return new BitmapDrawable(Resources.getSystem(), x);
    }

    public static SubmitPostRequestBody create_body_for_submit_post(Bitmap main_bitmap_image, String hashTag, String location, List<TaggedUserData> taggedUserDataList) {

        SubmitPostRequestBody submitPostRequestBody = new SubmitPostRequestBody();
        SubmitPostRequestBody.DataBean dataBean = new SubmitPostRequestBody.DataBean();
        SubmitPostRequestBody.DataBean.AttributesBean attributesBean = new SubmitPostRequestBody.DataBean.AttributesBean();
//        location = location != null || location.isEmpty() ? " " : location;
        if (location != null || StringUtils.isNotEmpty(location)) {
            attributesBean.setLocation(location);
        } else {
            attributesBean.setLocation("");
        }

//        hashTag = hashTag != null || hashTag.isEmpty() ? " " : hashTag;
        if (hashTag != null || StringUtils.isNotEmpty(hashTag)) {
            attributesBean.setPost_description(hashTag);
        } else {
            attributesBean.setPost_description(" ");
        }

        String base_64_image = Utils.bitmapToBase64(main_bitmap_image);
        attributesBean.setPost_image_base64(base_64_image);
        List<SubmitPostRequestBody.DataBean.AttributesBean.PostUserTagsBean> postUserTagsBeans = new ArrayList<>();
        for (int i = 0; i < taggedUserDataList.size(); i++) {
            SubmitPostRequestBody.DataBean.AttributesBean.PostUserTagsBean single_data = new SubmitPostRequestBody.DataBean.AttributesBean.PostUserTagsBean();
            single_data.setUser_id(taggedUserDataList.get(i).getUser_id());
            single_data.setUser_pivot_x(taggedUserDataList.get(i).getPivot_x_piont());
            single_data.setUser_pivot_y(taggedUserDataList.get(i).getPivot_y_point());
            postUserTagsBeans.add(single_data);
        }
        attributesBean.setPost_user_tags(postUserTagsBeans);
        dataBean.setAttributes(attributesBean);
        submitPostRequestBody.setData(dataBean);
        return submitPostRequestBody;
    }

    /**
     * Function to get Progress percentage
     *
     * @param currentDuration
     * @param totalDuration
     */
    public static int getProgressPercentage(long currentDuration, long totalDuration) {
        Double percentage = (double) 0;

        long currentSeconds = (int) (currentDuration / 1000);
        long totalSeconds = (int) (totalDuration / 1000);

        // calculating percentage
        percentage = (((double) currentSeconds) / totalSeconds) * 100;

        // return percentage
        return percentage.intValue();
    }

    public String milliSecondsToTimer(long milliseconds) {
        String finalTimerString = "";
        String secondsString = "";

        // Convert total duration into time
        int hours = (int) (milliseconds / (1000 * 60 * 60));
        int minutes = (int) (milliseconds % (1000 * 60 * 60)) / (1000 * 60);
        int seconds = (int) ((milliseconds % (1000 * 60 * 60)) % (1000 * 60) / 1000);
        // Add hours if there
        if (hours > 0) {
            finalTimerString = hours + ":";
        }

        // Prepending 0 to seconds if it is one digit
        if (seconds < 10) {
            secondsString = "0" + seconds;
        } else {
            secondsString = "" + seconds;
        }

        finalTimerString = finalTimerString + minutes + ":" + secondsString;

        // return timer string
        return finalTimerString;
    }

    /**
     * Function to change progress to timer
     *
     * @param progress      -
     * @param totalDuration returns current duration in milliseconds
     */
    public int progressToTimer(int progress, int totalDuration) {
        int currentDuration = 0;
        totalDuration = (int) (totalDuration / 1000);
        currentDuration = (int) ((((double) progress) / 100) * totalDuration);

        // return current duration in milliseconds
        return currentDuration * 1000;
    }
}
