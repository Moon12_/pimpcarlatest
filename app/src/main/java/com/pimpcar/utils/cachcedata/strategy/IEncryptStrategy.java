package com.pimpcar.utils.cachcedata.strategy;


public interface IEncryptStrategy {

    String encrypt(String str);

    String decode(String str);
}
