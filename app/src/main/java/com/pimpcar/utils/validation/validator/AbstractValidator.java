package com.pimpcar.utils.validation.validator;


import com.pimpcar.Application.PimpCarApplication;
import com.pimpcar.R;

public abstract class AbstractValidator {

    String mErrorMessage = PimpCarApplication.getApplicationInstance().getApplicationContext().getString(R.string.error_invalid_field);

    public abstract boolean isValid(String value);

    public abstract String getErrorMessage();

    public void setErrorMessage(String message) {
        mErrorMessage = message;
    }
}
