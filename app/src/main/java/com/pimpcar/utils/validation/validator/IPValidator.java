package com.pimpcar.utils.validation.validator;


import android.util.Patterns;

import com.pimpcar.Application.PimpCarApplication;
import com.pimpcar.R;


public class IPValidator extends AbstractValidator {

    public IPValidator() {
        mErrorMessage = PimpCarApplication.getApplicationInstance().getApplicationContext().getString(R.string.error_invalid_ip);
    }

    @Override
    public boolean isValid(String value) {
        return Patterns.IP_ADDRESS.matcher(value).matches();
    }

    @Override
    public String getErrorMessage() {
        return mErrorMessage;
    }
}
