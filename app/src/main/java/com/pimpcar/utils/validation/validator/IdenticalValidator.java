package com.pimpcar.utils.validation.validator;

import android.widget.EditText;

import com.pimpcar.Application.PimpCarApplication;
import com.pimpcar.R;

public class IdenticalValidator extends AbstractValidator {

    private EditText mOtherEditText;

    public IdenticalValidator(EditText view) {
        mOtherEditText = view;
        mErrorMessage = PimpCarApplication.getInstance().getApplicationContext().getString(R.string.error_fields_mismatch);
    }

    @Override
    public boolean isValid(String value) {
        return value.equals(mOtherEditText.getText().toString());
    }

    @Override
    public String getErrorMessage() {
        return mErrorMessage;
    }
}
