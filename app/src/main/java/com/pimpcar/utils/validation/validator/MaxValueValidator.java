package com.pimpcar.utils.validation.validator;


import com.pimpcar.Application.PimpCarApplication;
import com.pimpcar.R;

public class MaxValueValidator extends AbstractValidator {

    private int mMaxValue;

    public MaxValueValidator(int value) {
        mMaxValue = value;
        mErrorMessage = PimpCarApplication.getApplicationInstance().getApplicationContext().getString(R.string.error_max_value, mMaxValue);
    }

    @Override
    public boolean isValid(String value) {
        try {
            double d = Double.parseDouble(value);
            mErrorMessage = PimpCarApplication.getApplicationInstance().getApplicationContext().getString(R.string.error_max_value, mMaxValue);
            return d <= mMaxValue;
        } catch (NumberFormatException nfe) {
            mErrorMessage = PimpCarApplication.getApplicationInstance().getApplicationContext().getString(R.string.error_invalid_number);
            return false;
        }
    }

    @Override
    public String getErrorMessage() {
        return mErrorMessage;
    }
}
