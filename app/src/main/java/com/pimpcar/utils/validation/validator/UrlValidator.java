package com.pimpcar.utils.validation.validator;


import android.util.Patterns;

import com.pimpcar.Application.PimpCarApplication;
import com.pimpcar.R;

public class UrlValidator extends AbstractValidator {

    public UrlValidator() {
        mErrorMessage = PimpCarApplication.getInstance().getApplicationContext().getString(R.string.error_invalid_url);
    }

    @Override
    public boolean isValid(String value) {
        return Patterns.WEB_URL.matcher(value).matches();
    }

    @Override
    public String getErrorMessage() {
        return mErrorMessage;
    }
}
