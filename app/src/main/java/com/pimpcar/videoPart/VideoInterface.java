package com.pimpcar.videoPart;

import com.google.gson.JsonObject;
import com.pimpcar.models.VideoListResponseModel;
import com.pimpcar.models.comments.models.CommentsLikeResponseModel;
import com.pimpcar.models.comments.models.CommentsResponseModel;
import com.pimpcar.models.comments.models.FollowUnFollowResponseModel;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface VideoInterface {

    String VIDEOURL = "http://3.145.19.246:4042/";

    @Multipart
    @POST("upload-video")
    Call<String> uploadImage(
            @Part MultipartBody.Part file,
//            @Part("file") RequestBody name,
            @Part("description") RequestBody description,
            @Part("hash_tags") RequestBody hash_tags,
            @Part("post_users_tag") RequestBody post_users_tag,
            @Part("user_id") RequestBody user_id,
            @Part("post_timestamp") RequestBody post_timestamp
    );

    @GET("get-post-comments/{post_id}")
    Call<CommentsResponseModel> getCommetnsApi(@Path("post_id") String postId);

    @POST("create-comment-like/{post_id}")
    Call<CommentsLikeResponseModel> getCommetnsLikeApi(@Path("post_id") String postId, @Body JsonObject jsonObject);

    @POST("create-comment-reply-like/{comment_reply_id}")
    Call<CommentsLikeResponseModel> getCommetnsReplyLikeApi(@Path("comment_reply_id") String postId, @Body JsonObject jsonObject);

    @POST("create-comment/{post_id}")
    Call<CommentsLikeResponseModel> createCommentsApi(@Path("post_id") String postId, @Body JsonObject jsonObject);

    @POST("create-comment-reply/{post_id}")
    Call<CommentsLikeResponseModel> createCommentsReplyApi(@Path("post_id") String postId, @Body JsonObject jsonObject);

    @POST("follow-unfollow/{post_id}")
    Call<FollowUnFollowResponseModel> followUnfollowApi(@Path("post_id") String postId, @Body JsonObject params);

    @GET("get-video-posts")
    Call<VideoListResponseModel> getVideolISTPosts();
}